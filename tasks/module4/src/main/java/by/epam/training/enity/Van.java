package by.epam.training.enity;

import org.apache.log4j.Logger;

import java.util.concurrent.atomic.AtomicLong;

public class Van {
    private static final Logger LOGGER = Logger.getLogger(Van.class);
    private static final AtomicLong atomic = new AtomicLong();

    private long vanId;
    private boolean withPerishableGoods;
    private TerminalAction action;

    public Van(TerminalAction action, boolean withPerishableGoods) {
        this.vanId = atomic.getAndIncrement();
        this.withPerishableGoods = withPerishableGoods;
        this.action = action;
        LOGGER.info(toString() + " wait his turn.");
    }

    public boolean isWithPerishableGoods() {
        return withPerishableGoods;
    }

    @Override
    public String toString() {
        return "Van{" +
                "vanId=" + vanId +
                ", withPerishableGoods=" + withPerishableGoods +
                ", action=" + action +
                '}';
    }


    public TerminalAction getAction() {
        return action;
    }
    public void goToTheQueue(LogisticBase base){
        base.addVanInQueue(this);
    }
}
