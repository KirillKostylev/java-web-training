package by.epam.training.enity;

import org.apache.log4j.Logger;

import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Terminal implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(Terminal.class);
    private long terminalId;
    private boolean isFree;
    private LogisticBase base;
    private Semaphore semaphore;

    public Terminal(long terminalId, LogisticBase base, Semaphore semaphore) {
        this.terminalId = terminalId;
        this.base = base;
        isFree = true;
        this.semaphore = semaphore;
    }

    public void load(Van van) {
        try {
            LOGGER.info(van + " is loading in " + this);
            TimeUnit.SECONDS.sleep(new Random().nextInt(10));
        } catch (InterruptedException e) {
            LOGGER.info("Impossible load van because - " + e.getMessage());
        }
    }

    public void unload(Van van) {
        try {
            LOGGER.info(van + " is unloading in " + this);
            TimeUnit.SECONDS.sleep(new Random().nextInt(10));
        } catch (InterruptedException e) {
            LOGGER.info("Impossible unload van because - " + e.getMessage());
        }
    }

    @Override
    public String toString() {
        return "Terminal{" +
                "terminalId=" + terminalId +
                '}';
    }

    @Override
    public void run() {
        while (true) {
            try {
                semaphore.acquire();
                Van van = base.getVanFromQueue();
                switch (van.getAction()) {
                    case LOAD:
                        load(van);
                        break;
                    case UNLOAD:
                        unload(van);
                        break;
                }
                semaphore.release();
                LOGGER.info(this + " is free.");
            } catch (InterruptedException e) {
                LOGGER.error("Terminal error. " + e.getMessage());
            }
        }
    }
}