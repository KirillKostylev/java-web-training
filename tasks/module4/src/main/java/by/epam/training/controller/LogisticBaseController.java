package by.epam.training.controller;

import by.epam.training.enity.LogisticBase;
import by.epam.training.enity.TerminalAction;
import by.epam.training.enity.Van;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class LogisticBaseController {
    public static void main(String[] args) {
        LogisticBase logisticBase = new LogisticBase(3);
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            boolean isPerishableGoods = random.nextBoolean();
            TerminalAction action = random.nextInt(2) == 1 ? TerminalAction.LOAD : TerminalAction.UNLOAD;
            Van van = new Van(action, isPerishableGoods);
            van.goToTheQueue(logisticBase);
        }
        try {
            TimeUnit.SECONDS.sleep(20);
            System.exit(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
