package by.epam.training.enity;

import java.util.PriorityQueue;

public class VanQueue {
    private PriorityQueue<Van> queue;

    VanQueue() {

        queue = new PriorityQueue<>((o1, o2) -> {
            if (o1.isWithPerishableGoods() && !o2.isWithPerishableGoods()) {
                return -1;
            } else if (o2.isWithPerishableGoods() && !o1.isWithPerishableGoods()) {
                return 1;
            }
            return 0;
        });
    }

    PriorityQueue<Van> getQueue() {
        return queue;
    }

    public boolean offer(Van v) {
        return queue.offer(v);
    }

    public Van poll() {
        return queue.poll();
    }

    public int getSize() {
        return queue.size();
    }
}
