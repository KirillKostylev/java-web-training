package by.epam.training.enity;

import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class LogisticBase {
    private static final Logger LOGGER = Logger.getLogger(LogisticBase.class);
    private VanQueue queue;
    private Semaphore semaphore;
    private ReentrantLock baseLocker;
    private Condition emptyQueue;

    public LogisticBase(int terminalNumber) {
        baseLocker = new ReentrantLock();
        emptyQueue = baseLocker.newCondition();
        semaphore = new Semaphore(terminalNumber);
        queue = new VanQueue();
        fillLogisticBaseWithTerminals(terminalNumber);
    }

    private void fillLogisticBaseWithTerminals(int terminalNumber) {
        for (int i = 1; i <= terminalNumber; i++) {
            Terminal terminal = new Terminal(i, this, semaphore);
            new Thread(terminal).start();
        }
    }

    public void addVanInQueue(Van van) {
        baseLocker.lock();
        queue.offer(van);
        emptyQueue.signalAll();
        baseLocker.unlock();
    }

    public Van getVanFromQueue() {
        Van van = null;
        baseLocker.lock();
        try {
            while (queue.getSize() < 1) {
                emptyQueue.await();
            }
           return queue.poll();
        } catch (InterruptedException e) {
         LOGGER.error("Impossible get Van because " + e.getMessage());
         return null;
        } finally {
            baseLocker.unlock();
        }
    }

    public boolean queueIsEmpty() {
        return queue.getSize() == 0;
    }


}

