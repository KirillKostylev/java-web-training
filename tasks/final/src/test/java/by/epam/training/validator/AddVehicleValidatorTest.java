package by.epam.training.validator;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;

import static org.mockito.Mockito.when;

public class AddVehicleValidatorTest {
    private static Cookie[] cookies = {new Cookie("language","ru_Ru")};

    @Test
    public void validateWithoutError() throws IOException, ServletException {
        //prepare
        AddVehicleValidator validator = new AddVehicleValidator();
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        Part part = Mockito.mock(Part.class);
        when(part.getSubmittedFileName()).thenReturn("");

        when(req.getParameter("vehicle.tariff")).thenReturn("2.25");
        when(req.getParameter("vehicle.location")).thenReturn("Minsk");
        when(req.getParameter("vehicle.description")).thenReturn("Sdfsdkjfhsdlbf asdhfaskdf");
        when(req.getPart("vehicle.photo")).thenReturn(part);
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();

        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }

    @Test
    public void validateWithIncorrectFileExpansion() throws IOException, ServletException {
        //prepare
        AddVehicleValidator validator = new AddVehicleValidator();
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        Part part = Mockito.mock(Part.class);
        when(part.getSubmittedFileName()).thenReturn("img_0.exe");

        when(req.getParameter("vehicle.tariff")).thenReturn("2.25");
        when(req.getParameter("vehicle.location")).thenReturn("Minsk");
        when(req.getParameter("vehicle.description")).thenReturn("Sdfsdkjfhsdlbf asdhfaskdf");
        when(req.getPart("vehicle.photo")).thenReturn(part);
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        expectedVR.addError(ErrorKey.INCORRECT_TYPE_FILE, "Неверное расширение файла. Вы можете загрузить файл с расширением только png и jpg");

        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }

    @Test
    public void validateWithIncorrectTariff() throws IOException, ServletException {
        //prepare
        AddVehicleValidator validator = new AddVehicleValidator();
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        Part part = Mockito.mock(Part.class);
        when(part.getSubmittedFileName()).thenReturn("");

        when(req.getParameter("vehicle.tariff")).thenReturn("wsef");
        when(req.getParameter("vehicle.location")).thenReturn("Minsk");
        when(req.getParameter("vehicle.description")).thenReturn("Sdfsdkjfhsdlbf asdhfaskdf");
        when(req.getPart("vehicle.photo")).thenReturn(part);
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        expectedVR.addError(ErrorKey.INCORRECT_TARIFF, "Некорректная сумма. Число должно содержать только цифры, для выделения дробной части используется точка '.'");

        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }

    @Test
    public void validateWithIncorrectTariff2() throws IOException, ServletException {
        //prepare
        AddVehicleValidator validator = new AddVehicleValidator();
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        Part part = Mockito.mock(Part.class);
        when(part.getSubmittedFileName()).thenReturn("");

        when(req.getParameter("vehicle.tariff")).thenReturn("2.251");
        when(req.getParameter("vehicle.location")).thenReturn("Minsk");
        when(req.getParameter("vehicle.description")).thenReturn("Sdfsdkjfhsdlbf asdhfaskdf");
        when(req.getPart("vehicle.photo")).thenReturn(part);
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        expectedVR.addError(ErrorKey.INCORRECT_TARIFF, "Некорректная сумма. Число должно содержать только цифры, для выделения дробной части используется точка '.'");

        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }

    @Test
    public void validateWithEmptyField() throws IOException, ServletException {
        //prepare
        AddVehicleValidator validator = new AddVehicleValidator();
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        Part part = Mockito.mock(Part.class);
        when(part.getSubmittedFileName()).thenReturn("");

        when(req.getParameter("vehicle.tariff")).thenReturn("2.251");
        when(req.getParameter("vehicle.location")).thenReturn("");
        when(req.getParameter("vehicle.description")).thenReturn("Sdfsdkjfhsdlbf asdhfaskdf");
        when(req.getPart("vehicle.photo")).thenReturn(part);
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        expectedVR.addError(ErrorKey.EMPTY_FIELD, "Не все поля заполнены.");

        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }
}