package by.epam.training.service.impl;

import by.epam.training.dao.ContactDao;
import by.epam.training.dao.impl.*;
import by.epam.training.service.dto.*;
import by.epam.training.dao.*;
import by.epam.training.dao.entity.OrderStatus;
import by.epam.training.dao.entity.UserRoles;
import by.epam.training.dao.entity.VehicleType;
import by.epam.training.service.ServiceException;
import by.epam.training.dao.UserDao;
import by.epam.training.util.MD5Util;
import by.epam.training.util.UtilityMethods;
import by.epam.training.dao.VehicleDao;
import by.epam.training.dao.WalletDao;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.List;

public class OrderServiceImplTest {
    private static OrderServiceImpl orderServiceImpl;
    private static UserServiceImpl userServiceImpl;
    private static VehicleServiceImpl vehicleServiceImpl;
    private static ConnectionPool connectionPool;

    @BeforeClass
    public static void init() throws ConnectionPoolException {
        connectionPool = ConnectionPoolImpl.getInstance();
        connectionPool.init("database");
        TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
        ConnectionManager connectionManager = new ConnectionManager(transactionManager);
        UserDao userDao = new UserDaoImpl(connectionManager);
        ContactDao contactDao = new ContactDaoImpl(connectionManager);
        WalletDao walletDao = new WalletDaoImpl(connectionManager);
        VehicleDao vehicleDao = new VehicleDaoImpl(connectionManager);
        OrderDao orderDao = new OrderDaoImpl(connectionManager);
        orderServiceImpl = new OrderServiceImpl(connectionManager, orderDao, userDao, contactDao, walletDao, vehicleDao);
        vehicleServiceImpl = new VehicleServiceImpl(connectionManager, vehicleDao, userDao, contactDao, walletDao);
        userServiceImpl = new UserServiceImpl(connectionManager, userDao, contactDao, walletDao, vehicleDao, orderDao);
    }


    @AfterClass
    public static void destroy() throws ConnectionPoolException {
        connectionPool.destroy();
    }

    @Test
    public void getOrdersByVehicle() throws ServiceException, TransactionException {
        //prepare
        VehicleDto vehicleDto = new VehicleDto();
        vehicleDto.setId(146);


        //actual
        List<OrderDto> orderList = orderServiceImpl.getOrdersByVehicle(vehicleDto);

        //assert
        Assert.assertTrue(orderList.size() > 0);
    }

    @Test
    public void getOrdersByCustomer() throws ServiceException, TransactionException {
        //prepare
        UserDto customer = new UserDto();
        customer.setId(86);

        //actual
        List<OrderDto> orderList = orderServiceImpl.getOrdersByCustomer(customer);

        //assert
        Assert.assertTrue( orderList.size()>0);
    }

    @Test
    public void updateStatus() throws ServiceException, TransactionException {
        //actual
        boolean isUpdated = orderServiceImpl.updateStatus(31L, OrderStatus.CANCELLED);
        //assert
        Assert.assertTrue(isUpdated);

        orderServiceImpl.updateStatus(31L, OrderStatus.DONE);
    }

    @Test
    public void create() throws ServiceException, TransactionException, FileNotFoundException {
        //prepare
        ContactDto contactDto = new ContactDto();
        contactDto.setFirstName("test");
        contactDto.setLastName("test");
        contactDto.setPhoneNumber("375448771123");
        contactDto.setEmail("test@gmail.com");

        UserDto actualUser = new UserDto();
        actualUser.setLogin("test1");
        actualUser.setPassword(MD5Util.md5getString("123123"));
        actualUser.setRole(UserRoles.DRIVER);
        actualUser.setRegistrationDate(new Timestamp(119, 11, 6, 15, 44, 6, 0));
        actualUser.setContactDto(contactDto);
        actualUser.setWalletDto(new WalletDto());
        userServiceImpl.create(actualUser);

        VehicleDto actualVehicleDto = new VehicleDto();
        actualVehicleDto.setUserDto(actualUser);

        InputStream is = new BufferedInputStream(
                new FileInputStream("X:\\Study\\Epam\\java-web-training\\tasks\\final\\src\\main\\webapp\\img\\drivers\\img_0.jpg"));
        byte[] imgInBytes = UtilityMethods.getByteArrayFromInputStream(is);
        actualVehicleDto.setPhoto(UtilityMethods.convertByteArrayToBase64(imgInBytes));
        actualVehicleDto.setLocation("Брест");
        actualVehicleDto.setDescription("Быстро и надежно");
        actualVehicleDto.setTariff(2);
        actualVehicleDto.setType(VehicleType.TRUCK);
        vehicleServiceImpl.create(actualVehicleDto);

        //actual
        OrderDto orderDto = new OrderDto();
        orderDto.setUserDto(actualUser);
        orderDto.setVehicleDto(actualVehicleDto);
        orderDto.setStatus(OrderStatus.ORDERED);
        orderDto.setLoadingDate(new Timestamp(119, 11, 6, 15, 44, 6, 0));
        orderDto.setUnloadingDate(new Timestamp(119, 11, 6, 15, 44, 6, 0));
        orderDto.setOrderTime(new Timestamp(119, 11, 6, 15, 44, 6, 0));
        orderDto.setCost(5);
        orderDto.setUnloadingAddress("Брест Вульковская 80");
        orderDto.setLoadingAddress("Брест Вульковская 80");

        long orderId = orderServiceImpl.create(orderDto);

        //assert
        Assert.assertTrue(orderId > 0);

        //cleaning
        orderServiceImpl.delete(orderDto);
        vehicleServiceImpl.delete(actualVehicleDto);
        userServiceImpl.delete(actualUser);
    }

    @Test
    public void delete() throws ServiceException, TransactionException, FileNotFoundException {
        //prepare
        ContactDto contactDto = new ContactDto();
        contactDto.setFirstName("test");
        contactDto.setLastName("test");
        contactDto.setPhoneNumber("375448771123");
        contactDto.setEmail("test@gmail.com");

        UserDto actualUser = new UserDto();
        actualUser.setLogin("test1");
        actualUser.setPassword(MD5Util.md5getString("123123"));
        actualUser.setRole(UserRoles.DRIVER);
        actualUser.setRegistrationDate(new Timestamp(119, 11, 6, 15, 44, 6, 0));
        actualUser.setContactDto(contactDto);
        actualUser.setWalletDto(new WalletDto());
        userServiceImpl.create(actualUser);

        VehicleDto actualVehicleDto = new VehicleDto();
        actualVehicleDto.setUserDto(actualUser);

        InputStream is = new BufferedInputStream(
                new FileInputStream("X:\\Study\\Epam\\java-web-training\\tasks\\final\\src\\main\\webapp\\img\\drivers\\img_0.jpg"));
        byte[] imgInBytes = UtilityMethods.getByteArrayFromInputStream(is);
        actualVehicleDto.setPhoto(UtilityMethods.convertByteArrayToBase64(imgInBytes));
        actualVehicleDto.setLocation("Брест");
        actualVehicleDto.setDescription("Быстро и надежно");
        actualVehicleDto.setTariff(2);
        actualVehicleDto.setType(VehicleType.TRUCK);
        vehicleServiceImpl.create(actualVehicleDto);

        OrderDto orderDto = new OrderDto();
        orderDto.setUserDto(actualUser);
        orderDto.setVehicleDto(actualVehicleDto);
        orderDto.setStatus(OrderStatus.ORDERED);
        orderDto.setLoadingDate(new Timestamp(119, 11, 6, 15, 44, 6, 0));
        orderDto.setUnloadingDate(new Timestamp(119, 11, 6, 15, 44, 6, 0));
        orderDto.setOrderTime(new Timestamp(119, 11, 6, 15, 44, 6, 0));
        orderDto.setCost(5);
        orderDto.setUnloadingAddress("Брест Вульковская 80");
        orderDto.setLoadingAddress("Брест Вульковская 80");
        orderServiceImpl.create(orderDto);

        //actual
        boolean isDeleted = orderServiceImpl.delete(orderDto);

        //assert
        Assert.assertTrue(isDeleted);

        //cleaning
        vehicleServiceImpl.delete(actualVehicleDto);
        userServiceImpl.delete(actualUser);
    }
}