package by.epam.training.validator;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.when;

public class MakeOrderValidatorTest {
    private static Cookie[] cookies = {new Cookie("language", "ru_Ru")};

    @Test
    public void validate() {
        MakeOrderValidator validator = new MakeOrderValidator();
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);

        when(req.getParameter("order.loadingAddress")).thenReturn("Брест Вульковская 80");
        when(req.getParameter("order.unloadingAddress")).thenReturn("Брест Гоголя 65");
        when(req.getParameter("order.loadingDate")).thenReturn("2020-08-01T20:00");
        when(req.getParameter("order.unloadingDate")).thenReturn("2020-08-01T22:00");
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();

        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }

    @Test
    public void validateWithIncorrectDate() {
        MakeOrderValidator validator = new MakeOrderValidator();
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);

        when(req.getParameter("order.loadingAddress")).thenReturn("Брест Вульковская 80");
        when(req.getParameter("order.unloadingAddress")).thenReturn("Брест Гоголя 65");
        when(req.getParameter("order.loadingDate")).thenReturn("2020-08-01T20:00");
        when(req.getParameter("order.unloadingDate")).thenReturn("2020-08-01T10:00");
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        expectedVR.addError(ErrorKey.INCORRECT_DATE, "Некорректная дата и время.");

        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }

    @Test
    public void validateWithIncorrectDate2() {
        MakeOrderValidator validator = new MakeOrderValidator();
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);

        when(req.getParameter("order.loadingAddress")).thenReturn("Брест Вульковская 80");
        when(req.getParameter("order.unloadingAddress")).thenReturn("Брест Гоголя 65");
        when(req.getParameter("order.loadingDate")).thenReturn("2019-08-01T20:00");
        when(req.getParameter("order.unloadingDate")).thenReturn("2019-08-01T22:00");
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        expectedVR.addError(ErrorKey.INCORRECT_DATE, "Некорректная дата и время.");

        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }

    @Test
    public void validateWithIncorrectAddress() {
        MakeOrderValidator validator = new MakeOrderValidator();
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);

        when(req.getParameter("order.loadingAddress")).thenReturn("Брест Вульковская 80");
        when(req.getParameter("order.unloadingAddress")).thenReturn("oau598+asf2q   ");
        when(req.getParameter("order.loadingDate")).thenReturn("2020-08-01T20:00");
        when(req.getParameter("order.unloadingDate")).thenReturn("2020-08-01T22:00");
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        expectedVR.addError(ErrorKey.INCORRECT_ADDRESS, "Некорректный адрес");

        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }
}