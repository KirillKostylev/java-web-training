package by.epam.training.validator;

import by.epam.training.dao.ContactDao;
import by.epam.training.dao.impl.*;
import by.epam.training.dao.*;
import by.epam.training.dao.OrderDao;
import by.epam.training.dao.UserDao;
import by.epam.training.service.impl.UserServiceImpl;
import by.epam.training.dao.VehicleDao;
import by.epam.training.dao.WalletDao;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.when;

public class SignUpValidatorTest {
    private static UserServiceImpl userServiceImpl;
    private static ConnectionPool connectionPool;
    private static Cookie[] cookies = {new Cookie("language", "ru_Ru")};

    @BeforeClass
    public static void init() throws ConnectionPoolException {
        connectionPool = ConnectionPoolImpl.getInstance();
        connectionPool.init("database");
        TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
        ConnectionManager connectionManager = new ConnectionManager(transactionManager);
        UserDao userDao = new UserDaoImpl(connectionManager);
        ContactDao contactDao = new ContactDaoImpl(connectionManager);
        WalletDao walletDao = new WalletDaoImpl(connectionManager);
        VehicleDao vehicleDao = new VehicleDaoImpl(connectionManager);
        OrderDao orderDao = new OrderDaoImpl(connectionManager);
        userServiceImpl = new UserServiceImpl(connectionManager, userDao, contactDao, walletDao, vehicleDao, orderDao);
    }

    @AfterClass
    public static void destroy() throws ConnectionPoolException {
        connectionPool.destroy();
    }


    @Test
    public void validateWithoutError() {
        //prepare
        SignUpValidator validator = new SignUpValidator(userServiceImpl);
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        when(req.getParameter("user.login")).thenReturn("asdfqe");
        when(req.getParameter("user.password")).thenReturn("123123");
        when(req.getParameter("user.passwordConfirm")).thenReturn("123123");
        when(req.getParameter("user.phoneNumber")).thenReturn("375297894561");
        when(req.getParameter("user.firstName")).thenReturn("asd");
        when(req.getParameter("user.lastName")).thenReturn("asd");
        when(req.getParameter("user.role")).thenReturn("CLIENT");
        when(req.getParameter("user.email")).thenReturn("asd@asd.as");
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }

    @Test
    public void validateWithIncorrectPhoneNumber() {
        //prepare
        SignUpValidator validator = new SignUpValidator(userServiceImpl);
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        when(req.getParameter("user.login")).thenReturn("asdfqe");
        when(req.getParameter("user.password")).thenReturn("123123");
        when(req.getParameter("user.passwordConfirm")).thenReturn("123123");
        when(req.getParameter("user.phoneNumber")).thenReturn("1231");
        when(req.getParameter("user.firstName")).thenReturn("asd");
        when(req.getParameter("user.lastName")).thenReturn("asd");
        when(req.getParameter("user.role")).thenReturn("CLIENT");
        when(req.getParameter("user.email")).thenReturn("asd@asd.as");
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        expectedVR.addError(ErrorKey.INCORRECT_PHONE_NUMBER, "Номер телефон должен соответствовать шаблону 375XXXXXXXXX.");
        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }

    @Test
    public void validateWithDoNotMatchPasswords() {
        //prepare
        SignUpValidator validator = new SignUpValidator(userServiceImpl);
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        when(req.getParameter("user.login")).thenReturn("asdfqe");
        when(req.getParameter("user.password")).thenReturn("123123");
        when(req.getParameter("user.passwordConfirm")).thenReturn("sdffs");
        when(req.getParameter("user.phoneNumber")).thenReturn("375297894561");
        when(req.getParameter("user.firstName")).thenReturn("asd");
        when(req.getParameter("user.lastName")).thenReturn("asd");
        when(req.getParameter("user.role")).thenReturn("CLIENT");
        when(req.getParameter("user.email")).thenReturn("asd@asd.as");
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        expectedVR.addError(ErrorKey.PASSWORD_DO_NOT_MATCH, "Пароли не совпадают.");
        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }

    @Test
    public void validateWithBusyLogin() {
        //prepare
        SignUpValidator validator = new SignUpValidator(userServiceImpl);
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        when(req.getParameter("user.login")).thenReturn("admin");
        when(req.getParameter("user.password")).thenReturn("123123");
        when(req.getParameter("user.passwordConfirm")).thenReturn("123123");
        when(req.getParameter("user.phoneNumber")).thenReturn("375297894561");
        when(req.getParameter("user.firstName")).thenReturn("asd");
        when(req.getParameter("user.lastName")).thenReturn("asd");
        when(req.getParameter("user.role")).thenReturn("CLIENT");
        when(req.getParameter("user.email")).thenReturn("asd@asd.as");
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        expectedVR.addError(ErrorKey.LOGIN_BUSY, "Пользователь с таким логином уже существует.");
        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }

    @Test
    public void validateWithIncorrectNamesForm() {
        //prepare
        SignUpValidator validator = new SignUpValidator(userServiceImpl);
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        when(req.getParameter("user.login")).thenReturn("sdf2da");
        when(req.getParameter("user.password")).thenReturn("123123");
        when(req.getParameter("user.passwordConfirm")).thenReturn("123123");
        when(req.getParameter("user.phoneNumber")).thenReturn("375297894561");
        when(req.getParameter("user.firstName")).thenReturn("dsfwd31/* qw ");
        when(req.getParameter("user.lastName")).thenReturn("asd sad");
        when(req.getParameter("user.role")).thenReturn("CLIENT");
        when(req.getParameter("user.email")).thenReturn("asd@asd.as");
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        expectedVR.addError(ErrorKey.INCORRECT_FIRST_NAME, "Некорректное имя. Имя должно быть длиной от 3 до 20 символов букв латинского алфавита.");
        expectedVR.addError(ErrorKey.INCORRECT_LAST_NAME, "Некорректная фамилия. Фамилия должна быть длиной от 3 до 20 символов букв латинского алфавита.");
        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }
}