package by.epam.training.service.impl;

import by.epam.training.dao.ContactDao;
import by.epam.training.dao.impl.*;
import by.epam.training.service.dto.ContactDto;
import by.epam.training.dao.*;
import by.epam.training.dao.entity.UserRoles;
import by.epam.training.dao.entity.VehicleType;
import by.epam.training.dao.OrderDao;
import by.epam.training.service.ServiceException;
import by.epam.training.dao.UserDao;
import by.epam.training.service.dto.VehicleDto;
import by.epam.training.service.dto.UserDto;
import by.epam.training.util.MD5Util;
import by.epam.training.util.UtilityMethods;
import by.epam.training.dao.WalletDao;
import by.epam.training.service.dto.WalletDto;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.List;

public class VehicleServiceImplTest {
    private static ConnectionPool connectionPool;
    private static VehicleServiceImpl vehicleServiceImpl;
    private static UserServiceImpl userServiceImpl;

    @BeforeClass
    public static void init() throws ConnectionPoolException {
        connectionPool = ConnectionPoolImpl.getInstance();
        connectionPool.init("database");
        TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
        ConnectionManager connectionManager = new ConnectionManager(transactionManager);
        VehicleDao vehicleDao = new VehicleDaoImpl(connectionManager);
        UserDao userDao = new UserDaoImpl(connectionManager);
        ContactDao contactDao = new ContactDaoImpl(connectionManager);
        WalletDao walletDao = new WalletDaoImpl(connectionManager);
        OrderDao orderDao = new OrderDaoImpl(connectionManager);
        vehicleServiceImpl = new VehicleServiceImpl(connectionManager, vehicleDao, userDao, contactDao, walletDao);
        userServiceImpl = new UserServiceImpl(connectionManager, userDao, contactDao, walletDao, vehicleDao, orderDao);
    }

    @AfterClass
    public static void destroy() throws ConnectionPoolException {
        connectionPool.destroy();
    }

    @Test
    public void saveVehicle() throws ServiceException, TransactionException, FileNotFoundException {
        //prepare
        UserDto userDto = userServiceImpl.getUserByLogin("test");
        VehicleDto actualVehicleDto = new VehicleDto();
        actualVehicleDto.setUserDto(userDto);
        InputStream is = new BufferedInputStream(
                new FileInputStream("X:\\Study\\Epam\\java-web-training\\tasks\\final\\src\\main\\webapp\\img\\drivers\\img_0.jpg"));
        byte[] imgInBytes = UtilityMethods.getByteArrayFromInputStream(is);
        actualVehicleDto.setPhoto(UtilityMethods.convertByteArrayToBase64(imgInBytes));
        actualVehicleDto.setLocation("Брест");
        actualVehicleDto.setDescription("Быстро и надежно");
        actualVehicleDto.setTariff(2.25);
        actualVehicleDto.setType(VehicleType.TRUCK);

        //actual
        long vehicleId = vehicleServiceImpl.create(actualVehicleDto);

        //assert
        Assert.assertTrue(vehicleId > 0);

        //cleaning trash
        vehicleServiceImpl.delete(actualVehicleDto);
    }

    @Test
    public void getVehicleByUserId() throws ServiceException, TransactionException, FileNotFoundException {
        // prepare
        ContactDto contactDto = new ContactDto();
        contactDto.setFirstName("test");
        contactDto.setLastName("test");
        contactDto.setPhoneNumber("375448771123");
        contactDto.setEmail("test@gmail.com");

        UserDto actualUser = new UserDto();
        actualUser.setLogin("test1");
        actualUser.setPassword(MD5Util.md5getString("123123"));
        actualUser.setRole(UserRoles.DRIVER);
        actualUser.setRegistrationDate(new Timestamp(119, 11, 6, 15, 44, 6, 0));
        actualUser.setContactDto(contactDto);
        actualUser.setWalletDto(new WalletDto());
        userServiceImpl.create(actualUser);

        VehicleDto expectVehicle = new VehicleDto();
        expectVehicle.setUserDto(actualUser);

        InputStream is = new BufferedInputStream(
                new FileInputStream("X:\\Study\\Epam\\java-web-training\\tasks\\final\\src\\main\\webapp\\img\\drivers\\img_0.jpg"));
        byte[] imgInBytes = UtilityMethods.getByteArrayFromInputStream(is);
        expectVehicle.setPhoto(UtilityMethods.convertByteArrayToBase64(imgInBytes));
        expectVehicle.setLocation("Брест");
        expectVehicle.setDescription("Быстро и надежно");
        expectVehicle.setTariff(2);
        expectVehicle.setType(VehicleType.TRUCK);
        expectVehicle.setId(vehicleServiceImpl.create(expectVehicle));
        //actual
        VehicleDto actualVehicle = vehicleServiceImpl.getVehicleByUser(actualUser);

        //assert
        Assert.assertEquals(expectVehicle, actualVehicle);

        vehicleServiceImpl.delete(expectVehicle);
        userServiceImpl.delete(actualUser);
    }

    @Test
    public void deleteVehicleByIdTest() throws ServiceException, TransactionException, FileNotFoundException {
        //prepare
        UserDto userDto = userServiceImpl.getUserByLogin("test");
        VehicleDto actualVehicleDto = new VehicleDto();
        actualVehicleDto.setUserDto(userDto);
        InputStream is = new BufferedInputStream(
                new FileInputStream("X:\\Study\\Epam\\java-web-training\\tasks\\final\\src\\main\\webapp\\img\\drivers\\img_0.jpg"));
        byte[] imgInBytes = UtilityMethods.getByteArrayFromInputStream(is);
        actualVehicleDto.setPhoto(UtilityMethods.convertByteArrayToBase64(imgInBytes));
        actualVehicleDto.setLocation("Брест");
        actualVehicleDto.setDescription("Быстро и надежно");
        actualVehicleDto.setTariff(2.25);
        actualVehicleDto.setType(VehicleType.TRUCK);
        long vehicleId = vehicleServiceImpl.create(actualVehicleDto);

        //actual
        boolean isDeleted = vehicleServiceImpl.delete(actualVehicleDto);

        //assert
        Assert.assertTrue(isDeleted);
    }

    @Test
    public void updateTest() throws ServiceException, TransactionException, FileNotFoundException {
        //prepare
        UserDto userDto = userServiceImpl.getUserByLogin("test");
        VehicleDto actualVehicleDto = new VehicleDto();
        actualVehicleDto.setUserDto(userDto);
        InputStream is = new BufferedInputStream(
                new FileInputStream("X:\\Study\\Epam\\java-web-training\\tasks\\final\\src\\main\\webapp\\img\\drivers\\img_0.jpg"));
        byte[] imgInBytes = UtilityMethods.getByteArrayFromInputStream(is);
        actualVehicleDto.setPhoto(UtilityMethods.convertByteArrayToBase64(imgInBytes));
        actualVehicleDto.setLocation("Брест");
        actualVehicleDto.setDescription("Быстро и надежно");
        actualVehicleDto.setTariff(2.25);
        actualVehicleDto.setType(VehicleType.TRUCK);
        long vehicleId = vehicleServiceImpl.create(actualVehicleDto);

        actualVehicleDto.setLocation("Минск");

        //actual
        boolean isUpdated = vehicleServiceImpl.update(actualVehicleDto);

        //assert
        Assert.assertTrue(isUpdated);

        vehicleServiceImpl.delete(actualVehicleDto);
    }

    @Test
    public void getAll() throws ServiceException {
        List<VehicleDto> vehicleList = vehicleServiceImpl.getAll();

        //assert
        Assert.assertTrue(vehicleList.size() > 0);
    }
}