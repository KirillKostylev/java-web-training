package by.epam.training.validator;

import by.epam.training.dao.ContactDao;
import by.epam.training.dao.impl.*;
import by.epam.training.dao.*;
import by.epam.training.dao.OrderDao;
import by.epam.training.dao.UserDao;
import by.epam.training.service.impl.UserServiceImpl;
import by.epam.training.dao.VehicleDao;
import by.epam.training.dao.WalletDao;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class SignInValidatorTest {
    private static UserServiceImpl userServiceImpl;
    private static ConnectionPool connectionPool;
    private static Cookie[] cookies = {new Cookie("language", "ru_Ru")};

    @BeforeClass
    public static void init() throws ConnectionPoolException {
        connectionPool = ConnectionPoolImpl.getInstance();
        connectionPool.init("database");
        TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
        ConnectionManager connectionManager = new ConnectionManager(transactionManager);
        UserDao userDao = new UserDaoImpl(connectionManager);
        ContactDao contactDao = new ContactDaoImpl(connectionManager);
        WalletDao walletDao = new WalletDaoImpl(connectionManager);
        VehicleDao vehicleDao = new VehicleDaoImpl(connectionManager);
        OrderDao orderDao = new OrderDaoImpl(connectionManager);
        userServiceImpl = new UserServiceImpl(connectionManager, userDao, contactDao, walletDao, vehicleDao, orderDao);
    }

    @AfterClass
    public static void destroy() throws ConnectionPoolException {
        connectionPool.destroy();
    }

    @Test
    public void validateWithoutError() {
        //prepare
        SignInValidator validator = new SignInValidator(userServiceImpl);
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        when(req.getParameter("user.login")).thenReturn("admin");
        when(req.getParameter("user.password")).thenReturn("123123");
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();

        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }

    @Test
    public void validateWithWrongLogin() {
        //prepare
        SignInValidator validator = new SignInValidator(userServiceImpl);
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        when(req.getParameter("user.login")).thenReturn("asdfqfvrwasd");
        when(req.getParameter("user.password")).thenReturn("123123");
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        expectedVR.addError(ErrorKey.INCORRECT_LOGIN_OR_PASSWORD, "Неверный логин или пароль.");

        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }

    @Test
    public void validateWithWrongPassword() {
        //prepare
        SignInValidator validator = new SignInValidator(userServiceImpl);
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        when(req.getParameter("user.login")).thenReturn("KirillKostylev");
        when(req.getParameter("user.password")).thenReturn("jkhnugyt");
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        expectedVR.addError(ErrorKey.INCORRECT_LOGIN_OR_PASSWORD, "Неверный логин или пароль.");

        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }

    @Test
    public void validateWithIncorrectFormatPassword() {
        //prepare
        SignInValidator validator = new SignInValidator(userServiceImpl);
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        when(req.getParameter("user.login")).thenReturn("KirillKostylev");
        when(req.getParameter("user.password")).thenReturn("/*sd-+fsae");
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        expectedVR.addError(ErrorKey.INCORRECT_FORMAT_PASSWORD, "Некорректный пароль. Пароль должен быть длиной от 3 до 20 символов и состоять только из цифр и букв латинского алфавита.");

        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }

    @Test
    public void validateWithIncorrectFormatLogin() {
        //prepare
        SignInValidator validator = new SignInValidator(userServiceImpl);
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        when(req.getParameter("user.login")).thenReturn("/*-*asdf*-/qq12 asd/ *-qw");
        when(req.getParameter("user.password")).thenReturn("123123");
        when(req.getCookies()).thenReturn(cookies);

        //actual
        ValidationResult actualVR = validator.validate(req);

        //expected
        ValidationResult expectedVR = new ValidationResult();
        expectedVR.addError(ErrorKey.INCORRECT_FORMAT_LOGIN, "Некорректный логин. Логин должен быть длиной от 3 до 20 символов и состоять только из цифр и букв латинского алфавита.");

        //assert
        Assert.assertEquals(expectedVR, actualVR);
    }
}