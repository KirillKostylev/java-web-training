package by.epam.training.service.impl;

import by.epam.training.dao.ContactDao;
import by.epam.training.dao.impl.*;
import by.epam.training.service.dto.ContactDto;
import by.epam.training.dao.*;
import by.epam.training.dao.entity.UserRoles;
import by.epam.training.dao.OrderDao;
import by.epam.training.service.ServiceException;
import by.epam.training.service.dto.UserDto;
import by.epam.training.util.MD5Util;
import by.epam.training.dao.VehicleDao;
import by.epam.training.dao.WalletDao;
import by.epam.training.service.dto.WalletDto;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.List;

public class UserServiceImplTest {
    private static UserServiceImpl userServiceImpl;
    private static ConnectionPool connectionPool;

    @BeforeClass
    public static void init() throws ConnectionPoolException {
        connectionPool = ConnectionPoolImpl.getInstance();
        connectionPool.init("database");
        TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
        ConnectionManager connectionManager = new ConnectionManager(transactionManager);
        UserDao userDao = new UserDaoImpl(connectionManager);
        ContactDao contactDao = new ContactDaoImpl(connectionManager);
        WalletDao walletDao = new WalletDaoImpl(connectionManager);
        VehicleDao vehicleDao = new VehicleDaoImpl(connectionManager);
        OrderDao orderDao = new OrderDaoImpl(connectionManager);
        userServiceImpl = new UserServiceImpl(connectionManager, userDao, contactDao, walletDao, vehicleDao, orderDao);
    }

    @AfterClass
    public static void destroy() throws ConnectionPoolException {
        connectionPool.destroy();
    }

    @Test
    public void create() throws ServiceException, TransactionException {
        //prepare
        ContactDto contactDto = new ContactDto();
        contactDto.setFirstName("James");
        contactDto.setLastName("Brown");
        contactDto.setPhoneNumber("375448771123");
        contactDto.setEmail("jamesBrown@gmail.com");

        UserDto actualUser = new UserDto();
        actualUser.setLogin("JamesBrown");
        actualUser.setPassword(MD5Util.md5getString("123123"));
        actualUser.setRole(UserRoles.DRIVER);
        actualUser.setRegistrationDate(new Timestamp(119, 11, 6, 15, 44, 6, 0));
        actualUser.setContactDto(contactDto);
        actualUser.setWalletDto(new WalletDto());

        //actual
        long userId = userServiceImpl.create(actualUser);

        //assert
        Assert.assertTrue(userId > 0);


        //cleaning trash
        userServiceImpl.delete(actualUser);
    }

    @Test
    public void getUserByLoginWithoutError() throws ServiceException, TransactionException {
        //expected
        ContactDto contactDto = new ContactDto();
        contactDto.setEmail("kirillkostylev2000@gmail.com");
        contactDto.setFirstName("Kirill");
        contactDto.setLastName("Kostylev");
        contactDto.setPhoneNumber("8077559");
        contactDto.setId(16);

        UserDto expectedUser = new UserDto();
        expectedUser.setLogin("admin");
        expectedUser.setId(14);
        expectedUser.setPassword(MD5Util.md5getString("123123"));
        expectedUser.setRole(UserRoles.ADMIN);
        expectedUser.setRegistrationDate(new Timestamp(119, 11, 6, 15, 44, 6, 0));
        expectedUser.setContactDto(contactDto);
        expectedUser.setWalletDto(new WalletDto(17, 0));

        //actual
        UserDto actualUser = userServiceImpl.getUserByLogin("admin");

        //assert
        Assert.assertEquals(expectedUser, actualUser);
    }

    @Test
    public void getUserByLoginWithNonexistentLogin() throws ServiceException, TransactionException {
        //actual
        UserDto actualUser = userServiceImpl.getUserByLogin("Kidafqw898512ouiyb");

        //expected
        UserDto expectedUser = null;

        //assert
        Assert.assertEquals(expectedUser, actualUser);
    }

    @Test
    public void getAllUsers() throws ServiceException {
        //actual
        List<UserDto> actualUsers = userServiceImpl.getAll();

        //assert
        Assert.assertFalse(actualUsers.isEmpty());
    }

    @Test
    public void deleteUser() throws ServiceException, TransactionException {
        //prepare
        ContactDto contactDto = new ContactDto();
        contactDto.setFirstName("James");
        contactDto.setLastName("Brown");
        contactDto.setPhoneNumber("375448771123");
        contactDto.setEmail("jamesBrown@gmail.com");

        UserDto actualUser = new UserDto();
        actualUser.setLogin("JamesBrown");
        actualUser.setPassword(MD5Util.md5getString("123123"));
        actualUser.setRole(UserRoles.DRIVER);
        actualUser.setRegistrationDate(new Timestamp(119, 11, 6, 15, 44, 6, 0));
        actualUser.setContactDto(contactDto);
        actualUser.setWalletDto(new WalletDto());
        long userId = userServiceImpl.create(actualUser);

        //actual
        boolean isDeleted = userServiceImpl.delete(actualUser);

        //assert
        Assert.assertTrue(isDeleted);

    }
}
