package by.epam.training.dao.entity;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class OrderEntity {
    long id;
    Timestamp orderTime;
    Timestamp loadingDate;
    Timestamp unloadingDate;
    String loadingAddress;
    String unloadingAddress;
    long vehicleId;
    long customerId;
    double cost;
    OrderStatus status;

}
