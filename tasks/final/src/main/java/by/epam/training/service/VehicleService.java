package by.epam.training.service;

import by.epam.training.dao.TransactionException;
import by.epam.training.service.dto.UserDto;
import by.epam.training.service.dto.VehicleDto;

public interface VehicleService extends Service<VehicleDto, Long> {
    VehicleDto getVehicleByUser(UserDto userDto) throws TransactionException, ServiceException;
}
