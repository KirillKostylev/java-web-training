package by.epam.training.dao;

import by.epam.training.service.dto.UserDto;
import by.epam.training.service.dto.VehicleDto;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface VehicleDao extends CRUDdao<VehicleDto, Long> {
    Optional<VehicleDto> getVehicleByUser(UserDto userDto) throws DaoException;

    Optional<List<VehicleDto>> getAllTransport(UserDao userDao, ContactDao contactDao, WalletDao walletDao) throws DaoException;

    VehicleDto getById(Long id, UserDao userDao, ContactDao contactDao, WalletDao walletDao) throws SQLException;
}
