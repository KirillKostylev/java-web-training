package by.epam.training.service.dto;

import lombok.Data;

@Data
public class WalletDto {
    private long id;
    private double count;

    public WalletDto(long id, double count) {
        this.id = id;
        this.count = count;
    }

    public WalletDto() {
        this.count = 0;
    }
}
