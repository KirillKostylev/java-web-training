package by.epam.training.validator;

public enum ErrorKey {
    EMPTY_FIELD,
    INCORRECT_LOGIN_OR_PASSWORD,
    INCORRECT_FORMAT_LOGIN,
    INCORRECT_FORMAT_PASSWORD,
    INCORRECT_PHONE_NUMBER,
    INCORRECT_FIRST_NAME,
    INCORRECT_LAST_NAME,
    PASSWORD_DO_NOT_MATCH,
    LOGIN_BUSY,
    INCORRECT_TARIFF,
    INCORRECT_DESCRIPTION,
    INCORRECT_LOCATION,
    INCORRECT_TYPE_FILE,
    INCORRECT_DATE,
    INCORRECT_ADDRESS,
    INCORRECT_PHOTO_SIZE
}
