package by.epam.training.validator;

import by.epam.training.filter.LocaleFilter;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import static by.epam.training.util.UtilityMethods.getValueFromCookies;
import static by.epam.training.util.UtilityMethods.stringIsEmpty;

@Log4j
public class AddVehicleValidator implements Validator {

    private static final String START_MSG = AddVehicleValidator.class + " was executed. ";
    private static final String VEHICLE_TARIFF_PATH = "vehicle.tariff";
    private static final String VEHICLE_LOCATION_PATH = "vehicle.location";
    private static final String REGEX_TO_MONEY = "^[0-9]+(\\.[0-9]{1,2})?$";
    private static final String REGEX_TO_LOCATION = "\\A(.|\\n){2,100}\\Z";
    private static final String REGEX_TO_DESCRIPTION = "\\A(.|\\n){5,1000}\\Z";
    private static final String INCORRECT_TARIFF_PATH = "locale.error.incorrect.money";
    private static final String INCORRECT_LOCATION_PATH = "locale.error.incorrect.location";
    private static final String INCORRECT_DESCRIPTION_PATH = "locale.error.incorrect.description";
    private static final String VEHICLE_DESCRIPTION_PATH = "vehicle.description";
    private static final String VEHICLE_PHOTO_PATH = "vehicle.photo";
    private static final String CANNOT_GET_PHOTO_FROM_REQUEST = "Cannot get photo from request.";
    private static final String LOCALE_ERROR_INCORRECT_FILE_TYPE = "locale.error.incorrect.file.type";
    private static final int MAX_PHOTO_SIZE = 64 * 1024;
    private static final String LOCALE_ERROR_INCORRECT_FILE_SIZE = "locale.error.incorrect.file.size";

    @Override
    public ValidationResult validate(HttpServletRequest request) {
        log.debug(START_MSG);
        ValidationResult validationResult = new ValidationResult();

        String locale = getValueFromCookies(request, LocaleFilter.COOKIE_NAME);
        String language = locale.substring(0, 2);
        String country = locale.substring(3);
        ResourceBundle bundle = ResourceBundle.getBundle(PATH_TO_RESOURCE_BUNDLE, new Locale(language, country));

        String tariff = request.getParameter(VEHICLE_TARIFF_PATH);
        String location = request.getParameter(VEHICLE_LOCATION_PATH);
        String description = request.getParameter(VEHICLE_DESCRIPTION_PATH);
        Part photoPart = null;
        try {
            photoPart = request.getPart(VEHICLE_PHOTO_PATH);
        } catch (IOException | ServletException e) {
            log.error(CANNOT_GET_PHOTO_FROM_REQUEST);
        }
        if (photoPart == null) {
            validationResult.addError(ErrorKey.EMPTY_FIELD, bundle.getString(EMPTY_FIELD_MESSAGE));
            return validationResult;
        }
        String fileName = photoPart.getSubmittedFileName();
        boolean isJpg = fileName.endsWith(".jpg");
        boolean isPng = fileName.endsWith(".png");
        long photoSize = photoPart.getSize();


        if (photoSize > MAX_PHOTO_SIZE) {
            validationResult.addError(ErrorKey.INCORRECT_PHOTO_SIZE, bundle.getString(LOCALE_ERROR_INCORRECT_FILE_SIZE));
        }

        if (!(isJpg || isPng || fileName.equals(""))) {
            validationResult.addError(ErrorKey.INCORRECT_TYPE_FILE, bundle.getString(LOCALE_ERROR_INCORRECT_FILE_TYPE));
        }

        if (stringIsEmpty(tariff) || stringIsEmpty(location) || stringIsEmpty(description)) {
            validationResult.addError(ErrorKey.EMPTY_FIELD, bundle.getString(EMPTY_FIELD_MESSAGE));
            return validationResult;
        }
        if (!tariff.matches(REGEX_TO_MONEY)) {
            validationResult.addError(ErrorKey.INCORRECT_TARIFF, bundle.getString(INCORRECT_TARIFF_PATH));
        }
        if (!location.matches(REGEX_TO_LOCATION)) {
            validationResult.addError(ErrorKey.INCORRECT_TARIFF, bundle.getString(INCORRECT_LOCATION_PATH));
        }
        if (!location.matches(REGEX_TO_DESCRIPTION)) {
            validationResult.addError(ErrorKey.INCORRECT_TARIFF, bundle.getString(INCORRECT_DESCRIPTION_PATH));
        }

        return validationResult;
    }
}
