package by.epam.training.dao;

import java.sql.Connection;

public interface ConnectionPool {
    void init(String dbPropertiesName) throws ConnectionPoolException;

    void destroy() throws ConnectionPoolException;

    Connection getConnection();
}
