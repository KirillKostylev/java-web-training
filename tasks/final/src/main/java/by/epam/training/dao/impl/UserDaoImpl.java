package by.epam.training.dao.impl;

import by.epam.training.dao.ContactDao;
import by.epam.training.service.dto.ContactDto;
import by.epam.training.dao.ConnectionManager;
import by.epam.training.dao.DaoException;
import by.epam.training.dao.entity.UserEntity;
import by.epam.training.dao.entity.UserRoles;
import by.epam.training.dao.UserDao;
import by.epam.training.service.dto.UserDto;
import by.epam.training.dao.WalletDao;
import by.epam.training.service.dto.WalletDto;
import lombok.extern.log4j.Log4j;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j
public class UserDaoImpl implements UserDao {
    private static final String INSERT_QUERY =
            "INSERT INTO user_account(user_role, login, password, registration_date, contact_id, wallet_id) " +
                    "VALUES(?,?,?,?,?,?)";
    private static final String FIND_USER_BY_LOGIN_QUERY =
            "SELECT id, user_role, login, password, registration_date, contact_id, wallet_id FROM user_account " +
                    "WHERE login = ? ;";
    private static final String FIND_USER_BY_ID_QUERY =
            "SELECT id, user_role, login, password, registration_date, contact_id, wallet_id FROM user_account " +
                    "WHERE id = ?;";
    private static final String GET_ALL_USERS_QUERY =
            "SELECT id, user_role, login, password, registration_date, contact_id, wallet_id FROM user_account ";
    private static final String DELETE_USER_BY_LOGIN_QUERY =
            "DELETE user_account, contact , wallet FROM  user_account, contact, wallet " +
                    "WHERE user_account.contact_id = contact.id AND user_account.wallet_id = wallet.id AND user_account.id = ?;";
    private static final String CANNOT_GET_ALL_USERS = "Cannot get info about all users. ";
    private static final String CANNOT_DELETE_USER = "Cannot delete user. ";
    private static final String CANNOT_CREATE_USER_ACCOUNT = "Cannot create user account. ";
    private static final String ID_USER_NOT_FOUND = "-id user not found.";
    private static final String COULD_NOT_GET_USER_BY_ID = "Could not get user by id ";
    private static final String WAS_ADDED_IN_DATABASE = " was added in database.";

    private ConnectionManager connectionManager;

    public UserDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Long save(UserDto userDto) throws DaoException {
        UserEntity entity = fromDto(userDto);
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement insertStmt = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            insertStmt.setString(++i, entity.getRole().getValue());
            insertStmt.setString(++i, entity.getLogin());
            insertStmt.setString(++i, entity.getPassword());
            insertStmt.setTimestamp(++i, entity.getRegistrationDate());
            insertStmt.setLong(++i, entity.getContactId());
            insertStmt.setLong(++i, entity.getWalletId());
            insertStmt.executeUpdate();
            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
                userDto.setId(generatedKeys.getLong(1));
            }
            log.debug(entity + WAS_ADDED_IN_DATABASE);
        } catch (SQLException e) {
            log.error(CANNOT_CREATE_USER_ACCOUNT + e);
            throw new DaoException(CANNOT_CREATE_USER_ACCOUNT, e);
        }
        return entity.getId();
    }

    @Override
    public boolean update(UserDto userDto) {
        return false;
    }


    @Override
    public boolean delete(Long userId) throws DaoException {
        boolean isDeleted = false;
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement delStmt = connection.prepareStatement(DELETE_USER_BY_LOGIN_QUERY)) {
            delStmt.setLong(1, userId);
            isDeleted = delStmt.executeUpdate() > 0;
        } catch (SQLException e) {
            log.error(CANNOT_DELETE_USER + e);
            throw new DaoException(CANNOT_DELETE_USER, e);
        }
        return isDeleted;
    }

    @Override
    public UserDto getById(Long id) throws SQLException {
        return null;
    }

    public Optional<UserDto> getById(Long id, WalletDao walletDao, ContactDao contactDao) throws DaoException {
        UserEntity result = null;
        Optional<UserDto> userDto;
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement selectStmt = connection.prepareStatement(FIND_USER_BY_ID_QUERY)) {
            selectStmt.setLong(1, id);

            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                result = parseResultSet(resultSet);
            }
            if (result == null) {
                log.debug(id + ID_USER_NOT_FOUND);
                return Optional.empty();
            }
            WalletDto walletDto = walletDao.getById(result.getWalletId());
            ContactDto contactDto = contactDao.getById(result.getContactId());
            UserDto tempDto = fromEntity(result, contactDto, walletDto);
            userDto = Optional.of(tempDto);

        } catch (SQLException e) {
            log.error(COULD_NOT_GET_USER_BY_ID + id + e);
            throw new DaoException(COULD_NOT_GET_USER_BY_ID + id, e);
        }

        return userDto;
    }

    @Override
    public Optional<List<UserDto>> findAll(ContactDao contactDao, WalletDao walletDao) throws DaoException {
        Optional<List<UserDto>> users;
        List<UserDto> result = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement selectStmt = connection.prepareStatement(GET_ALL_USERS_QUERY)) {
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserEntity tempUserEntity = parseResultSet(resultSet);
                WalletDto walletDto = walletDao.getById(tempUserEntity.getWalletId());
                ContactDto contactDto = contactDao.getById(tempUserEntity.getContactId());
                UserDto tempDto = fromEntity(tempUserEntity, contactDto, walletDto);
                result.add(tempDto);
            }
            users = Optional.of(result);
        } catch (SQLException e) {
            log.error(CANNOT_GET_ALL_USERS, e);
            throw new DaoException(CANNOT_GET_ALL_USERS, e);
        }
        return users;
    }


    @Override
    public Optional<UserDto> findByLogin(String login, ContactDao contactDao, WalletDao walletDao) throws DaoException {
        UserEntity result = null;
        Optional<UserDto> userDto;
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement selectStmt = connection.prepareStatement(FIND_USER_BY_LOGIN_QUERY)) {
            selectStmt.setString(1, login);

            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                result = parseResultSet(resultSet);
            }
            if (result == null) {
                log.debug("User with login: " + login + " not found. ");
                return Optional.empty();
            }
            WalletDto walletDto = walletDao.getById(result.getWalletId());
            ContactDto contactDto = contactDao.getById(result.getContactId());
            UserDto tempDto = fromEntity(result, contactDto, walletDto);
            userDto = Optional.of(tempDto);

        } catch (SQLException e) {
            log.error("Could not get user by login:" + e);
            throw new DaoException("Could not get user by login", e);
        }

        return userDto;
    }

    private UserEntity fromDto(UserDto dto) {
        UserEntity entity = new UserEntity();
        entity.setId(dto.getId());
        entity.setLogin(dto.getLogin());
        entity.setPassword(dto.getPassword());
        entity.setRole(dto.getRole());
        entity.setRegistrationDate(dto.getRegistrationDate());
        entity.setWalletId(dto.getWalletDto().getId());
        entity.setContactId(dto.getContactDto().getId());
        return entity;
    }

    private UserDto fromEntity(UserEntity entity, ContactDto contactDto, WalletDto walletDto) {
        UserDto dto = new UserDto();
        dto.setId(entity.getId());
        dto.setRole(entity.getRole());
        dto.setPassword(entity.getPassword());
        dto.setLogin(entity.getLogin());
        dto.setRegistrationDate(entity.getRegistrationDate());
        dto.setContactDto(contactDto);
        dto.setWalletDto(walletDto);
        return dto;
    }

    private UserEntity parseResultSet(ResultSet resultSet) throws SQLException {
        UserEntity userEntity = new UserEntity();

        long entityId = resultSet.getLong("id");
        String login = resultSet.getString("login");
        UserRoles role = UserRoles.valueOf(resultSet.getString("user_role").toUpperCase());
        String password = resultSet.getString("password");
        Timestamp registrationDate = resultSet.getTimestamp("registration_date");
        long contactId = resultSet.getLong("contact_id");
        long walletId = resultSet.getLong("wallet_id");

        userEntity.setId(entityId);
        userEntity.setRole(role);
        userEntity.setLogin(login);
        userEntity.setPassword(password);
        userEntity.setRegistrationDate(registrationDate);
        userEntity.setContactId(contactId);
        userEntity.setWalletId(walletId);

        return userEntity;
    }
}

