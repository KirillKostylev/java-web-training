package by.epam.training.core;

import by.epam.training.command.ServletCommand;
import by.epam.training.command.impl.*;
import by.epam.training.dao.*;
import by.epam.training.dao.impl.*;
import by.epam.training.service.OrderService;
import by.epam.training.service.UserService;
import by.epam.training.service.VehicleService;
import by.epam.training.service.impl.OrderServiceImpl;
import by.epam.training.service.impl.UserServiceImpl;
import by.epam.training.service.impl.VehicleServiceImpl;
import lombok.extern.log4j.Log4j;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import static by.epam.training.command.CommandName.*;

@Log4j
public class ApplicationContext {
    private static final String APP_CONTEXT_INIT_MESSAGE = "ApplicationContext was initialized. ";
    private static final String CONNECTION_POOL_DESTROY_ERROR = "Cannot destroy connection pool. ";
    private static final String CONNECTION_POOL_INITIALIZATION_ERROR = "Connection pool initialization error. ";
    public static final String ERROR_PAGE = "jsp/view/errorPage.jsp";

    private static ReentrantLock locker = new ReentrantLock();
    private static ApplicationContext instance;
    private Map<String, ServletCommand> commands = new HashMap<>();

    private ApplicationContext() {
    }

    public static ApplicationContext getInstance() {
        if (instance == null) {
            try {
                locker.lock();
                if (instance == null) {
                    instance = new ApplicationContext();
                }
            } finally {
                locker.unlock();
            }
        }
        return instance;
    }

    public void init() throws ApplicationContextException {
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        try {
            connectionPool.init("database");
        } catch (ConnectionPoolException e) {
            log.error(CONNECTION_POOL_INITIALIZATION_ERROR, e);
            throw new ApplicationContextException(CONNECTION_POOL_INITIALIZATION_ERROR, e);
        }


        TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
        ConnectionManager connectionManager = new ConnectionManager(transactionManager);

        UserDao userDao = new UserDaoImpl(connectionManager);
        ContactDao contactDao = new ContactDaoImpl(connectionManager);
        WalletDao walletDao = new WalletDaoImpl(connectionManager);
        VehicleDao vehicleDao = new VehicleDaoImpl(connectionManager);
        OrderDao orderDao = new OrderDaoImpl(connectionManager);

        UserService userServiceImpl = new UserServiceImpl(connectionManager, userDao, contactDao, walletDao, vehicleDao, orderDao);
        VehicleService vehicleServiceImpl = new VehicleServiceImpl(connectionManager, vehicleDao, userDao, contactDao, walletDao);
        OrderService orderServiceImpl = new OrderServiceImpl(connectionManager, orderDao, userDao, contactDao, walletDao, vehicleDao);

        ServletCommand startCommand = new StartCommand();
        ServletCommand logOutUserCommand = new LogOutUserCommand();
        ServletCommand signInUserCommand = new SignInUserCommand(userServiceImpl);
        ServletCommand viewSignInFormCommand = new ViewSignInFormCommand();
        ServletCommand viewSignUpFormCommand = new ViewSignUpFormCommand();
        ServletCommand signUpUserCommand = new SignUpUserCommand(userServiceImpl);
        ServletCommand userManagementCommand = new UserManagementCommand(userServiceImpl);
        ServletCommand viewUserManagementTable = new ViewUserManagementTableCommand();
        ServletCommand deleteUserCommand = new DeleteUserCommand(userServiceImpl);
        ServletCommand viewMyVehicleCommand = new ViewMyVehicleCommand(vehicleServiceImpl);
        ServletCommand viewFormAddVehicleCommand = new ViewFormAddVehicleCommand();
        ServletCommand saveVehicleCommand = new SaveVehicleCommand(vehicleServiceImpl);
        ServletCommand viewChangeVehicleFormCommand = new ViewChangeVehicleFormCommand();
        ServletCommand deleteVehicleCommand = new DeleteVehicleCommand(vehicleServiceImpl, orderServiceImpl);
        ServletCommand changeVehicleCommand = new ChangeVehicleCommand(vehicleServiceImpl);
        ServletCommand viewAllTransport = new ViewAllTransportCommand(vehicleServiceImpl);
        ServletCommand viewOrderFormCommand = new ViewOrderFormCommand();
        ServletCommand calculateTheOrderCostCommand = new CalculateTheOrderCostCommand(vehicleServiceImpl, userServiceImpl);
        ServletCommand saveOrderCommand = new SaveOrderCommand(orderServiceImpl, vehicleServiceImpl, userServiceImpl);
        ServletCommand viewMyOrdersCommand = new ViewMyOrdersCommand(orderServiceImpl, vehicleServiceImpl, userServiceImpl);
        ServletCommand changeOrderStatusCommand = new ChangeOrderStatusCommand(orderServiceImpl);

        commands.put(START_COMMAND.getCommandPath(), startCommand);
        commands.put(LOG_OUT_USER_COMMAND.getCommandPath(), logOutUserCommand);
        commands.put(SIGN_IN_USER_COMMAND.getCommandPath(), signInUserCommand);
        commands.put(VIEW_SIGN_IN_FORM_COMMAND.getCommandPath(), viewSignInFormCommand);
        commands.put(VIEW_SIGN_UP_FORM_COMMAND.getCommandPath(), viewSignUpFormCommand);
        commands.put(SIGN_UP_USER_COMMAND.getCommandPath(), signUpUserCommand);
        commands.put(USER_MANAGEMENT_COMMAND.getCommandPath(), userManagementCommand);
        commands.put(VIEW_USER_MANAGEMENT_TABLE_COMMAND.getCommandPath(), viewUserManagementTable);
        commands.put(DELETE_USER_COMMAND.getCommandPath(), deleteUserCommand);
        commands.put(VIEW_MY_VEHICLE_COMMAND.getCommandPath(), viewMyVehicleCommand);
        commands.put(VIEW_ADD_VEHICLE_COMMAND.getCommandPath(), viewFormAddVehicleCommand);
        commands.put(SAVE_VEHICLE_COMMAND.getCommandPath(), saveVehicleCommand);
        commands.put(VIEW_CHANGE_VEHICLE_FORM_COMMAND.getCommandPath(), viewChangeVehicleFormCommand);
        commands.put(DELETE_VEHICLE_COMMAND.getCommandPath(), deleteVehicleCommand);
        commands.put(CHANGE_VEHICLE_COMMAND.getCommandPath(), changeVehicleCommand);
        commands.put(VIEW_ALL_TRANSPORT.getCommandPath(), viewAllTransport);
        commands.put(VIEW_ORDER_FORM.getCommandPath(), viewOrderFormCommand);
        commands.put(CALCULATE_THE_ORDER_COST_COMMAND.getCommandPath(), calculateTheOrderCostCommand);
        commands.put(SAVE_ORDER_COMMAND.getCommandPath(), saveOrderCommand);
        commands.put(VIEW_MY_ORDERS.getCommandPath(), viewMyOrdersCommand);
        commands.put(CHANGE_ORDER_STATUS_COMMAND.getCommandPath(), changeOrderStatusCommand);

        log.info(APP_CONTEXT_INIT_MESSAGE);
    }

    public void destroy() throws ApplicationContextException {
        try {
            ConnectionPoolImpl.getInstance().destroy();
        } catch (ConnectionPoolException e) {
            log.error(CONNECTION_POOL_DESTROY_ERROR, e);
            throw new ApplicationContextException(CONNECTION_POOL_DESTROY_ERROR, e);
        }
    }

    public ServletCommand getCommand(String commandName) {
        return commands.get(commandName);
    }
}
