package by.epam.training.command.impl;

import by.epam.training.command.CommandException;
import by.epam.training.command.CommandName;
import by.epam.training.command.ServletCommand;
import by.epam.training.dao.TransactionException;
import by.epam.training.service.OrderService;
import by.epam.training.service.ServiceException;
import by.epam.training.service.VehicleService;
import by.epam.training.service.dto.OrderDto;
import by.epam.training.service.dto.VehicleDto;
import lombok.extern.log4j.Log4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Log4j
public class DeleteVehicleCommand implements ServletCommand {
    private static final String START_MSG = DeleteVehicleCommand.class + " was executed. ";
    private static final String CANNOT_DELETE_VEHICLE = "Cannot delete vehicle.";
    private VehicleService vehicleServiceImpl;
    private OrderService orderServiceImpl;

    public DeleteVehicleCommand(VehicleService vehicleServiceImpl, OrderService orderServiceImpl) {
        this.vehicleServiceImpl = vehicleServiceImpl;
        this.orderServiceImpl = orderServiceImpl;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.debug(START_MSG);
        VehicleDto vehicleDto = (VehicleDto) req.getSession().getAttribute("vehicle");
        try {
            List<OrderDto> driverOrders = orderServiceImpl.getOrdersByVehicle(vehicleDto);
            if (!driverOrders.isEmpty()) {
                for (OrderDto order : driverOrders) {
                    orderServiceImpl.delete(order);
                }
            }
            boolean isDeleted = vehicleServiceImpl.delete(vehicleDto);
            if (isDeleted) {
                req.getSession().removeAttribute("vehicle");
                resp.sendRedirect(CommandName.VIEW_MY_VEHICLE_COMMAND.getCommandPath());
            }
        } catch (ServiceException | IOException | TransactionException e) {
            log.error(CANNOT_DELETE_VEHICLE + e);
            throw new CommandException(CANNOT_DELETE_VEHICLE, e);
        }
    }
}
