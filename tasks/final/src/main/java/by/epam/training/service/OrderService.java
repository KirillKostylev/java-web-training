package by.epam.training.service;

import by.epam.training.dao.TransactionException;
import by.epam.training.dao.entity.OrderStatus;
import by.epam.training.service.dto.OrderDto;
import by.epam.training.service.dto.UserDto;
import by.epam.training.service.dto.VehicleDto;

import java.util.List;

public interface OrderService extends Service<OrderDto, Long> {
    List<OrderDto> getOrdersByVehicle(VehicleDto vehicleDto) throws ServiceException, TransactionException;

    List<OrderDto> getOrdersByCustomer(UserDto userDto) throws ServiceException, TransactionException;

    boolean updateStatus(Long orderId, OrderStatus newStatus) throws ServiceException, TransactionException;
}
