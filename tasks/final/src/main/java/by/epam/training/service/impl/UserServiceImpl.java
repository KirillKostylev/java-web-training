package by.epam.training.service.impl;

import by.epam.training.dao.*;
import by.epam.training.dao.entity.UserRoles;
import by.epam.training.service.ServiceException;
import by.epam.training.service.UserService;
import by.epam.training.service.dto.OrderDto;
import by.epam.training.service.dto.UserDto;
import by.epam.training.service.dto.VehicleDto;
import lombok.extern.log4j.Log4j;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j
public class UserServiceImpl implements UserService {
    private static final String USER_CREATION_ERROR = "Cannot create new User. ";
    private static final String USER_SEARCH_ERROR = "Cannot found User. ";
    private static final String CANNOT_GET_ALL_USERS = "Cannot get all users.";
    private static final String ALL_USERS_RECEIVED_SUCCESSFULLY = "All users received successfully";
    private static final String CANNOT_DELETE_USER = "Cannot delete user.";
    private static final String WAS_DELETED_SUCCESSFULLY_MESSAGE = " was deleted successfully";
    private static final String USER_WAS_DELETED_LOGIN = "User was deleted login=";
    private static final String CANNOT_DELETE_ORDERS_FROM_DRIVER_LOGIN = "Cannot delete orders from driver login = ";
    private TransactionManager transactionManager;
    private ConnectionManager connectionManager;
    private UserDao userDao;
    private ContactDao contactDao;
    private WalletDao walletDao;
    private VehicleDao vehicleDao;
    private OrderDao orderDao;

    public UserServiceImpl(ConnectionManager connectionManager, UserDao userDao, ContactDao contactDao,
                           WalletDao walletDao, VehicleDao vehicleDao, OrderDao orderDao) {
        this.connectionManager = connectionManager;
        transactionManager = connectionManager.getTransactionManager();
        this.userDao = userDao;
        this.contactDao = contactDao;
        this.walletDao = walletDao;
        this.vehicleDao = vehicleDao;
        this.orderDao = orderDao;
    }

    @Override
    public Long create(UserDto dto) throws ServiceException, TransactionException {
        long userId;
        try {
            transactionManager.beginTransaction();
            contactDao.save(dto.getContactDto());
            walletDao.save(dto.getWalletDto());
            userId = userDao.save(dto);
            transactionManager.commitTransaction();
        } catch (SQLException e) {
            log.error(USER_CREATION_ERROR, e);
            transactionManager.rollbackTransaction();
            throw new ServiceException(USER_CREATION_ERROR, e);
        }
        return userId;
    }

    @Override
    public boolean update(UserDto userDto) {
        return false;
    }

    @Override
    public UserDto getUserByLogin(String login) throws ServiceException, TransactionException {
        Optional<UserDto> dto;
        UserDto resultDto = null;
        try {
            transactionManager.beginTransaction();
            dto = userDao.findByLogin(login, contactDao, walletDao);
            if (dto.isPresent()) {
                resultDto = dto.get();
            }
            transactionManager.commitTransaction();
        } catch (TransactionException | DaoException e) {
            log.error(USER_SEARCH_ERROR, e);
            transactionManager.rollbackTransaction();
            throw new ServiceException(USER_SEARCH_ERROR, e);
        }
        return resultDto;
    }

    @Override
    public List<UserDto> getAll() throws ServiceException {
        List<UserDto> resultDtos = new ArrayList<>();
        Optional<List<UserDto>> users;
        try {
            transactionManager.beginTransaction();
            users = userDao.findAll(contactDao, walletDao);
            if (users.isPresent()) {
                resultDtos = users.get();
                log.debug(ALL_USERS_RECEIVED_SUCCESSFULLY);
            }
            transactionManager.commitTransaction();
        } catch (TransactionException | DaoException e) {
            log.error(CANNOT_GET_ALL_USERS, e);
            throw new ServiceException(CANNOT_GET_ALL_USERS, e);
        }
        return resultDtos;
    }

    @Override
    public boolean delete(UserDto userDto) throws ServiceException, TransactionException {
        try {
            transactionManager.beginTransaction();
            List<OrderDto> orderList;
            if (userDto.getRole().equals(UserRoles.DRIVER)) {
                Optional<VehicleDto> vehicleDto = vehicleDao.getVehicleByUser(userDto);
                if (vehicleDto.isPresent()) {
                    orderList = orderDao.getAllByVehicle(vehicleDto.get(), userDao, contactDao, walletDao);
                    deleteOrderList(orderList);
                    vehicleDao.delete(vehicleDto.get().getId());
                }
            } else {
                orderList = orderDao.getAllByCustomer(userDto, userDao, contactDao, walletDao, vehicleDao);
                deleteOrderList(orderList);
            }
            boolean isDeleted = false;
            isDeleted = userDao.delete(userDto.getId());
            if (isDeleted) {
                log.debug(USER_WAS_DELETED_LOGIN + userDto.getLogin());
            }
            transactionManager.commitTransaction();
            return isDeleted;
        } catch (SQLException e) {
            log.error(CANNOT_DELETE_USER, e);
            transactionManager.rollbackTransaction();
            throw new ServiceException(CANNOT_DELETE_USER, e);
        }
    }

    private boolean deleteOrderList(List<OrderDto> orderList) {
        for (OrderDto order : orderList) {
            try {
                orderDao.delete(order.getId());
            } catch (SQLException e) {
                log.error(CANNOT_DELETE_ORDERS_FROM_DRIVER_LOGIN + order.getVehicleDto().getUserDto().getLogin());
            }
        }
        return orderList.isEmpty();
    }
}
