package by.epam.training.dao;

import by.epam.training.service.dto.ContactDto;

public interface ContactDao extends CRUDdao<ContactDto, Long> {

}
