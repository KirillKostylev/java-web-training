package by.epam.training.validator;

import by.epam.training.dao.TransactionException;
import by.epam.training.filter.LocaleFilter;
import by.epam.training.service.ServiceException;
import by.epam.training.service.UserService;
import by.epam.training.service.dto.UserDto;
import lombok.extern.log4j.Log4j;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.ResourceBundle;

import static by.epam.training.util.UtilityMethods.getValueFromCookies;
import static by.epam.training.util.UtilityMethods.stringIsEmpty;

@Log4j
public class SignUpValidator implements Validator {
    private static final String PHONE_NUMBER_REGEX = "(^375)([1-9]{2})([0-9]{7})$";
    private static final String NAME_REGEX = "^[a-zA-zа-яА-Я\\\\-]{3,25}$";
    private static final String VALIDATOR_START_MESSAGE = "SignUpValidator started validating.";
    private static final String INCORRECT_PHONE_NUMBER = "locale.error.incorrect.phone.number";
    private static final String INCORRECT_LAST_NAME = "locale.error.incorrect.last.name";
    private static final String INCORRECT_FIRST_NAME = "locale.error.incorrect.first.name";
    private static final String PASSWORD_DO_NOT_MATCH = "locale.error.password.matching";
    private static final String CHECK_USER_FOR_EXISTING_ERROR = "Error during user verification for existence.";
    private static final String LOGIN_BUSY = "locale.error.user.exist";

    private UserService userServiceImpl;

    public SignUpValidator(UserService userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    @Override
    public ValidationResult validate(HttpServletRequest request) {
        log.debug(VALIDATOR_START_MESSAGE);
        ValidationResult validationResult = new ValidationResult();

        String locale = getValueFromCookies(request, LocaleFilter.COOKIE_NAME);
        String language = locale.substring(0, 2);
        String country = locale.substring(3);
        ResourceBundle bundle = ResourceBundle.getBundle(PATH_TO_RESOURCE_BUNDLE, new Locale(language, country));

        String firstName = request.getParameter("user.firstName");
        String lastName = request.getParameter("user.lastName");
        String phoneNumber = request.getParameter("user.phoneNumber");
        String email = request.getParameter("user.email");
        String login = request.getParameter("user.login");
        String password = request.getParameter("user.password");
        String passwordConfirm = request.getParameter("user.passwordConfirm");

        if (stringIsEmpty(login) || stringIsEmpty(password) || stringIsEmpty(passwordConfirm) || stringIsEmpty(email)
                || stringIsEmpty(phoneNumber) || stringIsEmpty(lastName) || stringIsEmpty(firstName)) {
            validationResult.addError(ErrorKey.EMPTY_FIELD, bundle.getString(EMPTY_FIELD_MESSAGE));
            return validationResult;
        }
        if (!login.matches(USER_LOGIN_AND_PASSWORD_REGEX)) {
            validationResult.addError(ErrorKey.INCORRECT_FORMAT_LOGIN, bundle.getString(INCORRECT_LOGIN_FORMAT_MESSAGE));
        }
        if (!password.matches(USER_LOGIN_AND_PASSWORD_REGEX)) {
            validationResult.addError(ErrorKey.INCORRECT_FORMAT_PASSWORD,
                    bundle.getString(INCORRECT_PASSWORD_FORMAT_MESSAGE));
        }
        if (!phoneNumber.matches(PHONE_NUMBER_REGEX)) {
            validationResult.addError(ErrorKey.INCORRECT_PHONE_NUMBER, bundle.getString(INCORRECT_PHONE_NUMBER));
        }
        if (!firstName.matches(NAME_REGEX)) {
            validationResult.addError(ErrorKey.INCORRECT_FIRST_NAME, bundle.getString(INCORRECT_FIRST_NAME));
        }
        if (!lastName.matches(NAME_REGEX)) {
            validationResult.addError(ErrorKey.INCORRECT_LAST_NAME, bundle.getString(INCORRECT_LAST_NAME));
        }
        if (!password.equals(passwordConfirm)) {
            validationResult.addError(ErrorKey.PASSWORD_DO_NOT_MATCH, bundle.getString(PASSWORD_DO_NOT_MATCH));
        }

        if (!validationResult.isValid()) {
            return validationResult;
        }

        try {
            UserDto userDto = userServiceImpl.getUserByLogin(login);
            if (userDto != null) {
                validationResult.addError(ErrorKey.LOGIN_BUSY, bundle.getString(LOGIN_BUSY));
            }
        } catch (ServiceException | TransactionException e) {
            log.error(CHECK_USER_FOR_EXISTING_ERROR + e);
        }
        return validationResult;
    }
}
