package by.epam.training.command.impl;

import by.epam.training.command.CommandException;
import by.epam.training.command.CommandName;
import by.epam.training.command.ServletCommand;
import by.epam.training.dao.TransactionException;
import by.epam.training.dao.entity.OrderStatus;
import by.epam.training.service.OrderService;
import by.epam.training.service.ServiceException;
import by.epam.training.service.UserService;
import by.epam.training.service.VehicleService;
import by.epam.training.service.dto.OrderDto;
import by.epam.training.service.dto.UserDto;
import by.epam.training.service.dto.VehicleDto;
import by.epam.training.util.UtilityMethods;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringEscapeUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;

@Log4j
public class SaveOrderCommand implements ServletCommand {
    private static final String START_MSG = SaveOrderCommand.class + " was executed.";
    private static final String CANNOT_SAVE_ORDER_TO_DATABASE = "Cannot save order to database. ";

    private OrderService orderServiceImpl;
    private VehicleService vehicleServiceImpl;
    private UserService userServiceImpl;

    public SaveOrderCommand(OrderService orderServiceImpl, VehicleService vehicleServiceImpl, UserService userServiceImpl) {
        this.orderServiceImpl = orderServiceImpl;
        this.vehicleServiceImpl = vehicleServiceImpl;
        this.userServiceImpl = userServiceImpl;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CommandException {
        log.debug(START_MSG);
        String orderPriceStr = req.getParameter("order.price");

        String loadingAddress = req.getParameter("order.loadingAddress");
        loadingAddress = StringEscapeUtils.escapeHtml4(loadingAddress);

        String unloadingAddress = req.getParameter("order.unloadingAddress");
        unloadingAddress = StringEscapeUtils.escapeHtml4(unloadingAddress);

        String loadingDateStr = req.getParameter("order.loadingDate");
        String unloadingDateStr = req.getParameter("order.unloadingDate");
        String vehicleOwnerLogin = req.getParameter("owner.login");

        Timestamp loadingDate = UtilityMethods.convertDatetimeLocalStringToTimestamp(loadingDateStr);
        Timestamp unloadingDate = UtilityMethods.convertDatetimeLocalStringToTimestamp(unloadingDateStr);
        double orderPrice = Double.parseDouble(orderPriceStr);
        Timestamp currentDate = new Timestamp(System.currentTimeMillis());
        UserDto userDto = (UserDto) req.getSession().getAttribute("user");

        OrderDto orderDto = new OrderDto();
        orderDto.setLoadingAddress(loadingAddress);
        orderDto.setUnloadingAddress(unloadingAddress);
        orderDto.setLoadingDate(loadingDate);
        orderDto.setUnloadingDate(unloadingDate);
        orderDto.setCost(orderPrice);
        orderDto.setOrderTime(currentDate);
        orderDto.setStatus(OrderStatus.ORDERED);
        orderDto.setUserDto(userDto);

        try {
            UserDto vehicleOwner = userServiceImpl.getUserByLogin(vehicleOwnerLogin);
            VehicleDto vehicleDto = vehicleServiceImpl.getVehicleByUser(vehicleOwner);
            orderDto.setVehicleDto(vehicleDto);
            long orderId = orderServiceImpl.create(orderDto);
        } catch (ServiceException | TransactionException e) {
            log.error(CANNOT_SAVE_ORDER_TO_DATABASE + e);
            throw new CommandException(CANNOT_SAVE_ORDER_TO_DATABASE, e);
        }

        resp.sendRedirect(CommandName.START_COMMAND.getCommandPath());
    }
}
