package by.epam.training.dao;

import java.sql.SQLException;

public interface CRUDdao<ENTITY, KEY> {
    KEY save(ENTITY entity) throws SQLException;

    boolean update(ENTITY entity) throws SQLException;

    boolean delete(KEY id) throws SQLException;

    ENTITY getById(KEY id) throws SQLException;

}
