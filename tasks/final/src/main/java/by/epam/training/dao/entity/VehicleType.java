package by.epam.training.dao.entity;

public enum VehicleType {
    PASSENGER_CAR("passenger_car"),
    TRUCK("truck"),
    VAN("van");

    private String value;

    VehicleType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
