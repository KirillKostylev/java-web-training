package by.epam.training.dao.impl;

import by.epam.training.dao.ContactDao;
import by.epam.training.dao.ConnectionManager;
import by.epam.training.dao.DaoException;
import by.epam.training.dao.entity.VehicleEntity;
import by.epam.training.dao.entity.VehicleType;
import by.epam.training.dao.UserDao;
import by.epam.training.service.dto.UserDto;
import by.epam.training.util.UtilityMethods;
import by.epam.training.dao.VehicleDao;
import by.epam.training.service.dto.VehicleDto;
import by.epam.training.dao.WalletDao;
import lombok.extern.log4j.Log4j;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j
public class VehicleDaoImpl implements VehicleDao {

    private static final String GET_VEHICLE_BY_USER_ID_QUERY =
            "SELECT vehicle_id, location, tariff, owner_id, vehicle_type, photo, description " +
                    "FROM vehicle WHERE owner_id = ?; ";
    private static final String GET_VEHICLE_BY_ID_QUERY =
            "SELECT vehicle_id, location, tariff, owner_id, vehicle_type, photo, description " +
                    "FROM vehicle WHERE vehicle_id = ?; ";
    private static final String INSERT_VEHICLE_QUERY =
            "INSERT INTO vehicle(location, tariff, owner_id, vehicle_type, photo, description) VALUES (?,?,?,?,?,?);";
    private static final String DELETE_VEHICLE_QUERY = "DELETE vehicle FROM vehicle WHERE vehicle_id = ?; ";
    private static final String UPDATE_VEHICLE_QUERY =
            "UPDATE vehicle SET location=?,tariff=?,vehicle_type=?, photo=?, description = ? WHERE vehicle_id=?;";
    private static final String SELECT_ALL_VEHICLE_QUERY =
            "SELECT vehicle_id, location, tariff, owner_id, vehicle_type, photo, description FROM vehicle;";
    private static final String WAS_SAVE_IN_DATABASE_MSG = " was save in database. ";
    private static final String CANNOT_SAVE_VEHICLE_IN_DATABASE_MSG = "Cannot save vehicle in database. ";
    private static final String CANNOT_GET_VEHICLE_BY_USER_ID = "Cannot get vehicle by userId: ";
    private static final String CANNOT_DELETE_VEHICLE_WITH_ID = "Cannot delete vehicle with id ";
    private static final String CANNOT_UPDATE_VEHICLE_WITH_ID = "Cannot update vehicle with id ";
    private static final String ID_VEHICLE_WAS_UPDATED = "-id vehicle was updated.";
    private static final String ID_VEHICLE_WAS_DELETED = "-id vehicle was deleted.";
    private static final String CANNOT_GET_ALL_TRANSPORT_FROM_DATABASE = "Cannot get all transport from database. ";
    private static final String CANNOT_GET_VEHICLE_WITH_ID = "Cannot get vehicle with id: ";
    private ConnectionManager connectionManager;

    public VehicleDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Long save(VehicleDto vehicleDto) throws DaoException {
        VehicleEntity entity = fromDto(vehicleDto);
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement insertStmt = connection.prepareStatement(INSERT_VEHICLE_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            insertStmt.setString(++i, entity.getLocation());
            insertStmt.setDouble(++i, entity.getTariff());
            insertStmt.setLong(++i, entity.getOwnerId());
            insertStmt.setString(++i, entity.getType().getValue());
//            insertStmt.setString(++i, entity.getPhoto());
            Blob imgBlob;
            imgBlob = new SerialBlob(entity.getPhoto());

            insertStmt.setBlob(++i, imgBlob);
            insertStmt.setString(++i, entity.getDescription());
            insertStmt.executeUpdate();
            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
                vehicleDto.setId(generatedKeys.getLong(1));
            }
            log.info(entity + WAS_SAVE_IN_DATABASE_MSG);
        } catch (SQLException e) {
            log.error(CANNOT_SAVE_VEHICLE_IN_DATABASE_MSG + e);
            throw new DaoException(CANNOT_SAVE_VEHICLE_IN_DATABASE_MSG, e);
        }
        return entity.getId();
    }

    @Override
    public boolean update(VehicleDto vehicleDto) throws DaoException {
        boolean isUpdated = false;
        VehicleEntity entity = fromDto(vehicleDto);
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement updStmt = connection.prepareStatement(UPDATE_VEHICLE_QUERY)) {
            Blob imgBlob = new SerialBlob(entity.getPhoto());
            int i = 0;
            updStmt.setString(++i, entity.getLocation());
            updStmt.setDouble(++i, entity.getTariff());
            updStmt.setString(++i, entity.getType().getValue());
            updStmt.setBlob(++i, imgBlob);
//            updStmt.setString(++i, entity.getPhoto());
            updStmt.setString(++i, entity.getDescription());
            updStmt.setLong(++i, entity.getId());
            isUpdated = updStmt.executeUpdate() > 0;
            if (isUpdated) {
                log.debug(entity.getId() + ID_VEHICLE_WAS_UPDATED);
            }
        } catch (SQLException e) {
            log.error(CANNOT_UPDATE_VEHICLE_WITH_ID + entity.getId() + e);
            throw new DaoException(CANNOT_GET_VEHICLE_BY_USER_ID + entity.getId(), e);
        }
        return isUpdated;
    }

    @Override
    public boolean delete(Long vehicleId) throws DaoException {
        boolean isDeleted;
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement delStmt = connection.prepareStatement(DELETE_VEHICLE_QUERY)) {
            delStmt.setLong(1, vehicleId);
            isDeleted = delStmt.executeUpdate() > 0;
        } catch (SQLException e) {
            log.error(CANNOT_DELETE_VEHICLE_WITH_ID + vehicleId + e);
            throw new DaoException(CANNOT_DELETE_VEHICLE_WITH_ID + vehicleId, e);
        }
        if (isDeleted) {
            log.info(vehicleId + ID_VEHICLE_WAS_DELETED);
        }
        return isDeleted;
    }

    @Override
    public VehicleDto getById(Long id) throws SQLException {
        return null;
    }

    @Override
    public VehicleDto getById(Long id, UserDao userDao, ContactDao contactDao, WalletDao walletDao) throws SQLException {
        VehicleDto resultVehicle = null;
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement selectStmt = connection.prepareStatement(GET_VEHICLE_BY_ID_QUERY)) {
            selectStmt.setLong(1, id);
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                VehicleEntity entity = parseResultSet(resultSet);
                UserDto userDto = userDao.getById(entity.getOwnerId(), walletDao, contactDao).get();
                resultVehicle = fromEntity(entity, userDto);
            }
        } catch (SQLException e) {
            log.error(CANNOT_GET_VEHICLE_WITH_ID + id + e);
            throw new DaoException(CANNOT_GET_VEHICLE_WITH_ID + id, e);
        }

        return resultVehicle;
    }

    @Override
    public Optional<VehicleDto> getVehicleByUser(UserDto userDto) throws DaoException {
        Optional<VehicleDto> vehicleDto;
        List<VehicleEntity> vehicleEntities = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement selectStmt = connection.prepareStatement(GET_VEHICLE_BY_USER_ID_QUERY)) {
            selectStmt.setLong(1, userDto.getId());
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                VehicleEntity entity = parseResultSet(resultSet);
                vehicleEntities.add(entity);
            }
            if (vehicleEntities.isEmpty()) {
                log.info("User with id: " + userDto + " don't have vehicle.");
                return Optional.empty();
            }
            VehicleEntity vehicleEntity = vehicleEntities.stream().findFirst().get();
            vehicleDto = Optional.of(fromEntity(vehicleEntity, userDto));
        } catch (SQLException e) {
            log.error(CANNOT_GET_VEHICLE_BY_USER_ID + userDto + e);
            throw new DaoException(CANNOT_GET_VEHICLE_BY_USER_ID + userDto, e);
        }

        return vehicleDto;
    }

    @Override
    public Optional<List<VehicleDto>> getAllTransport(UserDao userDao, ContactDao contactDao, WalletDao walletDao) throws DaoException {
        Optional<List<VehicleDto>> transportList = Optional.empty();
        List<VehicleDto> resultList = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_VEHICLE_QUERY)) {
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                VehicleEntity entity = parseResultSet(resultSet);
                Optional<UserDto> user = userDao.getById(entity.getOwnerId(), walletDao, contactDao);
                if (user.isPresent()) {
                    VehicleDto vehicleDto = fromEntity(entity, user.get());
                    resultList.add(vehicleDto);
                }
            }
            transportList = Optional.of(resultList);
        } catch (SQLException e) {
            log.error(CANNOT_GET_ALL_TRANSPORT_FROM_DATABASE + e);
            throw new DaoException(CANNOT_GET_ALL_TRANSPORT_FROM_DATABASE, e);
        }
        return transportList;
    }


    private VehicleEntity fromDto(VehicleDto dto) {
        VehicleEntity entity = new VehicleEntity();
        entity.setDescription(dto.getDescription());
        entity.setLocation(dto.getLocation());
        entity.setOwnerId(dto.getUserDto().getId());
        entity.setType(dto.getType());
        entity.setTariff(dto.getTariff());
        entity.setId(dto.getId());
        byte[] photoInBytes = UtilityMethods.convertBase64ToByteArray(dto.getPhoto());
        entity.setPhoto(photoInBytes);

        return entity;
    }

    private VehicleDto fromEntity(VehicleEntity vehicleEntity, UserDto userDto) {
        VehicleDto dto = new VehicleDto();
        dto.setId(vehicleEntity.getId());
        dto.setType(vehicleEntity.getType());
        dto.setTariff(vehicleEntity.getTariff());
        dto.setDescription(vehicleEntity.getDescription());
        dto.setLocation(vehicleEntity.getLocation());
        String vehiclePhotoInBase64 = UtilityMethods.convertByteArrayToBase64(vehicleEntity.getPhoto());
        dto.setPhoto(vehiclePhotoInBase64);
        dto.setUserDto(userDto);
        return dto;
    }

    private VehicleEntity parseResultSet(ResultSet resultSet) throws SQLException {
        VehicleEntity entity = new VehicleEntity();

        long vehicleId = resultSet.getLong("vehicle_id");
        String location = resultSet.getString("location");
        String description = resultSet.getString("description");
//        String photo = resultSet.getString("photo");
        Blob photo = resultSet.getBlob("photo");
        double tariff = resultSet.getDouble("tariff");
        VehicleType type = VehicleType.valueOf(resultSet.getString("vehicle_type").toUpperCase());
        long userId = resultSet.getLong("owner_id");

        entity.setId(vehicleId);
        entity.setOwnerId(userId);
        entity.setTariff(tariff);
        entity.setType(type);
        entity.setLocation(location);
        entity.setDescription(description);
        if (photo != null) {
            entity.setPhoto(photo.getBytes(1, (int) photo.length()));
            photo.free();
        }

        return entity;
    }
}
