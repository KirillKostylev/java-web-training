package by.epam.training.validator;

import javax.servlet.http.HttpServletRequest;

public interface Validator {
    String PATH_TO_RESOURCE_BUNDLE = "i18n//localization";
    String USER_LOGIN_AND_PASSWORD_REGEX = "^[a-zA-Z0-9]{3,20}$";
    String INCORRECT_LOGIN_FORMAT_MESSAGE = "locale.error.incorrect.login";
    String INCORRECT_PASSWORD_FORMAT_MESSAGE = "locale.error.incorrect.password";
    String EMPTY_FIELD_MESSAGE = "locale.error.incorrect.empty.field";

    ValidationResult validate(HttpServletRequest request);
}
