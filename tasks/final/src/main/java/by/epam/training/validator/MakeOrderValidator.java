package by.epam.training.validator;

import by.epam.training.filter.LocaleFilter;
import by.epam.training.util.DistanceCalculator;
import by.epam.training.util.UtilityMethods;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Locale;
import java.util.ResourceBundle;

import static by.epam.training.util.UtilityMethods.getValueFromCookies;

public class MakeOrderValidator implements Validator {

    private static final String INCORRECT_DATE_MSG = "locale.error.incorrect.date";
    private static final String INCORRECT_ADDRESS_MSG = "locale.error.incorrect.address";

    @Override
    public ValidationResult validate(HttpServletRequest request) {
        ValidationResult validationResult = new ValidationResult();


        String locale = getValueFromCookies(request, LocaleFilter.COOKIE_NAME);
        String language = locale.substring(0, 2);
        String country = locale.substring(3);
        ResourceBundle bundle = ResourceBundle.getBundle(PATH_TO_RESOURCE_BUNDLE, new Locale(language, country));

        String firstAddr = request.getParameter("order.loadingAddress");
        String secondAddr = request.getParameter("order.unloadingAddress");
        String loadingDateStr = request.getParameter("order.loadingDate");
        String unloadingDateStr = request.getParameter("order.unloadingDate");

        Timestamp loadingDate = UtilityMethods.convertDatetimeLocalStringToTimestamp(loadingDateStr);
        Timestamp unloadingDate = UtilityMethods.convertDatetimeLocalStringToTimestamp(unloadingDateStr);
        Timestamp currentDate = new Timestamp(System.currentTimeMillis());

        if (loadingDate.after(unloadingDate) || loadingDate.before(currentDate)) {
            validationResult.addError(ErrorKey.INCORRECT_DATE, bundle.getString(INCORRECT_DATE_MSG));
            return validationResult;
        }
        String coordinate1 = DistanceCalculator.findCoordinate(firstAddr);
        String coordinate2 = DistanceCalculator.findCoordinate(secondAddr);
        if (coordinate1.equals("") || coordinate2.equals("")) {
            validationResult.addError(ErrorKey.INCORRECT_ADDRESS, bundle.getString(INCORRECT_ADDRESS_MSG));
        }
        return validationResult;
    }
}
