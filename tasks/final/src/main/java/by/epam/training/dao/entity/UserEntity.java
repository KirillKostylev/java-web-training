package by.epam.training.dao.entity;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class UserEntity {
    private long id;
    private String login;
    private String password;
    private UserRoles role = UserRoles.CLIENT;
    private Timestamp registrationDate;
    private long contactId;
    private long walletId;

}
