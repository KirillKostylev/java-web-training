package by.epam.training.command.impl;

import by.epam.training.command.CommandException;
import by.epam.training.command.ServletCommand;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
public class ViewUserManagementTableCommand implements ServletCommand {
    private static final String USER_MANAGEMENT_PATH = "/jsp/view/UserManagement.jsp";
    private static final String START_MSG = ViewUserManagementTableCommand.class + " was executed. ";

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CommandException {
        log.debug(START_MSG);
        req.getRequestDispatcher(USER_MANAGEMENT_PATH).forward(req, resp);
    }
}
