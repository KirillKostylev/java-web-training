package by.epam.training.core;

public class ApplicationContextException extends Exception {
    public ApplicationContextException(String message) {
        super(message);
    }

    public ApplicationContextException(String message, Throwable cause) {
        super(message, cause);
    }
}
