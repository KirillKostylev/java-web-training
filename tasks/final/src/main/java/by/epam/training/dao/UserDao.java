package by.epam.training.dao;

import by.epam.training.service.dto.UserDto;

import java.util.List;
import java.util.Optional;

public interface UserDao extends CRUDdao<UserDto, Long> {
    Optional<UserDto> findByLogin(String login, ContactDao contactDao, WalletDao walletDao) throws DaoException;

    Optional<UserDto> getById(Long id, WalletDao walletDao, ContactDao contactDao) throws DaoException;

    Optional<List<UserDto>> findAll(ContactDao contactDao, WalletDao walletDao) throws DaoException;

}
