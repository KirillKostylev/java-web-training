package by.epam.training.command.impl;

import by.epam.training.command.CommandException;
import by.epam.training.command.CommandName;
import by.epam.training.command.ServletCommand;
import by.epam.training.dao.TransactionException;
import by.epam.training.service.ServiceException;
import by.epam.training.service.UserService;
import by.epam.training.service.dto.UserDto;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
public class DeleteUserCommand implements ServletCommand {
    private static final String CANNOT_DELETE_USER = "Cannot delete user.";
    private static final String START_MSG = DeleteUserCommand.class + " was executed. ";

    private UserService userServiceImpl;

    public DeleteUserCommand(UserService userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CommandException {
        log.debug(START_MSG);
        String login = req.getParameter("user.login");
        try {
            UserDto userDto = userServiceImpl.getUserByLogin(login);
            userServiceImpl.delete(userDto);
        } catch (ServiceException | TransactionException e) {
            log.error(CANNOT_DELETE_USER + e);
            throw new CommandException(CANNOT_DELETE_USER, e);
        }
        resp.sendRedirect(CommandName.USER_MANAGEMENT_COMMAND.getCommandPath());
    }
}
