package by.epam.training.command.impl;

import by.epam.training.command.CommandException;
import by.epam.training.command.CommandName;
import by.epam.training.command.ServletCommand;
import by.epam.training.service.ServiceException;
import by.epam.training.service.UserService;
import by.epam.training.service.dto.UserDto;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Log4j
public class UserManagementCommand implements ServletCommand {
    private static final String CANNOT_GET_ALL_USERS = "Cannot get all users.";
    private static final String START_MSG = UserManagementCommand.class + " was executed. ";
    private UserService userServiceImpl;

    public UserManagementCommand(UserService userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CommandException {
        log.debug(START_MSG);
        List<UserDto> users = null;
        try {
            users = userServiceImpl.getAll();
        } catch (ServiceException e) {
            log.error(CANNOT_GET_ALL_USERS, e);
            throw new CommandException(CANNOT_GET_ALL_USERS, e);
        }
        req.setAttribute("users", users);
        req.getRequestDispatcher(CommandName.VIEW_USER_MANAGEMENT_TABLE_COMMAND.getCommandPath().toLowerCase())
                .forward(req, resp);
    }
}
