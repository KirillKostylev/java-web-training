package by.epam.training.util;

import lombok.extern.log4j.Log4j;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;

@Log4j
public class DistanceCalculator {
    public static final String URL =
            "https://geocode-maps.yandex.ru/1.x/?apikey=b36673d8-109c-47a1-9975-3bd68550f962&geocode=";
    public static final String COORDINATES_TAG_NAME = "pos";
    public static final int EARTH_RADIUS = 6371;
    private static final String CANNOT_GET_COORDINATES_FROM_ADDRESS = "Cannot get coordinates from address: ";
    private static final String COORDINATE_HAS_BEEN_FOUND = "Coordinate has been found: ";
    private static final String DISTANCE_HAS_BEEN_CALCULATED = "Distance has been calculated";

    public static double distanceCalculate(String firstAddr, String secondAddr) {
        String coordinate1 = "", coordinate2 = "";
        coordinate1 = findCoordinate(firstAddr);
        coordinate2 = findCoordinate(secondAddr);
        String[] latitudeAndLongitude1 = coordinate1.split("\\s");
        String[] latitudeAndLongitude2 = coordinate2.split("\\s");
        double latitude1 = Double.parseDouble(latitudeAndLongitude1[0]);
        double longitude1 = Double.parseDouble(latitudeAndLongitude1[1]);
        double latitude2 = Double.parseDouble(latitudeAndLongitude2[0]);
        double longitude2 = Double.parseDouble(latitudeAndLongitude2[1]);
        double dLatitude = Math.toRadians(latitude2 - latitude1);
        double dLongitude = Math.toRadians(longitude2 - longitude1);

        double a = Math.sin(dLatitude / 2) * Math.sin(dLatitude / 2) +
                Math.cos(Math.toRadians(latitude1)) * Math.cos(Math.toRadians(latitude2)) *
                        Math.sin(dLongitude / 2) * Math.sin(dLongitude / 2);

        double b = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        log.debug(DISTANCE_HAS_BEEN_CALCULATED);
        return EARTH_RADIUS * b;
    }

    public static String findCoordinate(String address) {
        String coordinates = "";
        URL url = null;
        try {
            address = URLEncoder.encode(address, "UTF-8");
            url = new URL(URL + address);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputStream byteStream = url.openStream();
            InputSource is = new InputSource(byteStream);
            Document doc = db.parse(is);
            Element documentElement = doc.getDocumentElement();
            NodeList elementsByTagName = documentElement.getElementsByTagName(COORDINATES_TAG_NAME);
            Node nl = elementsByTagName.item(0);

            coordinates = "";
            if (nl == null) {
                return "";
            }
            String[] str = nl.getTextContent().split("\\s");

            coordinates += str[1] + " " + str[0];
            log.debug(COORDINATE_HAS_BEEN_FOUND + coordinates);
            return coordinates;
        } catch (ParserConfigurationException | SAXException | IOException e) {
            log.error(CANNOT_GET_COORDINATES_FROM_ADDRESS + address + e);
        }

        return coordinates;
    }
}
