package by.epam.training.dao.entity;

public enum OrderStatus {
    ORDERED("ordered"),
    ACCEPTED("accepted"),
    IN_THE_WAY("in_the_way"),
    DONE("done"),
    CANCELLED("cancelled");

    private String value;

    OrderStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
