package by.epam.training.listener;

import by.epam.training.core.ApplicationContext;
import by.epam.training.core.ApplicationContextException;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
@Log4j
public class ApplicationContextListener implements ServletContextListener {

    private static final String APP_CONTEXT_DESTROY_MESSAGE = "ApplicationContext was destroyed. ";
    private static final String APPLICATION_CONTEXT_DESTROYING_ERROR = "ApplicationContext destroying error. ";
    private static final String APPLICATION_CONTEXT_INITIALIZING_ERROR = "ApplicationContext initializing error.";

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            ApplicationContext.getInstance().init();
        } catch (ApplicationContextException e) {
            log.error(APPLICATION_CONTEXT_INITIALIZING_ERROR);
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            ApplicationContext.getInstance().destroy();
        } catch (ApplicationContextException e) {
            log.error(APPLICATION_CONTEXT_DESTROYING_ERROR, e);
        }
        log.info(APP_CONTEXT_DESTROY_MESSAGE);
    }
}
