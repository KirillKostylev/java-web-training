package by.epam.training.service.impl;

import by.epam.training.dao.*;
import by.epam.training.dao.entity.OrderStatus;
import by.epam.training.service.OrderService;
import by.epam.training.service.ServiceException;
import by.epam.training.service.dto.OrderDto;
import by.epam.training.service.dto.UserDto;
import by.epam.training.service.dto.VehicleDto;
import lombok.extern.log4j.Log4j;

import java.sql.SQLException;
import java.util.List;

@Log4j
public class OrderServiceImpl implements OrderService {
    private static final String CANNOT_SAVE_ORDER_IN_DATABASE = "Cannot save order in database.";
    private static final String CANNOT_GET_DRIVER_ORDERS = "Cannot get driver orders ";
    private static final String CANNOT_GET_CUSTOMER_S_ORDERS = "Cannot get customer's orders. ";
    private static final String CANNOT_DELETE_ORDER_WITH_ID = "Cannot delete order with id = ";
    private static final String ID_ORDER_WAS_DELETED_FROM_DATABASE = " = id order was deleted from database.";
    private ConnectionManager connectionManager;
    private TransactionManager transactionManager;
    private OrderDao orderDao;
    private UserDao userDao;
    private ContactDao contactDao;
    private WalletDao walletDao;
    private VehicleDao vehicleDao;

    public OrderServiceImpl(ConnectionManager connectionManager, OrderDao orderDao, UserDao userDao,
                            ContactDao contactDao, WalletDao walletDao, VehicleDao vehicleDao) {
        this.connectionManager = connectionManager;
        transactionManager = connectionManager.getTransactionManager();
        this.orderDao = orderDao;
        this.userDao = userDao;
        this.contactDao = contactDao;
        this.walletDao = walletDao;
        this.vehicleDao = vehicleDao;
    }

    @Override
    public Long create(OrderDto orderDto) throws ServiceException, TransactionException {
        long orderId = 0;
        try {
            transactionManager.beginTransaction();
            orderId = orderDao.save(orderDto);
            transactionManager.commitTransaction();
        } catch (SQLException e) {
            log.error(CANNOT_SAVE_ORDER_IN_DATABASE + e);
            throw new ServiceException(CANNOT_SAVE_ORDER_IN_DATABASE, e);
        }
        return orderId;
    }

    @Override
    public boolean update(OrderDto orderDto) throws ServiceException {
        return false;
    }

    @Override
    public boolean delete(OrderDto orderDto) throws ServiceException, TransactionException {
        boolean isDeleted;
        try {
            transactionManager.beginTransaction();
            isDeleted = orderDao.delete(orderDto.getId());
            transactionManager.commitTransaction();
        } catch (SQLException e) {
            log.error(CANNOT_DELETE_ORDER_WITH_ID + orderDto.getId(), e);
            throw new ServiceException(CANNOT_DELETE_ORDER_WITH_ID + orderDto.getId(), e);
        }
        if (isDeleted) {
            log.info(orderDto.getId() + ID_ORDER_WAS_DELETED_FROM_DATABASE);
        }
        return isDeleted;
    }

    @Override
    public List<OrderDto> getAll() throws ServiceException {
        return null;
    }

    @Override
    public List<OrderDto> getOrdersByVehicle(VehicleDto vehicleDto) throws ServiceException, TransactionException {
        List<OrderDto> resultOrderList;
        try {
            transactionManager.beginTransaction();
            resultOrderList = orderDao.getAllByVehicle(vehicleDto, userDao, contactDao, walletDao);
            transactionManager.commitTransaction();
        } catch (TransactionException | DaoException e) {
            log.error(CANNOT_GET_DRIVER_ORDERS + e);
            transactionManager.rollbackTransaction();
            throw new ServiceException(CANNOT_GET_DRIVER_ORDERS, e);
        }
        return resultOrderList;
    }

    @Override
    public List<OrderDto> getOrdersByCustomer(UserDto userDto) throws ServiceException, TransactionException {
        List<OrderDto> resultOrderList;
        try {
            transactionManager.beginTransaction();
            resultOrderList = orderDao.getAllByCustomer(userDto, userDao, contactDao, walletDao, vehicleDao);
            transactionManager.commitTransaction();
        } catch (TransactionException | DaoException e) {
            log.error(CANNOT_GET_CUSTOMER_S_ORDERS + e);
            transactionManager.rollbackTransaction();
            throw new ServiceException(CANNOT_GET_CUSTOMER_S_ORDERS, e);
        }
        return resultOrderList;
    }

    @Override
    public boolean updateStatus(Long orderId, OrderStatus newStatus) throws ServiceException, TransactionException {
        boolean isUpdated = false;
        try {
            transactionManager.beginTransaction();
            isUpdated = orderDao.updateStatus(orderId, newStatus);
            transactionManager.commitTransaction();
        } catch (TransactionException | DaoException e) {
            log.error(CANNOT_GET_CUSTOMER_S_ORDERS + e);
            transactionManager.rollbackTransaction();
            throw new ServiceException(CANNOT_GET_CUSTOMER_S_ORDERS, e);
        }
        return isUpdated;
    }
}
