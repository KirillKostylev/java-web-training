package by.epam.training.command.impl;

import by.epam.training.command.CommandException;
import by.epam.training.command.CommandName;
import by.epam.training.command.ServletCommand;
import by.epam.training.dao.TransactionException;
import by.epam.training.dao.entity.OrderStatus;
import by.epam.training.service.OrderService;
import by.epam.training.service.ServiceException;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
public class ChangeOrderStatusCommand implements ServletCommand {
    private static final String CANNOT_CHANGE_THE_STATUS = "Cannot change the status of an order with an id ";
    private static final String MESSAGE = ChangeOrderStatusCommand.class + " was executed. ";

    private OrderService orderServiceImpl;

    public ChangeOrderStatusCommand(OrderService orderServiceImpl) {
        this.orderServiceImpl = orderServiceImpl;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CommandException {
        log.debug(MESSAGE);
        String orderStatusStr = req.getParameter("order.status");
        String orderIdStr = req.getParameter("order.id");
        long orderId = Long.parseLong(orderIdStr);
        OrderStatus orderStatus = OrderStatus.valueOf(orderStatusStr);
        try {
            orderServiceImpl.updateStatus(orderId, orderStatus);
        } catch (ServiceException | TransactionException e) {
            log.error(CANNOT_CHANGE_THE_STATUS + orderId + e);
            throw new CommandException(CANNOT_CHANGE_THE_STATUS + orderId, e);
        }
        resp.sendRedirect(CommandName.VIEW_MY_ORDERS.getCommandPath());
    }
}
