package by.epam.training.command.impl;

import by.epam.training.command.CommandException;
import by.epam.training.command.CommandName;
import by.epam.training.command.ServletCommand;
import by.epam.training.dao.TransactionException;
import by.epam.training.service.ServiceException;
import by.epam.training.service.UserService;
import by.epam.training.service.VehicleService;
import by.epam.training.service.dto.UserDto;
import by.epam.training.service.dto.VehicleDto;
import by.epam.training.util.DistanceCalculator;
import by.epam.training.validator.ErrorKey;
import by.epam.training.validator.MakeOrderValidator;
import by.epam.training.validator.ValidationResult;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Log4j
public class CalculateTheOrderCostCommand implements ServletCommand {
    private static final String CANNOT_FOUND_VEHICLE_OWNER = "Cannot found vehicle owner.";
    private static final String START_MSG = CalculateTheOrderCostCommand.class + " was executed.";

    private VehicleService vehicleServiceImpl;
    private UserService userServiceImpl;


    public CalculateTheOrderCostCommand(VehicleService vehicleServiceImpl, UserService userServiceImpl) {
        this.vehicleServiceImpl = vehicleServiceImpl;
        this.userServiceImpl = userServiceImpl;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CommandException {
        log.debug(START_MSG);
        MakeOrderValidator makeOrderValidator = new MakeOrderValidator();
        ValidationResult validationResult = makeOrderValidator.validate(req);

        String loadingAddress = req.getParameter("order.loadingAddress");
        String unloadingAddress = req.getParameter("order.unloadingAddress");
        String loadingDate = req.getParameter("order.loadingDate");
        String unloadingDate = req.getParameter("order.unloadingDate");

        req.setAttribute("loadingAddress", loadingAddress);
        req.setAttribute("unloadingAddress", unloadingAddress);
        req.setAttribute("loadingDate", loadingDate);
        req.setAttribute("unloadingDate", unloadingDate);

        if (!validationResult.isValid()) {
            List<String> errors = new LinkedList<>();
            for (Map.Entry<ErrorKey, String> error : validationResult.getResult().entrySet()) {
                errors.add(error.getValue());
            }
            req.setAttribute("errors", errors);
            req.getRequestDispatcher(CommandName.VIEW_ORDER_FORM.getCommandPath().toLowerCase()).forward(req, resp);
            return;
        }
        double distance = DistanceCalculator.distanceCalculate(loadingAddress, unloadingAddress);
        distance = Math.rint(distance * 100) / 100;

        String vehicleOwnerLogin = req.getParameter("owner.login");
        VehicleDto vehicleDto = null;
        try {
            UserDto owner = userServiceImpl.getUserByLogin(vehicleOwnerLogin);
            vehicleDto = vehicleServiceImpl.getVehicleByUser(owner);
        } catch (ServiceException | TransactionException e) {
            log.error(CANNOT_FOUND_VEHICLE_OWNER + e);
        }
        double tariff;
        if (vehicleDto != null) {
            tariff = vehicleDto.getTariff();
        } else {
            tariff = 0;
        }
        double orderPrice = tariff * distance;
        orderPrice = Math.rint(orderPrice * 100) / 100;

        req.setAttribute("distance", distance);
        req.setAttribute("price", orderPrice);
        req.getRequestDispatcher(CommandName.VIEW_ORDER_FORM.getCommandPath()).forward(req, resp);
    }
}
