package by.epam.training.command.impl;

import by.epam.training.command.CommandException;
import by.epam.training.command.ServletCommand;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
public class ViewOrderFormCommand implements ServletCommand {

    private static final String PATH_TO_ORDER_FORM_JSP = "jsp/view/OrderForm.jsp";
    private static final String START_MSG = ViewOrderFormCommand.class + "was executed. ";

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CommandException {
        log.debug(START_MSG);
        String ownerLogin = req.getParameter("owner.login");
        req.setAttribute("ownerLogin", ownerLogin);
        req.getRequestDispatcher(PATH_TO_ORDER_FORM_JSP).forward(req, resp);
    }
}
