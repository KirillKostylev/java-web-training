package by.epam.training.dao.impl;

import by.epam.training.dao.ContactDao;
import by.epam.training.dao.ConnectionManager;
import by.epam.training.dao.DaoException;
import by.epam.training.dao.entity.OrderEntity;
import by.epam.training.dao.entity.OrderStatus;
import by.epam.training.dao.OrderDao;
import by.epam.training.service.dto.OrderDto;
import by.epam.training.dao.UserDao;
import by.epam.training.service.dto.UserDto;
import by.epam.training.dao.VehicleDao;
import by.epam.training.service.dto.VehicleDto;
import by.epam.training.dao.WalletDao;
import lombok.extern.log4j.Log4j;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j
public class OrderDaoImpl implements OrderDao {
    public static final String INSERT_ORDER_QUERY =
            "INSERT INTO reservation(order_time,loading_date,unloading_date,loading_address,unloading_address," +
                    "vehicle_id,customer_id,cost,order_status) VALUES(?,?,?,?,?,?,?,?,?);";
    private static final String SELECT_ALL_ORDERS_BY_VEHICLE_ID =
            "SELECT id,order_time,loading_date,unloading_date,loading_address,unloading_address,vehicle_id," +
                    "customer_id,cost,order_status FROM reservation WHERE vehicle_id=?;";
    private static final String SELECT_ALL_ORDERS_BY_CUSTOMER_ID =
            "SELECT id,order_time,loading_date,unloading_date,loading_address,unloading_address,vehicle_id," +
                    "customer_id,cost,order_status FROM reservation WHERE customer_id=?;";
    private static final String UPDATE_ORDER_STATUS_QUERY = "UPDATE reservation SET order_status=? WHERE id=?;";
    private static final String DELETE_ORDER_BY_ID_QUERY = "DELETE reservation FROM reservation WHERE id=?;";
    private static final String WAS_ADDED_IN_DATABASE = " was added in database.";
    private static final String CANNOT_ADD_ORDER_IN_DATABASE = "Cannot add order in database. ";
    private static final String CANNOT_GET_ORDERS_FROM_DATABASE = "Cannot get orders from database.";
    private static final String ORDER_STATUS_WAS_CHANGED = "Order status was changed.";
    private static final String CANNOT_UPDATE_ORDER_STATUS = "Cannot update order status";
    private static final String CANNOT_DELETE_ORDER_WITH_ID = "Cannot delete order with id: ";
    private static final String ID_ORDER_WAS_DELETED = " = id order was deleted.";
    private ConnectionManager connectionManager;

    public OrderDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override

    public Long save(OrderDto orderDto) throws DaoException {
        OrderEntity entity = fromDto(orderDto);
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement insertStmt = connection.prepareStatement(INSERT_ORDER_QUERY,
                Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            insertStmt.setTimestamp(++i, entity.getOrderTime());
            insertStmt.setTimestamp(++i, entity.getLoadingDate());
            insertStmt.setTimestamp(++i, entity.getUnloadingDate());
            insertStmt.setString(++i, entity.getLoadingAddress());
            insertStmt.setString(++i, entity.getUnloadingAddress());
            insertStmt.setLong(++i, entity.getVehicleId());
            insertStmt.setLong(++i, entity.getCustomerId());
            insertStmt.setDouble(++i, entity.getCost());
            insertStmt.setString(++i, entity.getStatus().getValue());
            insertStmt.executeUpdate();
            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                long id = generatedKeys.getLong(1);
                entity.setId(id);
                orderDto.setId(id);
            }
            log.debug(entity + WAS_ADDED_IN_DATABASE);
        } catch (SQLException e) {
            log.error(CANNOT_ADD_ORDER_IN_DATABASE + e);
            throw new DaoException(CANNOT_ADD_ORDER_IN_DATABASE, e);
        }
        return entity.getId();
    }


    @Override
    public boolean update(OrderDto orderDto) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(Long id) throws SQLException {
        boolean isDeleted;
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement delStmt = connection.prepareStatement(DELETE_ORDER_BY_ID_QUERY)) {
            delStmt.setLong(1, id);
            isDeleted = delStmt.executeUpdate() > 0;
        } catch (SQLException e) {
            log.error(CANNOT_DELETE_ORDER_WITH_ID + id, e);
            throw new DaoException(CANNOT_DELETE_ORDER_WITH_ID + id, e);
        }
        if (isDeleted) {
            log.info(id + ID_ORDER_WAS_DELETED);
        }
        return isDeleted;
    }

    @Override
    public OrderDto getById(Long id) throws SQLException {
        return null;
    }

    @Override
    public List<OrderDto> getAllByVehicle(VehicleDto vehicleDto, UserDao userDao, ContactDao contactDao,
                                          WalletDao walletDao) throws DaoException {
        List<OrderDto> orderList = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_ORDERS_BY_VEHICLE_ID)) {
            selectStmt.setLong(1, vehicleDto.getId());
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                OrderEntity entity = parseResultSet(resultSet);
                Optional<UserDto> customer = userDao.getById(entity.getCustomerId(), walletDao, contactDao);
                OrderDto orderDto = null;
                if (customer.isPresent()) {
                    orderDto = fromEntity(entity, vehicleDto, customer.get());
                }
                orderList.add(orderDto);
            }
        } catch (SQLException e) {
            log.error(CANNOT_GET_ORDERS_FROM_DATABASE + e);
            throw new DaoException(CANNOT_GET_ORDERS_FROM_DATABASE, e);
        }
        return orderList;
    }

    @Override
    public List<OrderDto> getAllByCustomer(UserDto userDto, UserDao userDao, ContactDao contactDao,
                                           WalletDao walletDao, VehicleDao vehicleDao) throws DaoException {
        List<OrderDto> orderList = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_ORDERS_BY_CUSTOMER_ID)) {
            selectStmt.setLong(1, userDto.getId());
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                OrderEntity entity = parseResultSet(resultSet);
                VehicleDto vehicleDto = vehicleDao.getById(entity.getVehicleId(), userDao, contactDao, walletDao);
                orderList.add(fromEntity(entity, vehicleDto, userDto));
            }
        } catch (SQLException e) {
            log.error(CANNOT_GET_ORDERS_FROM_DATABASE + e);
            throw new DaoException(CANNOT_GET_ORDERS_FROM_DATABASE, e);
        }
        return orderList;
    }

    @Override
    public boolean updateStatus(Long orderId, OrderStatus newStatus) throws DaoException {
        boolean isUpdated;
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement updStmt = connection.prepareStatement(UPDATE_ORDER_STATUS_QUERY)) {
            int i = 0;
            updStmt.setString(++i, newStatus.getValue());
            updStmt.setLong(++i, orderId);
            isUpdated = updStmt.executeUpdate() > 0;
            if (isUpdated) {
                log.debug(ORDER_STATUS_WAS_CHANGED);
            }
        } catch (SQLException e) {
            log.error(CANNOT_UPDATE_ORDER_STATUS + e);
            throw new DaoException(CANNOT_UPDATE_ORDER_STATUS, e);
        }
        return isUpdated;
    }

    private OrderEntity parseResultSet(ResultSet resultSet) throws SQLException {
        OrderEntity entity = new OrderEntity();

        long id = resultSet.getLong("id");
        Timestamp orderTime = resultSet.getTimestamp("order_time");
        Timestamp loadingDate = resultSet.getTimestamp("loading_date");
        Timestamp unloadingDate = resultSet.getTimestamp("unloading_date");
        String loadingAddress = resultSet.getString("loading_address");
        String unloadingAddress = resultSet.getString("unloading_address");
        long vehicleId = resultSet.getLong("vehicle_id");
        long customerId = resultSet.getLong("customer_id");
        double cost = resultSet.getDouble("cost");
        String order_status_str = resultSet.getString("order_status");
        OrderStatus status = OrderStatus.valueOf(order_status_str.toUpperCase());

        entity.setId(id);
        entity.setLoadingDate(loadingDate);
        entity.setUnloadingDate(unloadingDate);
        entity.setLoadingAddress(loadingAddress);
        entity.setUnloadingAddress(unloadingAddress);
        entity.setCost(cost);
        entity.setOrderTime(orderTime);
        entity.setStatus(status);
        entity.setVehicleId(vehicleId);
        entity.setCustomerId(customerId);

        return entity;
    }

    private OrderEntity fromDto(OrderDto dto) {
        OrderEntity entity = new OrderEntity();
        entity.setLoadingAddress(dto.getLoadingAddress());
        entity.setUnloadingAddress(dto.getUnloadingAddress());
        entity.setLoadingDate(dto.getLoadingDate());
        entity.setUnloadingDate(dto.getUnloadingDate());
        entity.setCost(dto.getCost());
        entity.setOrderTime(dto.getOrderTime());
        entity.setCustomerId(dto.getUserDto().getId());
        entity.setVehicleId(dto.getVehicleDto().getId());
        entity.setStatus(dto.getStatus());
        return entity;
    }

    private OrderDto fromEntity(OrderEntity entity, VehicleDto vehicleDto, UserDto customer) {
        OrderDto dto = new OrderDto();
        dto.setOrderTime(entity.getOrderTime());
        dto.setLoadingDate(entity.getLoadingDate());
        dto.setUnloadingDate(entity.getUnloadingDate());
        dto.setLoadingAddress(entity.getLoadingAddress());
        dto.setUnloadingAddress(entity.getUnloadingAddress());
        dto.setCost(entity.getCost());
        dto.setStatus(entity.getStatus());
        dto.setId(entity.getId());
        dto.setVehicleDto(vehicleDto);
        dto.setUserDto(customer);
        return dto;
    }
}
