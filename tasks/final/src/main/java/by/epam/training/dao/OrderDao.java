package by.epam.training.dao;

import by.epam.training.dao.entity.OrderStatus;
import by.epam.training.service.dto.OrderDto;
import by.epam.training.service.dto.UserDto;
import by.epam.training.service.dto.VehicleDto;

import java.util.List;

public interface OrderDao extends CRUDdao<OrderDto, Long> {
    List<OrderDto> getAllByVehicle(VehicleDto vehicleDto, UserDao userDao, ContactDao contactDao,
                                   WalletDao walletDao) throws DaoException;

    List<OrderDto> getAllByCustomer(UserDto userDto, UserDao userDao, ContactDao contactDao,
                                    WalletDao walletDao, VehicleDao vehicleDao) throws DaoException;

    boolean updateStatus(Long orderId, OrderStatus newStatus) throws DaoException;
}
