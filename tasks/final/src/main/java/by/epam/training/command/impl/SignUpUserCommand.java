package by.epam.training.command.impl;

import by.epam.training.command.CommandException;
import by.epam.training.command.CommandName;
import by.epam.training.command.ServletCommand;
import by.epam.training.dao.TransactionException;
import by.epam.training.dao.entity.UserRoles;
import by.epam.training.service.ServiceException;
import by.epam.training.service.UserService;
import by.epam.training.service.dto.ContactDto;
import by.epam.training.service.dto.UserDto;
import by.epam.training.service.dto.WalletDto;
import by.epam.training.util.MD5Util;
import by.epam.training.validator.ErrorKey;
import by.epam.training.validator.SignUpValidator;
import by.epam.training.validator.ValidationResult;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Log4j
public class SignUpUserCommand implements ServletCommand {
    private static final String CANNOT_CREATE_USER = "Cannot create user.";
    private static final String START_MSG = SignUpUserCommand.class + " was executed. ";

    private UserService userServiceImpl;

    public SignUpUserCommand(UserService userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException, CommandException {
        log.debug(START_MSG);

        String firstName = req.getParameter("user.firstName");
        String lastName = req.getParameter("user.lastName");
        String phoneNumber = req.getParameter("user.phoneNumber");
        String email = req.getParameter("user.email");
        String role = req.getParameter("user.role");
        String login = req.getParameter("user.login");
        String password = req.getParameter("user.password");

        SignUpValidator signUpValidator = new SignUpValidator(userServiceImpl);
        ValidationResult vr = signUpValidator.validate(req);
        if (!vr.isValid()) {
            List<String> errors = new LinkedList<>();
            for (Map.Entry<ErrorKey, String> error : vr.getResult().entrySet()) {
                errors.add(error.getValue());
            }
            req.setAttribute("errors", errors);
            req.getRequestDispatcher(CommandName.VIEW_SIGN_UP_FORM_COMMAND.getCommandPath().toLowerCase()).forward(req, resp);
            return;
        }

        ContactDto contactDto = new ContactDto();
        contactDto.setFirstName(firstName);
        contactDto.setLastName(lastName);
        contactDto.setPhoneNumber(phoneNumber);
        contactDto.setEmail(email);

        UserDto userDto = new UserDto();
        userDto.setRole(UserRoles.valueOf(role));
        userDto.setWalletDto(new WalletDto());
        userDto.setContactDto(contactDto);
        userDto.setLogin(login);
        userDto.setPassword(MD5Util.md5getString(password));
        userDto.setRegistrationDate(new Timestamp(System.currentTimeMillis()));

        try {
            userServiceImpl.create(userDto);
        } catch (ServiceException | TransactionException e) {
            req.getRequestDispatcher(CommandName.VIEW_SIGN_UP_FORM_COMMAND.getCommandPath().toLowerCase()).forward(req, resp);
            log.error(CANNOT_CREATE_USER + e);
            throw new CommandException(CANNOT_CREATE_USER, e);
        }
        resp.sendRedirect(CommandName.START_COMMAND.getCommandPath().toLowerCase());
    }
}
