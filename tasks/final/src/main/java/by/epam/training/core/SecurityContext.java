package by.epam.training.core;

import by.epam.training.command.CommandName;
import by.epam.training.dao.entity.UserRoles;
import lombok.extern.log4j.Log4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

@Log4j
public class SecurityContext {
    private static final String SECURITY_FILE_NAME = "security.properties";
    private static final String CANNOT_READ_SECURITY_FILE_MSG = SECURITY_FILE_NAME + " file cannot be read.";
    private static final String SECURITY_CONTEXT_WAS_INITIALIZED = SecurityContext.class + " was initialized. ";
    private final Map<String, Set<UserRoles>> commandsAndRoles = new HashMap<>();
    private static ReentrantLock lock = new ReentrantLock();
    private static SecurityContext instance;

    private SecurityContext() {
    }

    public static SecurityContext getInstance() {
        if (instance == null) {
            try {
                lock.lock();
                if (instance == null) {
                    instance = new SecurityContext();
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public void init() {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(SECURITY_FILE_NAME)) {
            Properties properties = new Properties();
            properties.load(inputStream);
            for (CommandName commandName : CommandName.values()) {
                String[] roles = properties.getProperty(commandName.name()).split(",");

                Set<UserRoles> commandRoles = new HashSet<>();
                for (String role : roles) {
                    UserRoles tempRole = UserRoles.valueOf(role.trim().toUpperCase());
                    commandRoles.add(tempRole);
                }
                commandsAndRoles.put(commandName.getCommandPath(), commandRoles);
            }
            log.info(SECURITY_CONTEXT_WAS_INITIALIZED);
        } catch (IOException e) {
            log.error(CANNOT_READ_SECURITY_FILE_MSG, e);
        }
    }

    public Set<UserRoles> getRolesForCommand(String commandName) {
        return commandsAndRoles.getOrDefault(commandName.toUpperCase(), new HashSet<>());
    }
}
