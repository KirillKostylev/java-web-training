package by.epam.training.validator;

import by.epam.training.dao.TransactionException;
import by.epam.training.filter.LocaleFilter;
import by.epam.training.service.ServiceException;
import by.epam.training.service.UserService;
import by.epam.training.service.dto.UserDto;
import by.epam.training.util.MD5Util;
import lombok.extern.log4j.Log4j;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.ResourceBundle;

import static by.epam.training.util.UtilityMethods.getValueFromCookies;
import static by.epam.training.util.UtilityMethods.stringIsEmpty;

@Log4j
public class SignInValidator implements Validator {
    private static final String VALIDATION_START_MESSAGE = "SignInValidator started validating. ";
    private static final String VALIDATION_ERROR_MESSAGE = "An error occurred during the validation process";
    private static final String INCORRECT_LOGIN_OR_PASSWORD_MESSAGE = "locale.error.incorrect.login.or.password";

    private UserService userServiceImpl;

    public SignInValidator(UserService userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    @Override
    public ValidationResult validate(HttpServletRequest request) {
        log.debug(VALIDATION_START_MESSAGE);
        ValidationResult validationResult = new ValidationResult();

        String locale = getValueFromCookies(request, LocaleFilter.COOKIE_NAME);
        // ru_RU split on ru and RU
        String language = locale.substring(0, 2);
        String country = locale.substring(3);
        ResourceBundle bundle = ResourceBundle.getBundle(PATH_TO_RESOURCE_BUNDLE, new Locale(language, country));

        String login = request.getParameter("user.login");
        String password = request.getParameter("user.password");

        if (stringIsEmpty(login) || stringIsEmpty(password)) {
            validationResult.addError(ErrorKey.EMPTY_FIELD, bundle.getString(EMPTY_FIELD_MESSAGE));
            return validationResult;
        }
        if (!login.matches(USER_LOGIN_AND_PASSWORD_REGEX)) {
            validationResult.addError(ErrorKey.INCORRECT_FORMAT_LOGIN, bundle.getString(INCORRECT_LOGIN_FORMAT_MESSAGE));
        }
        if (!password.matches(USER_LOGIN_AND_PASSWORD_REGEX)) {
            validationResult.addError(ErrorKey.INCORRECT_FORMAT_PASSWORD,
                    bundle.getString(INCORRECT_PASSWORD_FORMAT_MESSAGE));
        }
        if (!validationResult.isValid()) {
            return validationResult;
        }
        String hashPassword = MD5Util.md5getString(password);

        UserDto userDto;
        try {
            userDto = userServiceImpl.getUserByLogin(login);
            if (userDto == null || !userDto.getPassword().equals(hashPassword)) {
                validationResult.addError(ErrorKey.INCORRECT_LOGIN_OR_PASSWORD,
                        bundle.getString(INCORRECT_LOGIN_OR_PASSWORD_MESSAGE));
                return validationResult;
            }
        } catch (ServiceException | TransactionException e) {
            log.error(VALIDATION_ERROR_MESSAGE);
        }
        return validationResult;
    }

}
