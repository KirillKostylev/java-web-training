package by.epam.training.service;

import by.epam.training.dao.TransactionException;

import java.util.List;

public interface Service<ENTITY, KEY> {
    KEY create(ENTITY entity) throws ServiceException, TransactionException;

    boolean update(ENTITY entity) throws ServiceException, TransactionException;

    boolean delete(ENTITY entity) throws ServiceException, TransactionException;

    List<ENTITY> getAll() throws ServiceException;
}
