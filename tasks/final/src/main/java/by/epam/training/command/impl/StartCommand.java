package by.epam.training.command.impl;


import by.epam.training.command.ServletCommand;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
public class StartCommand implements ServletCommand {
    private static final String START_PAGE = "jsp/view/StartPage.jsp";
    private static final String START_MSG = StartCommand.class + " was executed. ";

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.debug(START_MSG);
        req.getRequestDispatcher(START_PAGE).forward(req, resp);
    }
}
