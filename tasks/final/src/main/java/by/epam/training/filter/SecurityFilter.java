package by.epam.training.filter;

import by.epam.training.core.SecurityContext;
import by.epam.training.dao.entity.UserRoles;
import by.epam.training.service.dto.UserDto;
import lombok.extern.log4j.Log4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

@Log4j
@WebFilter(urlPatterns = {"/*"}, servletNames = "ApplicationServlet", filterName = "SecurityFilter")
public class SecurityFilter implements Filter {

    private static final String START_MESSAGE = "SecurityFilter started work. ";
    private static final String FINAL_MESSAGE = "SecurityFilter finished work. ";
    private static final String MSG_FOR_403_ERROR = "Access denied";
    private static final String MSG_FOR_404_ERROR = "Page not found";
    public static final String ERROR_PAGE_PATH = "jsp/view/ErrorPage.jsp";
    public static final String ERROR_CODE = "error.code";
    public static final String ERROR_MESSAGE = "error.message";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        SecurityContext.getInstance().init();
        log.info(START_MESSAGE);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpReq = (HttpServletRequest) servletRequest;
        String command = httpReq.getRequestURI();

        if (command.isEmpty() || command.equals("/") || command.contains(".")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        UserRoles currentRole;
        HttpSession session = httpReq.getSession();
        UserDto user = (UserDto) session.getAttribute("user");
        if (user == null) {
            currentRole = UserRoles.VISITOR;
        } else {
            currentRole = user.getRole();
        }

        Set<UserRoles> rolesForCommand = SecurityContext.getInstance().getRolesForCommand(command);

        if (rolesForCommand.isEmpty()) {
            servletRequest.setAttribute(ERROR_CODE, "404");
            servletRequest.setAttribute(ERROR_MESSAGE, MSG_FOR_404_ERROR);
            httpReq.getRequestDispatcher(ERROR_PAGE_PATH).forward(servletRequest, servletResponse);
            return;
        }

        if (rolesForCommand.contains(currentRole)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            servletRequest.setAttribute(ERROR_CODE, "403");
            servletRequest.setAttribute(ERROR_MESSAGE, MSG_FOR_403_ERROR);
            httpReq.getRequestDispatcher(ERROR_PAGE_PATH).forward(servletRequest, servletResponse);
        }

    }

    @Override
    public void destroy() {
        log.info(FINAL_MESSAGE);
    }
}
