package by.epam.training.filter;


import lombok.extern.log4j.Log4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

@Log4j
@WebFilter(servletNames = "ApplicationServlet", urlPatterns = "/*", filterName = "LocaleFilter")
public class LocaleFilter implements Filter {
    public static final String COOKIE_NAME = "language";
    private static final String FILTER_INIT_MSG = "LocaleFilter was successful initialized. Default locale is: ";
    private static final String DEFAULT_LOCALE = "en_Us";
    private static final String LOCALE_COOKIE_WAS_NOT_FOUND = "Locale cookie wasn't found";
    private static final String LOCALE_COOKIE_FOUND = "Locale cookie found";

    @Override
    public void init(FilterConfig filterConfig) {
        String defaultLocale = filterConfig.getInitParameter("defaultLocale");
        if (defaultLocale == null) {
            defaultLocale = DEFAULT_LOCALE;
        }
        log.info(FILTER_INIT_MSG + defaultLocale);
    }


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String tempStr = httpRequest.getRequestURI();
        if (tempStr.contains(".") || tempStr.equals("/")) {
            chain.doFilter(request, response);
            return;
        }
        Cookie[] requestCookies = httpRequest.getCookies();

        Optional<Cookie> localeCookie = Optional.empty();

        if (requestCookies != null) {
            localeCookie = Arrays.stream(requestCookies)
                    .filter(cookie -> cookie.getName().equalsIgnoreCase(COOKIE_NAME))
                    .filter(cookie -> cookie.getValue().equalsIgnoreCase("ru_Ru") ||
                            cookie.getValue().equalsIgnoreCase("en_Us"))
                    .findFirst();
        }
        String lang;
        if (localeCookie.isPresent()) {
            lang = localeCookie.get().getValue();
        } else {
            log.debug(LOCALE_COOKIE_WAS_NOT_FOUND);
            ((HttpServletResponse) response).addCookie(new Cookie(COOKIE_NAME, DEFAULT_LOCALE));
            lang = DEFAULT_LOCALE;
        }
        request.setAttribute(COOKIE_NAME, lang);
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
