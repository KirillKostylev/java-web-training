package by.epam.training.command.impl;

import by.epam.training.command.CommandException;
import by.epam.training.command.ServletCommand;
import by.epam.training.dao.TransactionException;
import by.epam.training.service.ServiceException;
import by.epam.training.service.VehicleService;
import by.epam.training.service.dto.UserDto;
import by.epam.training.service.dto.VehicleDto;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Log4j
public class ViewMyVehicleCommand implements ServletCommand {

    private static final String VIEW_MY_VEHICLE_PATH = "jsp/view/MyVehicle.jsp";
    private static final String START_MSG = ViewMyVehicleCommand.class + " was executed. ";
    private static final String CANNOT_GET_VEHICLE = "Cannot get vehicle from database. ";

    private VehicleService vehicleServiceImpl;

    public ViewMyVehicleCommand(VehicleService vehicleServiceImpl) {
        this.vehicleServiceImpl = vehicleServiceImpl;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CommandException {
        log.debug(START_MSG);
        HttpSession session = req.getSession();
        try {
            UserDto user = (UserDto) session.getAttribute("user");
            VehicleDto vehicle = vehicleServiceImpl.getVehicleByUser(user);
            session.setAttribute("vehicle", vehicle);
        } catch (TransactionException | ServiceException e) {
            log.error(CANNOT_GET_VEHICLE, e);
            throw new CommandException(CANNOT_GET_VEHICLE, e);
        }
        req.getRequestDispatcher(VIEW_MY_VEHICLE_PATH).forward(req, resp);
    }
}
