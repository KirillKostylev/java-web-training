package by.epam.training.command.impl;

import by.epam.training.command.CommandException;
import by.epam.training.command.CommandName;
import by.epam.training.command.ServletCommand;
import by.epam.training.dao.TransactionException;
import by.epam.training.dao.entity.VehicleType;
import by.epam.training.service.ServiceException;
import by.epam.training.service.VehicleService;
import by.epam.training.service.dto.UserDto;
import by.epam.training.service.dto.VehicleDto;
import by.epam.training.util.UtilityMethods;
import by.epam.training.validator.AddVehicleValidator;
import by.epam.training.validator.ErrorKey;
import by.epam.training.validator.ValidationResult;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringEscapeUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


@Log4j
public class SaveVehicleCommand implements ServletCommand {

    public static final String ABSOLUTE_PATH_TO_DEFAULT_VEHICLE_IMG =
            "X:\\Study\\Epam\\java-web-training\\tasks\\final\\src\\main\\webapp\\img\\img_0.jpg";

    private static final String START_MSG = SaveVehicleCommand.class + " was executed. ";
    private static final String CANNOT_SAVE_VEHICLE_IN_DATABASE = "Cannot save vehicle in database. ";
    private static final String DEFAULT_IMG_NOT_FOUND_BY_PATH = "Default img not found by path: ";


    private VehicleService vehicleServiceImpl;

    public SaveVehicleCommand(VehicleService vehicleServiceImpl) {
        this.vehicleServiceImpl = vehicleServiceImpl;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CommandException {
        log.debug(START_MSG);
        HttpSession session = req.getSession();
        AddVehicleValidator validator = new AddVehicleValidator();
        ValidationResult vr = validator.validate(req);
        if (!vr.isValid()) {
            List<String> errors = new LinkedList<>();
            for (Map.Entry<ErrorKey, String> error : vr.getResult().entrySet()) {
                errors.add(error.getValue());
            }
            req.setAttribute("errors", errors);
            req.getRequestDispatcher(CommandName.VIEW_ADD_VEHICLE_COMMAND.getCommandPath().toLowerCase()).forward(req, resp);
            return;
        }

        String description = req.getParameter("vehicle.description");
        description = StringEscapeUtils.escapeHtml4(description);

        String location = req.getParameter("vehicle.location");
        location = StringEscapeUtils.escapeHtml4(location);

        String vehicleType = req.getParameter("vehicle.type");
        String strTariff = req.getParameter("vehicle.tariff");
        Part photoPart = req.getPart("vehicle.photo");
        UserDto user = (UserDto) session.getAttribute("user");

        double tariff = Double.parseDouble(strTariff);
        byte[] vehicleImgInBytes = getPhotoFromServletPart(photoPart);
        VehicleType type = VehicleType.valueOf(vehicleType);

        VehicleDto vehicleDto = new VehicleDto();
        vehicleDto.setDescription(description);
        vehicleDto.setLocation(location);
        String photoInBase64 = UtilityMethods.convertByteArrayToBase64(vehicleImgInBytes);
        vehicleDto.setPhoto(photoInBase64);
        vehicleDto.setTariff(tariff);
        vehicleDto.setType(type);
        vehicleDto.setUserDto(user);

        try {
            vehicleServiceImpl.create(vehicleDto);
            session.setAttribute("vehicle", vehicleDto);
        } catch (ServiceException | TransactionException e) {
            log.error(CANNOT_SAVE_VEHICLE_IN_DATABASE + e);
            throw new CommandException(CANNOT_SAVE_VEHICLE_IN_DATABASE, e);
        }
        resp.sendRedirect(CommandName.VIEW_MY_VEHICLE_COMMAND.getCommandPath());
    }

    private byte[] getPhotoFromServletPart(Part photoPart) throws IOException {
        byte[] vehicleImgInBytes = new byte[0];
        if (photoPart.getSize() > 0) {
            vehicleImgInBytes = UtilityMethods.getByteArrayFromInputStream(photoPart.getInputStream());
        } else {
            InputStream is = null;
            try {
                is = new BufferedInputStream(
                        new FileInputStream(ABSOLUTE_PATH_TO_DEFAULT_VEHICLE_IMG));
                vehicleImgInBytes = UtilityMethods.getByteArrayFromInputStream(is);
            } catch (FileNotFoundException e) {
                log.error(DEFAULT_IMG_NOT_FOUND_BY_PATH + ABSOLUTE_PATH_TO_DEFAULT_VEHICLE_IMG);
            }

        }
        return vehicleImgInBytes;
    }

}
