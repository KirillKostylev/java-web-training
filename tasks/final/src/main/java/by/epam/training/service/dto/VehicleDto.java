package by.epam.training.service.dto;

import by.epam.training.dao.entity.VehicleType;
import lombok.Data;

@Data
public class VehicleDto {
    private long id;
    private String location;
    private double tariff;
    private UserDto userDto;
    private VehicleType type;
    private String description;
    private String photo;
}
