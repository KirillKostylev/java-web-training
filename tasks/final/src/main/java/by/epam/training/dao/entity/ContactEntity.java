package by.epam.training.dao.entity;

import lombok.Data;

import java.sql.Blob;

@Data
public class ContactEntity {
    private long id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private Blob photo = null;

    public ContactEntity(long id, String firstName, String lastName, String email, String phoneNumber, Blob photo) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.photo = photo;
    }

    public ContactEntity() {
    }
}
