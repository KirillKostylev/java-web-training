package by.epam.training.dao.impl;

import by.epam.training.dao.ConnectionManager;
import by.epam.training.dao.DaoException;
import by.epam.training.dao.entity.WalletEntity;
import by.epam.training.dao.WalletDao;
import by.epam.training.service.dto.WalletDto;
import lombok.extern.log4j.Log4j;

import java.sql.*;

@Log4j
public class WalletDaoImpl implements WalletDao {
    private static final String INSERT_QUERY =
            "INSERT INTO wallet(count) VALUES (?);";
    private static final String UPDATE_QUERY =
            "UPDATE wallet SET count= ? WHERE id = ?;";
    private static final String FIND_BY_ID = "SELECT id, count FROM wallet WHERE id = ?;";

    private ConnectionManager connectionManager;

    public WalletDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Long save(WalletDto dto) throws DaoException {
        WalletEntity entity = fromDto(dto);
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement insertStmt = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            insertStmt.setDouble(++i, entity.getCount());
            insertStmt.executeUpdate();
            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
                dto.setId(generatedKeys.getLong(1));
                log.debug(entity + " was added in database.");
            }
        } catch (SQLException e) {
            log.error("Could not create wallet: " + e);
            throw new DaoException("Could not create wallet: " + e, e);
        }
        return entity.getId();
    }

    @Override
    public boolean update(WalletDto walletDto) throws SQLException {
        WalletEntity entity = fromDto(walletDto);
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {

            int i = 0;
            updateStmt.setDouble(++i, entity.getCount());
            updateStmt.setDouble(++i, entity.getId());
            boolean result = updateStmt.executeUpdate() > 0;
            if (result) {
                log.debug(entity + " was was changed.");
            }
            return result;

        } catch (SQLException e) {
            log.error("Could not change wallet: " + e);
            throw new DaoException("Could not change wallet: " + e, e);
        }

    }

    @Override
    public boolean delete(Long walletId) {
        return false;
    }

    @Override
    public WalletDto getById(Long id) throws DaoException {
        WalletEntity resultEntity = null;
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement selectStmt = connection.prepareStatement(FIND_BY_ID)) {
            selectStmt.setLong(1, id);

            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                resultEntity = parseResultSet(resultSet);
            }
        } catch (SQLException e) {
            log.error("Could not get wallet by id: " + e);
            throw new DaoException("Could not get wallet by id: ", e);
        }
        WalletDto dto = fromEntity(resultEntity);
        return dto;
    }

    private WalletEntity fromDto(WalletDto dto) {
        WalletEntity entity = new WalletEntity();
        entity.setId(dto.getId());
        entity.setCount(dto.getCount());
        return entity;
    }

    private WalletDto fromEntity(WalletEntity entity) {

        WalletDto dto = new WalletDto();
        dto.setId(entity.getId());
        dto.setCount(entity.getCount());
        return dto;
    }

    private WalletEntity parseResultSet(ResultSet resultSet) throws SQLException {
        WalletEntity walletEntity = new WalletEntity();

        long id = resultSet.getLong("id");
        double count = resultSet.getDouble("count");
        walletEntity.setId(id);
        walletEntity.setCount(count);
        return walletEntity;
    }
}
