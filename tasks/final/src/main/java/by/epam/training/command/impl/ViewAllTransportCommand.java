package by.epam.training.command.impl;

import by.epam.training.command.CommandException;
import by.epam.training.command.ServletCommand;
import by.epam.training.service.ServiceException;
import by.epam.training.service.VehicleService;
import by.epam.training.service.dto.VehicleDto;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Log4j
public class ViewAllTransportCommand implements ServletCommand {
    private static final String START_MSG = ViewAllTransportCommand.class + " was started.";
    private static final String CANNOT_GET_ALL_TRANSPORT = "Cannot get all transport.";
    private static final String PATH_TO_SELECT_VEHICLE_JSP = "jsp/view/SelectVehicle.jsp";
    private VehicleService vehicleServiceImpl;

    public ViewAllTransportCommand(VehicleService vehicleServiceImpl) {
        this.vehicleServiceImpl = vehicleServiceImpl;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CommandException {
        log.debug(START_MSG);
        try {
            List<VehicleDto> vehicleList = vehicleServiceImpl.getAll();
            req.setAttribute("vehicleList", vehicleList);
        } catch (ServiceException e) {
            log.error(CANNOT_GET_ALL_TRANSPORT + e);
            throw new CommandException(CANNOT_GET_ALL_TRANSPORT, e);
        }
        req.getRequestDispatcher(PATH_TO_SELECT_VEHICLE_JSP).forward(req, resp);
    }
}
