package by.epam.training.service;

import by.epam.training.dao.TransactionException;
import by.epam.training.service.dto.UserDto;

public interface UserService extends Service<UserDto, Long> {
    UserDto getUserByLogin(String login) throws ServiceException, TransactionException;
}
