package by.epam.training.dao.entity;

public enum UserRoles {

    ADMIN("admin"),
    DRIVER("driver"),
    CLIENT("client"),
    VISITOR("visitor");

    private String value;

    UserRoles(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
