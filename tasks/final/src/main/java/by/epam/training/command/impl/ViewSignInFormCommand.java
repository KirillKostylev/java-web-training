package by.epam.training.command.impl;

import by.epam.training.command.ServletCommand;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
public class ViewSignInFormCommand implements ServletCommand {

    private static final String PATH_TO_SIGN_IN_FORM = "/jsp/view/SignIn.jsp";
    private static final String START_MSG = ViewSignInFormCommand.class + " was executed. ";

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.debug(START_MSG);
        req.getRequestDispatcher(PATH_TO_SIGN_IN_FORM).forward(req, resp);
    }
}
