package by.epam.training.service.impl;

import by.epam.training.dao.*;
import by.epam.training.service.ServiceException;
import by.epam.training.service.VehicleService;
import by.epam.training.service.dto.UserDto;
import by.epam.training.service.dto.VehicleDto;
import lombok.extern.log4j.Log4j;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Log4j
public class VehicleServiceImpl implements VehicleService {
    private static final String CANNOT_SAVE_VEHICLE = "Cannot save vehicle. ";
    private static final String CANNOT_GET_VEHICLE_BY_USER_ID = "Cannot get vehicle by userId: ";
    private static final String CANNOT_DELETE_VEHICLE_FROM_USER = "Cannot delete vehicle from user: ";
    private static final String CANNOT_UPDATE_VEHICLE = "Cannot update vehicle. ";
    private static final String DELETED_HIS_VEHICLE_FROM_DATABASE = " deleted his vehicle from database.";
    private static final String CHANGED_INFO_ABOUT_HIS_VEHICLE = " changed info about his vehicle.";
    private static final String ADDED_HIS_VEHICLE_TO_THE_DATABASE = " added his vehicle to the database. ";
    private static final String CANNOT_GET_ALL_TRANSPORT = "Cannot get all transport. ";
    private ConnectionManager connectionManager;
    private TransactionManager transactionManager;
    private VehicleDao vehicleDao;
    private UserDao userDao;
    private WalletDao walletDao;
    private ContactDao contactDao;

    public VehicleServiceImpl(ConnectionManager connectionManager, VehicleDao vehicleDao, UserDao userDao,
                              ContactDao contactDao, WalletDao walletDao) {
        this.connectionManager = connectionManager;
        transactionManager = connectionManager.getTransactionManager();
        this.vehicleDao = vehicleDao;
        this.userDao = userDao;
        this.contactDao = contactDao;
        this.walletDao = walletDao;
    }

    @Override
    public Long create(VehicleDto vehicleDto) throws ServiceException, TransactionException {
        long vehicleId;
        try {
            transactionManager.beginTransaction();
            vehicleId = vehicleDao.save(vehicleDto);
            transactionManager.commitTransaction();
        } catch (SQLException e) {
            log.error(CANNOT_SAVE_VEHICLE + e);
            transactionManager.rollbackTransaction();
            throw new ServiceException(CANNOT_SAVE_VEHICLE, e);
        }
        if (vehicleId > 0) {
            log.info(vehicleDto.getUserDto().getLogin() + ADDED_HIS_VEHICLE_TO_THE_DATABASE);
        }
        return vehicleId;
    }

    @Override
    public VehicleDto getVehicleByUser(UserDto userDto) throws TransactionException, ServiceException {
        Optional<VehicleDto> dto;
        VehicleDto resultDto = null;
        try {
            transactionManager.beginTransaction();
            dto = vehicleDao.getVehicleByUser(userDto);
            if (dto.isPresent()) {
                resultDto = dto.get();
            }
            transactionManager.commitTransaction();
        } catch (TransactionException | DaoException e) {
            log.error(CANNOT_GET_VEHICLE_BY_USER_ID + userDto.getId() + e);
            transactionManager.rollbackTransaction();
            throw new ServiceException(CANNOT_GET_VEHICLE_BY_USER_ID + userDto.getId(), e);
        }
        return resultDto;
    }

    @Override
    public boolean delete(VehicleDto vehicleDto) throws ServiceException, TransactionException {
        boolean isDeleted = false;
        try {
            transactionManager.beginTransaction();
            isDeleted = vehicleDao.delete(vehicleDto.getId());
            transactionManager.commitTransaction();
        } catch (SQLException e) {
            String userLogin = vehicleDto.getUserDto().getLogin();
            transactionManager.rollbackTransaction();
            log.error(CANNOT_DELETE_VEHICLE_FROM_USER + userLogin + e);
            throw new ServiceException(CANNOT_DELETE_VEHICLE_FROM_USER + userLogin, e);
        }
        if (isDeleted) {
            log.info(vehicleDto.getUserDto().getLogin() + DELETED_HIS_VEHICLE_FROM_DATABASE);
        }
        return isDeleted;
    }

    @Override
    public List<VehicleDto> getAll() throws ServiceException {
        Optional<List<VehicleDto>> transportList;
        List<VehicleDto> resultList = null;
        try {
            transactionManager.beginTransaction();
            transportList = vehicleDao.getAllTransport(userDao, contactDao, walletDao);
            transactionManager.commitTransaction();
        } catch (TransactionException | DaoException e) {
            log.error(CANNOT_GET_ALL_TRANSPORT + e);
            throw new ServiceException(CANNOT_GET_ALL_TRANSPORT, e);
        }
        if (transportList.isPresent()) {
            resultList = transportList.get();
        }
        return resultList;
    }

    @Override
    public boolean update(VehicleDto vehicleDto) throws ServiceException, TransactionException {
        boolean isChanged;
        try {
            transactionManager.beginTransaction();
            isChanged = vehicleDao.update(vehicleDto);
            transactionManager.commitTransaction();
        } catch (SQLException e) {
            log.error(CANNOT_UPDATE_VEHICLE + e);
            transactionManager.rollbackTransaction();
            throw new ServiceException(CANNOT_UPDATE_VEHICLE, e);
        }
        if (isChanged) {
            log.info(vehicleDto.getUserDto().getLogin() + CHANGED_INFO_ABOUT_HIS_VEHICLE);
        }
        return isChanged;

    }
}
