package by.epam.training.service.dto;

import by.epam.training.dao.entity.OrderStatus;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class OrderDto {
    private long id;
    private Timestamp orderTime;
    private Timestamp loadingDate;
    private Timestamp unloadingDate;
    private String loadingAddress;
    private String unloadingAddress;
    private VehicleDto vehicleDto;
    private UserDto userDto;
    private double cost;
    private OrderStatus status;
}
