package by.epam.training.command.impl;

import by.epam.training.command.CommandName;
import by.epam.training.command.ServletCommand;
import lombok.extern.log4j.Log4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Log4j
public class LogOutUserCommand implements ServletCommand {

    private static final String LOGOUT_USER_ERROR = "Logout user error. ";
    private static final String START_MSG = LogOutUserCommand.class + " was executed. ";

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug(START_MSG);
        HttpSession session = req.getSession();
        session.removeAttribute("user");
        session.removeAttribute("vehicle");
        try {
            resp.sendRedirect(CommandName.START_COMMAND.getCommandPath().toLowerCase());
        } catch (IOException e) {
            log.error(LOGOUT_USER_ERROR);
        }
    }
}
