package by.epam.training.service.dto;

import lombok.Data;

import java.sql.Blob;

@Data
public class ContactDto {
    private long id;
    private String email;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private Blob photo;
}
