package by.epam.training.util;

import lombok.extern.log4j.Log4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Base64;
import java.util.concurrent.atomic.AtomicLong;

@Log4j
public class UtilityMethods {


    private static final String CANNOT_CONVERT_BYTE_ARRAY_FROM_PART = "Cannot convert byte array from input stream";

    public static boolean stringIsNull(String str) {
        return str == null;
    }

    public static boolean stringIsEmpty(String str) {
        if (stringIsNull(str)) {
            return true;
        }
        return str.isEmpty();
    }

    public static String getValueFromCookies(HttpServletRequest request, String key) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(key)) {
                    return cookie.getValue();
                }
            }
        }
        return "";
    }

    public static Timestamp convertDatetimeLocalStringToTimestamp(String date) {
        if (StringUtils.countMatches(date, ":") == 1) {
            date += ":00";
        }
        return Timestamp.valueOf(date.replace("T", " "));
    }

    public static byte[] getByteArrayFromInputStream(InputStream is)  {
        byte[] byteArray = new byte[0];
        try {
            byteArray= IOUtils.toByteArray(is);
        } catch (IOException e) {
            log.error(CANNOT_CONVERT_BYTE_ARRAY_FROM_PART,e);
        }
        return byteArray;
    }

    public static String convertByteArrayToBase64(byte[] array){
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(array);
    }

    public static byte[] convertBase64ToByteArray(String base64){
        Base64.Decoder encoder = Base64.getDecoder();
        return encoder.decode(base64);
    }
}
