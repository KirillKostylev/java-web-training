package by.epam.training.service.dto;

import by.epam.training.dao.entity.UserRoles;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class UserDto {
    private long id;
    private UserRoles role;
    private String login;
    private String password;
    private Timestamp registrationDate;
    private ContactDto contactDto;
    private WalletDto walletDto;

}

