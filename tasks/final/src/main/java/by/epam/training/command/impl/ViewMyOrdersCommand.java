package by.epam.training.command.impl;

import by.epam.training.command.CommandException;
import by.epam.training.command.ServletCommand;
import by.epam.training.dao.TransactionException;
import by.epam.training.dao.entity.UserRoles;
import by.epam.training.service.OrderService;
import by.epam.training.service.ServiceException;
import by.epam.training.service.UserService;
import by.epam.training.service.VehicleService;
import by.epam.training.service.dto.OrderDto;
import by.epam.training.service.dto.UserDto;
import by.epam.training.service.dto.VehicleDto;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Log4j
public class ViewMyOrdersCommand implements ServletCommand {
    private static final String START_MSG = ViewMyOrdersCommand.class + " was executed.";
    private static final String PATH_TO_MY_ORDERS_JSP = "jsp/view/MyOrders.jsp";
    private static final String CANNOT_GET_THE_LIST_OF_ORDERS_OF_THE_DRIVER = "Cannot get the list of orders of the driver: ";
    private static final String CANNOT_GET_THE_LIST_OF_CUSTOMER_ORDERS = "Cannot get the list of customer orders: ";
    private OrderService orderServiceImpl;
    private VehicleService vehicleServiceImpl;
    private UserService userServiceImpl;

    public ViewMyOrdersCommand(OrderService orderServiceImpl, VehicleService vehicleServiceImpl, UserService userServiceImpl) {
        this.orderServiceImpl = orderServiceImpl;
        this.vehicleServiceImpl = vehicleServiceImpl;
        this.userServiceImpl = userServiceImpl;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CommandException {
        log.debug(START_MSG);
        List<OrderDto> orderList = new ArrayList<>();
        HttpSession session = req.getSession();
        UserDto userDto = (UserDto) session.getAttribute("user");
        if (userDto.getRole().equals(UserRoles.DRIVER)) {
            VehicleDto vehicle = (VehicleDto) session.getAttribute("vehicle");
            try {
                if (vehicle == null) {
                    vehicle = vehicleServiceImpl.getVehicleByUser(userDto);
                }
                if (vehicle != null) {
                    orderList = orderServiceImpl.getOrdersByVehicle(vehicle);
                }
            } catch (TransactionException | ServiceException e) {
                log.error(CANNOT_GET_THE_LIST_OF_ORDERS_OF_THE_DRIVER + userDto.getLogin() + e);
                throw new CommandException(CANNOT_GET_THE_LIST_OF_ORDERS_OF_THE_DRIVER + userDto.getLogin(), e);
            }
        } else {
            try {
                orderList = orderServiceImpl.getOrdersByCustomer(userDto);
            } catch (ServiceException | TransactionException e) {
                log.error(CANNOT_GET_THE_LIST_OF_CUSTOMER_ORDERS + userDto.getLogin() + e);
                throw new CommandException(CANNOT_GET_THE_LIST_OF_CUSTOMER_ORDERS + userDto.getLogin(), e);
            }

        }
        req.setAttribute("orderList", orderList);
        req.getRequestDispatcher(PATH_TO_MY_ORDERS_JSP).forward(req, resp);
    }
}
