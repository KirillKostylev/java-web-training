package by.epam.training.dao.impl;

import by.epam.training.dao.ContactDao;
import by.epam.training.service.dto.ContactDto;
import by.epam.training.dao.ConnectionManager;
import by.epam.training.dao.DaoException;
import by.epam.training.dao.entity.ContactEntity;
import lombok.extern.log4j.Log4j;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Log4j
public class ContactDaoImpl implements ContactDao {

    private static final String INSERT_QUERY =
            "INSERT INTO contact(first_name, last_name, email,phone_number, photo) VALUES (?,?,?,?,?);";
    private static final String UPDATE_QUERY =
            "UPDATE contact SET first_name=?, last_name=?, email=?, phone_number=?, photo=? WHERE id = ?;";
    private static final String FIND_BY_ID =
            "SELECT id, first_name, last_name, email, phone_number, photo FROM contact WHERE id = ?;";

    private ConnectionManager connectionManager;

    public ContactDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Long save(ContactDto dto) throws SQLException {
        ContactEntity entity = fromDto(dto);
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement insertStmt = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            int i = 0;
            insertStmt.setString(++i, entity.getFirstName());
            insertStmt.setString(++i, entity.getLastName());
            insertStmt.setString(++i, entity.getEmail());
            insertStmt.setString(++i, entity.getPhoneNumber());
            insertStmt.setBlob(++i, entity.getPhoto());
            insertStmt.executeUpdate();

            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
                dto.setId(generatedKeys.getLong(1));
                log.debug(entity + " was added in database.");
            }
        } catch (SQLException e) {
            log.error("Could not create contact: " + e);
            throw new DaoException("Could not create contact: " + e, e);
        }
        return entity.getId();
    }

    @Override
    public boolean update(ContactDto dto) throws DaoException {
        ContactEntity entity = fromDto(dto);
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {

            int i = 0;
            updateStmt.setString(++i, entity.getFirstName());
            updateStmt.setString(++i, entity.getLastName());
            updateStmt.setString(++i, entity.getEmail());
            updateStmt.setString(++i, entity.getPhoneNumber());
            updateStmt.setBlob(++i, entity.getPhoto());
            updateStmt.setLong(++i, entity.getId());
            boolean result = updateStmt.executeUpdate() > 0;
            if (result) {
                log.debug(entity + " was changed.");
            }
            return result;

        } catch (SQLException e) {
            log.error("Could not change contact: " + e);
            throw new DaoException("Could not change contact: " + e, e);
        }
    }

    @Override
    public boolean delete(Long ContactId) throws DaoException {
        return false;
    }

    @Override
    public ContactDto getById(Long id) throws DaoException {
        List<ContactEntity> result = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        try (PreparedStatement getByIdStmt = connection.prepareStatement(FIND_BY_ID)) {

            getByIdStmt.setLong(1, id);
            ResultSet resultSet = getByIdStmt.executeQuery();
            while (resultSet.next()) {
                ContactEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            log.error("Could not fount contact: " + e);
            throw new DaoException("Could not fount contact: " + e, e);
        }
        return result.stream()
                .map(this::fromEntity)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("ContactEntity not found with given id: " + id));
    }

    private ContactEntity parseResultSet(ResultSet resultSet) throws SQLException {
        long contactId = resultSet.getLong("id");
        String firstName = resultSet.getString("first_name");
        String lastName = resultSet.getString("last_name");
        String email = resultSet.getString("email");
        String phoneNumber = resultSet.getString("phone_number");
        Blob photo = resultSet.getBlob("photo");

        ContactEntity contactEntity = new ContactEntity();
        contactEntity.setId(contactId);
        contactEntity.setFirstName(firstName);
        contactEntity.setLastName(lastName);
        contactEntity.setEmail(email);
        contactEntity.setPhoneNumber(phoneNumber);
        contactEntity.setPhoto(photo);
        return contactEntity;
    }

    private ContactEntity fromDto(ContactDto dto) {
        ContactEntity entity = new ContactEntity();
        entity.setEmail(dto.getEmail());
        entity.setFirstName(dto.getFirstName());
        entity.setId(dto.getId());
        entity.setLastName(dto.getLastName());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setPhoto(dto.getPhoto());
        return entity;
    }

    private ContactDto fromEntity(ContactEntity entity) {

        ContactDto dto = new ContactDto();
        dto.setEmail(entity.getEmail());
        dto.setFirstName(entity.getFirstName());
        dto.setId(entity.getId());
        dto.setLastName(entity.getLastName());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setPhoto(entity.getPhoto());
        return dto;
    }
}
