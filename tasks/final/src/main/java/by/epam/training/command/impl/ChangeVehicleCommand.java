package by.epam.training.command.impl;

import by.epam.training.command.CommandException;
import by.epam.training.command.CommandName;
import by.epam.training.command.ServletCommand;
import by.epam.training.dao.TransactionException;
import by.epam.training.dao.entity.VehicleType;
import by.epam.training.service.ServiceException;
import by.epam.training.service.VehicleService;
import by.epam.training.service.dto.VehicleDto;
import by.epam.training.util.UtilityMethods;
import by.epam.training.validator.AddVehicleValidator;
import by.epam.training.validator.ErrorKey;
import by.epam.training.validator.ValidationResult;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringEscapeUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


@Log4j
public class ChangeVehicleCommand implements ServletCommand {
    private static final String CANNOT_UPDATE_VEHICLE_WITH_ID = "Cannot update vehicle with id=";
    private static final String START_MSG = ChangeVehicleCommand.class + " was executed. ";
    private static final String CANNOT_FIND_THE_USER_S_VEHICLE = "Cannot find the user's vehicle ";
    private VehicleService vehicleServiceImpl;

    public ChangeVehicleCommand(VehicleService vehicleServiceImpl) {
        this.vehicleServiceImpl = vehicleServiceImpl;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CommandException {
        log.debug(START_MSG);
        AddVehicleValidator validator = new AddVehicleValidator();
        ValidationResult validationResult = validator.validate(req);
        if (!validationResult.isValid()) {
            List<String> errors = new LinkedList<>();
            for (Map.Entry<ErrorKey, String> error : validationResult.getResult().entrySet()) {
                errors.add(error.getValue());
            }
            req.setAttribute("errors", errors);
            req.getRequestDispatcher(CommandName.VIEW_CHANGE_VEHICLE_FORM_COMMAND.getCommandPath()).forward(req, resp);
        }

        VehicleDto vehicleDto = (VehicleDto) req.getSession().getAttribute("vehicle");

        String vehicleTypeString = req.getParameter("vehicle.type");
        String tariffStr = req.getParameter("vehicle.tariff");

        String description = req.getParameter("vehicle.description");
        description = StringEscapeUtils.escapeHtml4(description);

        String location = req.getParameter("vehicle.location");
        location = StringEscapeUtils.escapeHtml4(location);

        Part photo = req.getPart("vehicle.photo");

        VehicleType vehicleType = VehicleType.valueOf(vehicleTypeString);
        double tariff = Double.parseDouble(tariffStr);

        if (photo.getSize() > 0) {
            InputStream is = photo.getInputStream();
            byte[] vehiclePhotoInBytes = UtilityMethods.getByteArrayFromInputStream(is);
            String vehiclePhotoInBase64 = UtilityMethods.convertByteArrayToBase64(vehiclePhotoInBytes);
            vehicleDto.setPhoto(vehiclePhotoInBase64);
        }
        vehicleDto.setType(vehicleType);
        vehicleDto.setTariff(tariff);
        vehicleDto.setDescription(description);
        vehicleDto.setLocation(location);

        try {
            vehicleServiceImpl.update(vehicleDto);
        } catch (ServiceException | TransactionException e) {
            log.error(CANNOT_UPDATE_VEHICLE_WITH_ID + vehicleDto.getId() + e);
            throw new CommandException(CANNOT_UPDATE_VEHICLE_WITH_ID + vehicleDto.getId(), e);
        }
        resp.sendRedirect(CommandName.VIEW_MY_VEHICLE_COMMAND.getCommandPath());
    }
}
