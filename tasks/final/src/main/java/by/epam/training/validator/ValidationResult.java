package by.epam.training.validator;

import lombok.Data;
import lombok.extern.log4j.Log4j;

import java.util.HashMap;
import java.util.Map;

@Log4j
@Data
public class ValidationResult {
    private Map<ErrorKey, String> result;

    public ValidationResult() {
        result = new HashMap<>();
    }

    public boolean isValid() {
        return result.isEmpty();
    }

    public void addError(ErrorKey key, String message) {
        log.warn(key + " : " + message);
        result.put(key, message);
    }

    public Map<ErrorKey, String> getResult() {
        return result;
    }
}
