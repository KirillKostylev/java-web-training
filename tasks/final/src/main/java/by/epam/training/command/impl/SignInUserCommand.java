package by.epam.training.command.impl;

import by.epam.training.command.CommandException;
import by.epam.training.command.CommandName;
import by.epam.training.command.ServletCommand;
import by.epam.training.dao.TransactionException;
import by.epam.training.service.ServiceException;
import by.epam.training.service.UserService;
import by.epam.training.service.dto.UserDto;
import by.epam.training.validator.ErrorKey;
import by.epam.training.validator.SignInValidator;
import by.epam.training.validator.ValidationResult;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Log4j
public class SignInUserCommand implements ServletCommand {
    private static final String COMMAND_WAS_EXECUTED_MESSAGE = SignInUserCommand.class + " was executed. ";
    private static final String GET_USER_EXCEPTION = "Cannot get user from db.";

    private UserService userServiceImpl;

    public SignInUserCommand(UserService userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CommandException {
        log.debug(COMMAND_WAS_EXECUTED_MESSAGE);
        SignInValidator signInValidator = new SignInValidator(userServiceImpl);
        ValidationResult vr = signInValidator.validate(req);
        UserDto userDto;
        if (!vr.isValid()) {
            List<String> errors = new LinkedList<>();
            for (Map.Entry<ErrorKey, String> error : vr.getResult().entrySet()) {
                errors.add(error.getValue());
            }
            req.setAttribute("errors", errors);
            req.getRequestDispatcher(CommandName.VIEW_SIGN_IN_FORM_COMMAND.getCommandPath().toLowerCase()).forward(req, resp);
            return;
        }
        try {
            userDto = userServiceImpl.getUserByLogin(req.getParameter("user.login"));
            log.info(userDto.getLogin() + " logged in.");
            req.getSession().setAttribute("user", userDto);
        } catch (ServiceException | TransactionException e) {
            log.error(GET_USER_EXCEPTION + e);
            throw new CommandException(GET_USER_EXCEPTION, e);
        }
        resp.sendRedirect(CommandName.START_COMMAND.getCommandPath().toLowerCase());
    }
}