package by.epam.training.dao;

import by.epam.training.service.dto.WalletDto;

public interface WalletDao extends CRUDdao<WalletDto, Long> {
}
