package by.epam.training.dao.entity;

import lombok.Data;

@Data
public class VehicleEntity {
    private long id;
    private String location;
    private double tariff;
    private long ownerId;
    private VehicleType type;
    private String description;
    private byte[] photo;
}
