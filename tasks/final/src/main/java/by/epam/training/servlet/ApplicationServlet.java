package by.epam.training.servlet;

import by.epam.training.command.CommandException;
import by.epam.training.command.CommandName;
import by.epam.training.command.ServletCommand;
import by.epam.training.core.ApplicationContext;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
@WebServlet(urlPatterns = "/", loadOnStartup = 1, name = "ApplicationServlet")
@MultipartConfig
public class ApplicationServlet extends HttpServlet {

    private static final String COMMAND_NOT_FOUND = "Command not found";
    private static final String EXECUTE_ERROR = "Execute error: ";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String context = req.getContextPath();
        String requestURI = req.getRequestURI();
        int contextLength = context.length();
        String action;
        action = requestURI.substring(contextLength);

        ServletCommand command = ApplicationContext.getInstance().getCommand(action.toUpperCase());
        if (action.equals("/")) {
            command = ApplicationContext.getInstance().getCommand(CommandName.START_COMMAND.getCommandPath());
        }
        try {
            command.execute(req, resp);
        } catch (CommandException e) {
            log.error(EXECUTE_ERROR + e.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}

