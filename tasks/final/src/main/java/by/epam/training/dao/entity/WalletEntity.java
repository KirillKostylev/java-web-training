package by.epam.training.dao.entity;

import lombok.Data;

@Data
public class WalletEntity {
    private long id;
    private double count;

    public WalletEntity(long id, double count) {
        this.id = id;
        this.count = count;
    }

    public WalletEntity() {
    }
}
