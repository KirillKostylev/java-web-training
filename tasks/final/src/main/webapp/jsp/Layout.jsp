<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<script type="text/javascript">
    function changeLang(name) {
        document.cookie = "language=" + name;
        window.location.reload(true);
    }
</script>

<fmt:setLocale value="${requestScope.get('language')}"/>

<fmt:setBundle basename="/i18n/localization" scope="application"/>
<fmt:message key="locale.layout.login" var="login"/>
<fmt:message key="locale.layout.signup" var="signup"/>
<fmt:message key="locale.layout.logout" var="logout"/>
<fmt:message key="locale.layout.home" var="home"/>
<fmt:message key="locale.layout.title" var="title"/>
<fmt:message key="locale.layout.lang.ru" var="ru"/>
<fmt:message key="locale.layout.lang.en" var="en"/>
<fmt:message key="locale.layout.user.management" var="userManagement"/>
<fmt:message key="locale.layout.profile" var="profile"/>
<fmt:message key="locale.layout.myVehicle" var="myCar"/>
<fmt:message key="locale.layout.select.vehicle" var="selectVehicle"/>
<fmt:message key="locale.my.order.orders" var="orders"/>
<html>
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/mdb.min.css">
    <link rel="stylesheet" href="../css/style.css">

</head>
<body class="body">
<header class="header">
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark black scrolling-navbar">
        <div class="container">
            <a href="start" class="navbar-brand waves-effect">
                <strong class="blue-text">Transportation platform</strong>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
                    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="basicExampleNav">
                <ul class="navbar-nav mr-auto">
                    <c:if test="${user.role == 'ADMIN'}">
                        <li class="nav-item">
                            <a class="nav-link" href="user-management">${userManagement}</a>
                        </li>
                    </c:if>
                    <c:if test="${user.role == 'DRIVER'}">
                        <li class="nav-item">
                            <a class="nav-link" href="view-my-vehicle">${myCar}</a>
                        </li>
                    </c:if>
                    <li class="nav-item">
                        <a class="nav-link" href="view-all-transport">${selectVehicle}</a>
                    </li>
                </ul>
                <ul class="navbar-nav nav-flex-icons">
                    <div class="list-group list-group-horizontal">
                        <a href="#" type="radio" class="nav-link waves-effect waves-light"
                           onclick="changeLang('en_Us')">${en}</a>

                        <a href="#" type="radio" class="nav-link waves-effect waves-light"
                           onclick="changeLang('ru_Ru')">${ru}</a>
                    </div>
                    <c:if test="${user.role == 'CLIENT' or user.role == 'DRIVER'}">
                        <li class="nav-item">
                            <a class="nav-link" href="view-my-orders">${orders}</a>
                        </li>
                    </c:if>

                    <c:if test="${user == null}">
                        <li class="nav-item">
                            <a class="nav-link" href="view-sign-in-form">${login}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="view-sign-up-form">
                                    ${signup}</a>
                        </li>
                    </c:if>
                    <c:if test="${user != null}">
                        <li class="nav-item">
                            <a class="nav-link" href="user-log-out">
                                    ${logout}</a>
                        </li>
                    </c:if>
                </ul>
            </div>
        </div>
    </nav>
</header>

<script type="text/javascript" src="../js/jquery.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Your custom scripts (optional) -->
<script type="text/javascript"></script>

</body>
</html>
