<%--
  Created by IntelliJ IDEA.
  User: Kirill Kostylev
  Date: 2/5/2020
  Time: 14:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="../Layout.jsp" %>

<fmt:message key="locale.my.order.title" var="title"/>
<fmt:message key="locale.my.order.time" var="time"/>
<fmt:message key="locale.signup.driver" var="driver"/>
<fmt:message key="locale.my.order.address" var="address"/>
<fmt:message key="locale.my.order.time.order" var="timeOrder"/>
<fmt:message key="locale.my.order.status" var="status"/>
<fmt:message key="locale.my.order.price" var="price"/>
<fmt:message key="locale.my.order.change.status" var="changeStatus"/>
<fmt:message key="locale.my.order.customer" var="customer"/>
<fmt:message key="locale.my.order.loading" var="loading"/>
<fmt:message key="locale.my.order.unloading" var="unloading"/>
<fmt:message key="locale.my.order.status.ordered" var="ordered"/>
<fmt:message key="locale.my.order.status.done" var="done"/>
<fmt:message key="locale.my.order.status.in.the.way" var="inTheWay"/>
<fmt:message key="locale.my.order.status.cancelled" var="cancelled"/>
<fmt:message key="locale.my.order.status.cancel" var="cancel"/>
<fmt:message key="locale.my.order.status.accepted" var="accepted"/>
<fmt:message key="locale.my.order.empty" var="emptyOrderList"/>


<html>
<head>
    <title>${title}</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/mdb.min.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body class="body">
<main class="main">
    <div class="container">
        <div class="card card-register mx-auto mt-5">
            <div class="card-header">${title}</div>
            <div class="card-body">
                <c:if test="${empty orderList }">
                    <div>${emptyOrderList}</div>
                </c:if>
                <ul class="list-group">
                    <c:forEach var="order" items="${orderList}">
                        <li class="list-group-item">
                            <c:if test="${user.role == 'CLIENT'}">
                                <div class="row">
                                    <div class="col-3">
                                        <div class="view overlay zoom"
                                             style="display:inline-block;">
                                            <img src="data:image/jpg;base64,${order.vehicleDto.photo}" class="img-fluid "
                                                 alt="zoom" width="200" height="150">
                                            <div class="mask flex-center waves-effect waves-light"></div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        &emsp;
                                        <div class="row">
                                            <div class="col">${time}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col">${address}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col">${price}: ${order.cost} BYN</div>
                                        </div>
                                        <div class="row">
                                            <div class="col">${timeOrder}: ${order.orderTime}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col"> ${driver}: ${order.vehicleDto.userDto.contactDto.firstName},
                                                +${order.vehicleDto.userDto.contactDto.phoneNumber}</div>
                                        </div>


                                    </div>
                                    <div class="col-3">
                                            ${loading}
                                        <div class="row">
                                            <div class="col">${order.loadingDate}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col">${order.loadingAddress}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col"> &emsp;</div>
                                        </div>
                                        <div class="row">
                                            <div class="col"> &emsp;</div>
                                        </div>
                                        <div class="row">
                                            <div class="col"> &emsp;</div>
                                        </div>
                                        <div class="row">
                                            <c:choose>
                                                <c:when test="${order.status=='ORDERED'}">
                                                    <div class="col">${status}: ${ordered}</div>
                                                </c:when>
                                                <c:when test="${order.status=='ACCEPTED'}">
                                                    <div class="col">${status}: ${accepted}</div>
                                                </c:when>
                                                <c:when test="${order.status=='IN_THE_WAY'}">
                                                    <div class="col">${status}: ${inTheWay}</div>
                                                </c:when>
                                                <c:when test="${order.status=='CANCELLED'}">
                                                    <div class="col">${status}: ${cancelled}</div>
                                                </c:when>
                                                <c:when test="${order.status=='DONE'}">
                                                    <div class="col">${status}: ${done}</div>
                                                </c:when>
                                            </c:choose>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                            ${unloading}
                                        <div class="row">
                                            <div class="col">${order.unloadingDate}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col">${order.unloadingAddress}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col"> &emsp;</div>
                                        </div>
                                        <div class="row">
                                            <div class="col"> &emsp;</div>
                                        </div>
                                        <c:if test="${order.status == 'ORDERED' || order.status == 'ACCEPTED'}">
                                            <div class="row">
                                                <div class="col">
                                                    <form type="post" action="change-order-status">
                                                        <input type="hidden" value="CANCELLED" name="order.status">
                                                        <input type="hidden" value="${order.id}" name="order.id">
                                                        <button style="text-align:center" class="btn btn-primary btn-sm"
                                                                type="submit">${cancel}</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>
                            </c:if>
                            <c:if test="${user.role == 'DRIVER'}">
                                <div class="row">
                                    <div class="col"></div>
                                    <div class="col">${loading}</div>
                                    <div class="col">${unloading}</div>
                                </div>
                                <div class="row">
                                    <div class="col">${time}</div>
                                    <div class="col">${order.loadingDate}</div>
                                    <div class="col">${order.unloadingDate}</div>
                                </div>
                                <div class="row">
                                    <div class="col">${address}</div>
                                    <div class="col">${order.loadingAddress}</div>
                                    <div class="col">${order.unloadingAddress}</div>
                                </div>
                                <div class="row">
                                    <div class="col">${price}: ${order.cost} BYN</div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                            ${customer}: ${order.userDto.contactDto.firstName},
                                        +${order.userDto.contactDto.phoneNumber}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">${timeOrder}: ${order.orderTime}</div>
                                </div>
                                <form type="post" action="change-order-status">
                                    <div class="row">
                                        <c:choose>
                                            <c:when test="${order.status == 'ORDERED'}">
                                                <div class="col">${status}: ${ordered}</div>
                                                <div class="col">
                                                    <select class="browser-default custom-select" name="order.status">
                                                        <option value="ORDERED" selected>${ordered}</option>
                                                        <option value="ACCEPTED">${accepted}</option>
                                                        <option value="IN_THE_WAY" disabled>${inTheWay}</option>
                                                        <option value="DONE" disabled>${done}</option>
                                                        <option value="CANCELLED">${cancelled}</option>
                                                    </select>
                                                </div>
                                            </c:when>
                                            <c:when test="${order.status == 'ACCEPTED'}">
                                                <div class="col">${status}: ${accepted}</div>
                                                <div class="col">
                                                    <select class="browser-default custom-select" name="order.status">
                                                        <option value="ORDERED" disabled>${ordered}</option>
                                                        <option value="ACCEPTED" selected>${accepted}</option>
                                                        <option value="IN_THE_WAY">${inTheWay}</option>
                                                        <option value="DONE" disabled>${done}</option>
                                                        <option value="CANCELLED" disabled>${cancelled}</option>
                                                    </select>
                                                </div>
                                            </c:when>
                                            <c:when test="${order.status == 'IN_THE_WAY'}">
                                                <div class="col">${status}: ${inTheWay}</div>
                                                <div class="col">
                                                    <select class="browser-default custom-select" name="order.status">
                                                        <option value="ORDERED" disabled>${ordered}</option>
                                                        <option value="ACCEPTED" disabled>${accepted}</option>
                                                        <option value="IN_THE_WAY" selected>${inTheWay}</option>
                                                        <option value="DONE">${done}</option>
                                                        <option value="CANCELLED" disabled>${cancelled}</option>
                                                    </select>
                                                </div>
                                            </c:when>
                                            <c:when test="${order.status == 'DONE'}">
                                                <div class="col">${status}: ${done}</div>
                                                <div class="col">
                                                    <select class="browser-default custom-select"
                                                            name="order.status">
                                                        <option value="ORDERED" disabled>${ordered}</option>
                                                        <option value="ACCEPTED" disabled>${accepted}</option>
                                                        <option value="IN_THE_WAY" disabled>${inTheWay}</option>
                                                        <option value="DONE" selected>${done}</option>
                                                        <option value="CANCELLED" disabled>${cancelled}</option>
                                                    </select>
                                                </div>
                                            </c:when>
                                            <c:when test="${order.status == 'CANCELLED'}">
                                                <div class="col">${status}: ${cancelled}</div>
                                                <div class="col">
                                                    <select class="browser-default custom-select"
                                                            name="order.status">
                                                        <option value="ORDERED" disabled>${ordered}</option>
                                                        <option value="ACCEPTED" disabled>${accepted}</option>
                                                        <option value="IN_THE_WAY" disabled>${inTheWay}</option>
                                                        <option value="DONE" disabled>${done}</option>
                                                        <option value="CANCELLED" selected>${cancelled}</option>
                                                    </select>
                                                </div>
                                            </c:when>
                                        </c:choose>
                                        <c:if test="${order.status != 'CANCELLED'}">
                                            <div class="col">
                                                <input type="hidden" value="${order.id}"
                                                       name="order.id">
                                                <button class="btn btn-primary btn-sm"
                                                        type="submit">${changeStatus}</button>
                                            </div>
                                        </c:if>
                                    </div>
                                </form>
                            </c:if>
                        </li>
                    </c:forEach>

                </ul>
            </div>
        </div>
    </div>
</main>
</body>
</html>
