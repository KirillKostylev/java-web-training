<%--
  Created by IntelliJ IDEA.
  User: Kirill Kostylev
  Date: 2/1/2020
  Time: 19:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="../Layout.jsp" %>
<fmt:message key="locale.order" var="title"/>
<fmt:message key="locale.order.loading.date" var="loadingDateMsg"/>
<fmt:message key="locale.order.unloading.date" var="unloadingDateMsg"/>
<fmt:message key="locale.order.unloading.address" var="unloadingAddressMsg"/>
<fmt:message key="locale.order.loading.address" var="loadingAddressMsg"/>
<fmt:message key="locale.select.vehicle.order" var="order"/>
<fmt:message key="locale.order.price" var="totalPrice"/>
<fmt:message key="locale.order.calculate.price" var="calculatePrice"/>
<fmt:message key="locale.order.distance" var="distanceMsg"/>

<html>
<head>
    <title>${title}</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="../../css/mdb.min.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body class="body">
<main class="main">
    <div class="container" style="width: 50%">
        <div class="card card-register mx-auto mt-5">
            <div class="card-header">${title}</div>
            <div class="card-body">
                <form method="post" action="calculate-the-order-cost">
                    <div class="form-group">
                        <label for="loadingAddress">${loadingAddressMsg}</label>
                        <input type="text" class="form-control" name="order.loadingAddress" value="${loadingAddress} "
                               id="loadingAddress" required>
                    </div>
                    <div class="form-group">
                        <label for="unloadingAddress">${unloadingAddressMsg}</label>
                        <input type="text" class="form-control" name="order.unloadingAddress"
                               value="${unloadingAddress}"
                               id="unloadingAddress" required>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="loading">${loadingDateMsg}</label>
                            <input type="datetime-local" class="form-control" id="loading" name="order.loadingDate"
                                   value="${loadingDate}" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="unloading">${unloadingDateMsg}</label>
                            <input type="datetime-local" class="form-control" id="unloading" name="order.unloadingDate"
                                   value="${unloadingDate}" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <span>${distanceMsg}: ${distance} KM</span>
                            <br>
                            <span>${totalPrice}: ${price} BYN</span>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="hidden" name="owner.login" value="${ownerLogin}">
                            <button class="btn btn-primary btn-block" type="submit">${calculatePrice}</button>
                        </div>
                    </div>
                </form>
                <c:if test="${not empty errors}">
                    <c:forEach var="error" items="${errors}">
                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert">${error}</div>
                        </div>
                    </c:forEach>
                </c:if>
                <form method="post" action="save-order">
                    <c:if test="${price == null}">
                        <button class="btn btn-primary btn-block" type="submit" disabled>${order}</button>
                    </c:if>
                    <c:if test="${price != null}">
                        <input type="hidden" value="${price}" name="order.price">
                        <input type="hidden" value="${loadingAddress}" name="order.loadingAddress">
                        <input type="hidden" value="${unloadingAddress}" name="order.unloadingAddress">
                        <input type="hidden" value="${loadingDate}" name="order.loadingDate">
                        <input type="hidden" value="${unloadingDate}" name="order.unloadingDate">
                        <input type="hidden" name="owner.login" value="${ownerLogin}">
                        <button class="btn btn-primary btn-block" type="submit">${order}</button>
                    </c:if>
                </form>
            </div>
        </div>
    </div>

</main>
</body>
</html>
