<%--
  Created by IntelliJ IDEA.
  User: Kirill Kostylev
  Date: 1/24/2020
  Time: 00:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="../Layout.jsp" %>

<fmt:message key="locale.add.vehicle.title" var="title"/>
<fmt:message key="locale.add.vehicle.type" var="type"/>
<fmt:message key="locale.add.vehicle.tariff" var="tariff"/>
<fmt:message key="locale.add.vehicle.description" var="description"/>
<fmt:message key="locale.add.vehicle.text" var="text"/>
<fmt:message key="locale.add.vehicle.tariff" var="tariff"/>
<fmt:message key="locale.add.vehicle.photo" var="photo"/>
<fmt:message key="locale.add.vehicle.car" var="car"/>
<fmt:message key="locale.add.vehicle.truck" var="truck"/>
<fmt:message key="locale.add.vehicle.van" var="van"/>
<fmt:message key="locale.add.vehicle.location" var="location"/>
<fmt:message key="locale.add.vehicle.location.minsk" var="minsk"/>
<fmt:message key="locale.my.vehicle.add" var="add"/>
<html>
<head>
    <title>${title}</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/mdb.min.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body class="body">
<main class="main">
    <div class="container" style="width: 50%;">
        <div class="card card-register mx-auto mt-5">
            <div class="card-header">${title}</div>
            <div class="card-body">
                <form method="post" action="save-vehicle" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="vehicleType">${type}</label>
                            <select class="browser-default custom-select" id="vehicleType" name="vehicle.type">
                                <option value="PASSENGER_CAR" selected="selected">${car}</option>
                                <option value="TRUCK">${truck}</option>
                                <option value="VAN">${van}</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tariff">${tariff}</label>
                            <input type="text" class="form-control" name="vehicle.tariff"
                                   id="tariff" placeholder="0.00" required>
                        </div>
                    </div>
                    <div class="form-group shadow-textarea">
                        <label for="aboutVehicle">${description}</label>
                        <textarea class="form-control z-depth-1" id="aboutVehicle" name="vehicle.description"
                                  rows="3" placeholder="${text}" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="location">${location}</label>
                        <input type="text" class="form-control" name="vehicle.location"
                               id="location" placeholder="${minsk}" required>
                    </div>
                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="photo"
                                   aria-describedby="inputGroupFileAddon01" name="vehicle.photo"
                                   accept="image/jpeg,image/png">
                            <label class="custom-file-label" for="photo">${photo}</label>
                        </div>
                    </div>
                    <c:if test="${not empty errors}">
                        <c:forEach var="error" items="${errors}">
                            <div class="col-md-12">
                                <div class="alert alert-danger" role="alert">${error}</div>
                            </div>
                        </c:forEach>
                    </c:if>
                    <button class="btn btn-primary btn-block" type="submit">${add}</button>
                </form>
            </div>
        </div>
    </div>
</main>

</body>
</html>
