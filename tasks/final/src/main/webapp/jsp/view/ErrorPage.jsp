<%--
  Created by IntelliJ IDEA.
  User: Kirill Kostylev
  Date: 1/23/2020
  Time: 01:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/mdb.min.css">
    <link rel="stylesheet" href="../../css/style.css">
    <title>Error page</title>
</head>
<body class="body">
<br/>
<h1 style="text-align: center; color: white">${requestScope.get("error.code")} error </h1>
<h2 style="text-align: center; color: white">${requestScope.get("error.message")} </h2>
</body>
</html>
