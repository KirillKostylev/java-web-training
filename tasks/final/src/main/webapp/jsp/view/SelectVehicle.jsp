<%--
  Created by IntelliJ IDEA.
  User: Kirill Kostylev
  Date: 1/31/2020
  Time: 15:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="../Layout.jsp" %>

<fmt:message key="locale.layout.select.vehicle" var="title"/>
<fmt:message key="locale.add.vehicle.car" var="car"/>
<fmt:message key="locale.add.vehicle.truck" var="truck"/>
<fmt:message key="locale.add.vehicle.van" var="van"/>
<fmt:message key="locale.add.vehicle.type" var="type"/>
<fmt:message key="locale.add.vehicle.tariff" var="tariff"/>
<fmt:message key="locale.add.vehicle.description" var="description"/>
<fmt:message key="locale.add.vehicle.location" var="location"/>
<fmt:message key="locale.select.vehicle.order" var="order"/>
<fmt:message key="locale.select.vehicle.owner" var="owner"/>
<fmt:message key="locale.select.vehicle.error.access" var="errorAccess"/>

<html>
<head>
    <title>${title}</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/mdb.min.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body class="body">
<main class="main">
    <div class="container">
        <div class="card card-register mx-auto mt-5">
            <div class="card-header">${title}</div>
            <div class="card-body">
                <ul class="list-group">
                    <c:forEach var="vehicle" items="${vehicleList}">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    <div class="view overlay zoom"
                                         style="display:inline-block;">
                                        <img src="data:image/jpg;base64,${vehicle.photo}" width="250" height="220" class="img-fluid "
                                             alt="zoom">
                                        <div class="mask flex-center waves-effect waves-light"></div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <c:choose>
                                        <c:when test="${vehicle.type == 'TRUCK'}">
                                            <h4 style="margin-bottom: 10px">${type}: ${truck}</h4>
                                        </c:when>
                                        <c:when test="${vehicle.type == 'PASSENGER_CAR'}">
                                            <h4 style="margin-bottom: 10px">${type}: ${car}</h4>
                                        </c:when>
                                        <c:otherwise>
                                            <h4 style="margin-bottom: 10px">${type}: ${van}</h4>
                                        </c:otherwise>
                                    </c:choose>
                                    <h4 style="margin-bottom: 10px">${tariff}: ${vehicle.tariff}</h4>
                                    <h4 style="margin-bottom: 10px">${location}: ${vehicle.location}</h4>
                                    <h4 style="margin-bottom: 10px">${description}: ${vehicle.description}</h4>
                                    <h4 style="margin-bottom: 0px">${owner}: ${vehicle.userDto.contactDto.firstName},
                                        +${vehicle.userDto.contactDto.phoneNumber}</h4>
                                </div>
                                <div class="col">
                                    <form method="post" action="view-order-form">
                                        <input type="hidden" name="vehicle.userDto.login"
                                               value="${vehicle.userDto.login}"/>
                                        <c:choose>
                                            <c:when test="${user.role == 'CLIENT'}">
                                                <input type="hidden" name="owner.login"
                                                       value="${vehicle.userDto.login}">
                                                <button style="margin:0;transform: translate(-50%, -50%);position: absolute;
                                                    top:50%;left: 50%;" class="btn btn-primary" type="submit">
                                                        ${order}
                                                </button>
                                            </c:when>
                                            <c:otherwise>
                                                <h5 style=" height: 100px;margin: 50px 0;">${errorAccess}</h5>
                                            </c:otherwise>
                                        </c:choose>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </c:forEach>
                </ul>

            </div>
        </div>
    </div>
</main>

</body>
</html>
