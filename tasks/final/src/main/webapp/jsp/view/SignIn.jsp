<%--
  Created by IntelliJ IDEA.
  User: Kirill Kostylev
  Date: 12/5/2019
  Time: 17:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="../Layout.jsp" %>
<fmt:message key="locale.signin.login" var="login"/>
<fmt:message key="locale.signin.password" var="password"/>
<fmt:message key="locale.signin.title" var="title"/>
<fmt:message key="locale.signin.signin" var="signin"/>
<fmt:message key="locale.signin.error.incorrect" var="errorIncorrect"/>
<html>
<head>
    <title>${title}</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="../../css/mdb.min.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body class="body">
<main class="main">
    <div class="container" style="width: 50%;">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">${title}</div>
            <div class="card-body">
                <form method="post" action="user-sign-in">
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="text" class="form-control" placeholder=${login} required="required"
                                   autofocus="autofocus" name="user.login">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="password" class="form-control" placeholder=${password} required="required"
                                   name="user.password">
                        </div>
                    </div>
                    <c:if test="${not empty errors}">
                        <c:forEach var="error" items="${errors}">
                            <div class="col-md-12">
                                <div class="alert alert-danger" role="alert">${error}</div>
                            </div>
                        </c:forEach>
                    </c:if>
                    <button class="btn btn-primary btn-block" type="submit">${signin}</button>
                </form>
            </div>
        </div>
    </div>
</main>
</body>
</html>
