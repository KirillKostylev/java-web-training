<%--
  Created by IntelliJ IDEA.
  User: Kirill Kostylev
  Date: 11/28/2019
  Time: 00:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@include file="../Layout.jsp" %>

<fmt:message key="locale.signup.title" var="title"/>
<fmt:message key="locale.signup.login" var="login"/>
<fmt:message key="locale.signup.password" var="password"/>
<fmt:message key="locale.signup.password2" var="password2"/>
<fmt:message key="locale.signup.register" var="register"/>
<fmt:message key="locale.signup.email" var="email"/>
<fmt:message key="locale.signup.error.password" var="errorPassword"/>
<fmt:message key="locale.signup.error.user.exist" var="errorUserExist"/>
<fmt:message key="locale.signup.phone" var="phoneNumber"/>
<fmt:message key="locale.signup.first.name" var="firstName"/>
<fmt:message key="locale.signup.last.name" var="lastName"/>
<fmt:message key="locale.signup.driver" var="driver"/>
<fmt:message key="locale.signup.client" var="client"/>

<html>
<head>
    <title>${title}</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="../../css/mdb.min.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body class="body">
<main class="main">
    <div class="container" style="width: 50%;">
        <div class="card card-register mx-auto mt-5">
            <div class="card-header">${title}</div>
            <div class="card-body">
                <form method="post" action="user-sign-up">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputFirstName">${firstName}</label>
                            <input type="text" class="form-control" name="user.firstName"
                                   id="inputFirstName" placeholder="${firstName}" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputLastName">${lastName}</label>
                            <input type="text" class="form-control" name="user.lastName"
                                   id="inputLastName" placeholder="${lastName}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPhone">${phoneNumber}</label>
                        <input type="text" class="form-control" name="user.phoneNumber"
                               id="inputPhone" placeholder="375XXXXXXXXX" required>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail">${email}</label>
                            <input type="email" class="form-control" name="user.email"
                                   id="inputEmail" placeholder="${email}" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputLogin">${login}</label>
                            <input type="text" class="form-control" name="user.login"
                                   id="inputLogin" placeholder="${login}" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword">${password}</label>
                            <input type="password" class="form-control" name="user.password"
                                   id="inputPassword" placeholder="${password}" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPasswordConfirm">${password2}</label>
                            <input type="password" class="form-control" name="user.passwordConfirm"
                                   id="inputPasswordConfirm" placeholder="${password2}" required>
                        </div>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline" style="margin-bottom: 10px">
                        <input type="radio" class="custom-control-input" id="defaultInline2"
                               value="CLIENT" name="user.role" checked>
                        <label class="custom-control-label" for="defaultInline2">${client}</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="defaultInline3"
                               value="DRIVER" name="user.role">
                        <label class="custom-control-label" for="defaultInline3">${driver}</label>
                    </div>
                    <c:if test="${not empty errors}">
                        <c:forEach var="error" items="${errors}">
                            <div class="col-md-12">
                                <div class="alert alert-danger" role="alert">${error}</div>
                            </div>
                        </c:forEach>
                    </c:if>
                    <button class="btn btn-primary btn-block" type="submit">${register}</button>
                </form>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript" src="../../js/jquery.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../../js/mdb.min.js"></script>
<!-- Your custom scripts (optional) -->
<script type="text/javascript"></script>
</body>
</html>
