<%--
  Created by IntelliJ IDEA.
  User: Kirill Kostylev
  Date: 12/6/2019
  Time: 17:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="../Layout.jsp" %>

<fmt:message key="locale.start.hello" var="hello"/>
<fmt:message key="locale.start.guest" var="guest"/>

<html>

<head>
    <title>${title}</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="../../css/mdb.min.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>

<main class="main">
    <div class="container">
        <div class="white-text text-center wow fadeInUp">
            <c:if test="${sessionScope.user == null}">
                <c:out value="${hello}, ${guest}!"/>
            </c:if>
            <c:if test="${sessionScope.user != null}">
                <c:out value="${hello}, ${sessionScope.user.contactDto.lastName}
                ${sessionScope.user.contactDto.firstName} (${sessionScope.user.role})"/>
            </c:if>
        </div>
    </div>
</main>

<script type="text/javascript" src="../../js/jquery.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../../js/mdb.min.js"></script>
<!-- Your custom scripts (optional) -->
<script type="text/javascript"></script>
</body>
</html>
