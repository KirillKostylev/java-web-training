<%--
  Created by IntelliJ IDEA.
  User: Kirill Kostylev
  Date: 1/24/2020
  Time: 00:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="../Layout.jsp" %>

<fmt:message key="locale.my.vehicle.title" var="title"/>
<fmt:message key="locale.my.vehicle.add" var="add"/>
<fmt:message key="locale.my.vehicle.msg" var="msg"/>
<fmt:message key="locale.my.vehicle.change" var="change"/>
<fmt:message key="locale.add.vehicle.type" var="type"/>
<fmt:message key="locale.add.vehicle.tariff" var="tariff"/>
<fmt:message key="locale.add.vehicle.description" var="description"/>
<fmt:message key="locale.add.vehicle.location" var="location"/>
<fmt:message key="locale.user.management.delete" var="delete"/>

<fmt:message key="locale.add.vehicle.car" var="car"/>
<fmt:message key="locale.add.vehicle.truck" var="truck"/>
<fmt:message key="locale.add.vehicle.van" var="van"/>

<html>
<head>
    <title>${title}</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/mdb.min.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body class="body">
<main class="main">
    <c:choose>
        <c:when test="${vehicle == null}">
            <br/>
            <h3 style="text-align: center; color: white">${msg}</h3>
            <div style="display: flex; justify-content: center;">
                <a href="view-add-vehicle">
                    <button type="button" class="btn btn-primary">${add}</button>
                </a>
            </div>
        </c:when>
        <c:when test="${vehicle != null}">
            <div class="container">
                <div class="card card-register mx-auto mt-5">
                    <div class="card-header">${title}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="view overlay zoom"
                                     style="display:inline-block;">
                                        <img src="data:image/jpg;base64,${vehicle.photo}" width="250" height="220"
                                             class="img-fluid " alt="zoom">
                                        <div class="mask flex-center waves-effect waves-light"></div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div>
                                    <c:choose>
                                        <c:when test="${vehicle.type == 'TRUCK'}">
                                            <h3 style="margin-bottom: 10px">${type}: ${truck}</h3>
                                        </c:when>
                                        <c:when test="${vehicle.type == 'PASSENGER_CAR'}">
                                            <h3 style="margin-bottom: 10px">${type}: ${car}</h3>
                                        </c:when>
                                        <c:otherwise>
                                            <h3 style="margin-bottom: 10px">${type}: ${van}</h3>
                                        </c:otherwise>
                                    </c:choose>
                                    <h3 style="margin-bottom: 10px">${tariff}: ${vehicle.tariff}</h3>
                                    <h3 style="margin-bottom: 10px">${location}: ${vehicle.location}</h3>
                                    <h3 style="margin-bottom: 20px">${description}: ${vehicle.description}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 40px">
                            <div class="col"></div>
                            <div class="col">
                                <form method="post" action="delete-vehicle">
                                    <input type="hidden" name="vehicle.id" value="${user.id}">
                                    <button class="btn btn-primary btn-block" type="submit"
                                            style="width: 100%">${delete}</button>
                                </form>
                            </div>
                            <div class="col"></div>
                            <div class="col">
                                <form method="post" action="view-change-vehicle-form">
                                    <button class="btn btn-primary btn-block" type="submit"
                                            style="width: 100%">${change}</button>
                                </form>
                            </div>
                            <div class="col"></div>
                        </div>
                    </div>
                </div>
            </div>
        </c:when>
    </c:choose>
</main>

</body>
</html>
