<%--
  Created by IntelliJ IDEA.
  User: Kirill Kostylev
  Date: 1/16/2020
  Time: 00:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="../Layout.jsp" %>

<fmt:message key="locale.user.management.last.name" var="lastName"/>
<fmt:message key="locale.user.management.first.name" var="firstName"/>
<fmt:message key="locale.user.management.login" var="login"/>
<fmt:message key="locale.user.management.email" var="email"/>
<fmt:message key="locale.user.management.registration.time" var="registerTime"/>
<fmt:message key="locale.user.management.delete" var="delete"/>
<fmt:message key="locale.user.management.role" var="role"/>
<fmt:message key="locale.user.management.phone" var="phone"/>

<html>
<head>
    <title>${title}</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="../../css/mdb.min.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body class="body">
<main class="main">
    <div class="container">
        <div class="card card-register mx-auto mt-5">
            <div class="card-header">${title}</div>
            <div class="card-body">
                <table class="table table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">${firstName}</th>
                        <th scope="col">${lastName}</th>
                        <th scope="col">${login}</th>
                        <th scope="col">${email}</th>
                        <th scope="col">${phone}</th>
                        <th scope="col">${registerTime}</th>
                        <th scope="col">${role}</th>
                        <th scope="col">${delete}</th>
                    </tr>
                    </thead>
                    <c:set var="counter" value="${0}" scope="request"/>
                    <c:forEach var="u" items="${users}">
                        <tbody>
                        <tr>
                            <th>${counter = counter + 1}</th>
                            <td>${u.contactDto.firstName}</td>
                            <td>${u.contactDto.lastName}</td>
                            <td>${u.login}</td>
                            <td>${u.contactDto.email}</td>
                            <td>${u.contactDto.phoneNumber}</td>
                            <td>${u.registrationDate}</td>
                            <td>${u.role}</td>
                            <td>
                                <form method="post" action="delete-user">
                                    <input type="hidden" name="user.login" value="${u.login}"/>
                                    <button class="btn btn-primary btn-sm table-user-mngmt"
                                            type="submit">${delete}</button>
                                </form>
                            </td>
                        </tr>
                        </tbody>

                    </c:forEach>

                </table>
            </div>
        </div>
    </div>
</main>
</body>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<!-- Your custom scripts (optional) -->
<script type="text/javascript"></script>
</html>