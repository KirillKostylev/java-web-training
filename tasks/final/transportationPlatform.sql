CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone_number` varchar(20) NOT NULL,
  `photo` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=206 DEFAULT CHARSET=utf8;

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_time` timestamp NOT NULL,
  `loading_date` timestamp NOT NULL,
  `unloading_date` timestamp NOT NULL,
  `loading_address` varchar(100) NOT NULL,
  `unloading_address` varchar(100) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `cost` double NOT NULL,
  `order_status` enum('ordered','accepted','done','in_the_way','cancelled') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_vehicle1_idx` (`vehicle_id`),
  KEY `fk_reservation_user_account1_idx` (`customer_id`),
  CONSTRAINT `fk_order_vehicle1` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicle` (`vehicle_id`),
  CONSTRAINT `fk_reservation_user_account1` FOREIGN KEY (`customer_id`) REFERENCES `user_account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

CREATE TABLE `user_account` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_role` enum('admin','client','driver') NOT NULL,
  `login` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `registration_date` timestamp NOT NULL,
  `contact_id` int(11) NOT NULL,
  `wallet_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  KEY `fk_user_account_contact1_idx` (`contact_id`),
  KEY `fk_user_account_wallet1_idx` (`wallet_id`),
  CONSTRAINT `fk_user_account_contact1` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`),
  CONSTRAINT `fk_user_account_wallet1` FOREIGN KEY (`wallet_id`) REFERENCES `wallet` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8;

CREATE TABLE `vehicle` (
  `vehicle_id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(45) NOT NULL,
  `tariff` decimal(10,2) NOT NULL,
  `owner_id` int(10) NOT NULL,
  `vehicle_type` enum('passenger_car','truck','van') NOT NULL,
  `photo` blob NOT NULL,
  `description` varchar(1000) NOT NULL,
  PRIMARY KEY (`vehicle_id`),
  UNIQUE KEY `bus_id_UNIQUE` (`vehicle_id`),
  KEY `fk_vehicle_user_account1_idx` (`owner_id`),
  CONSTRAINT `fk_vehicle_user_account1` FOREIGN KEY (`owner_id`) REFERENCES `user_account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8;

CREATE TABLE `wallet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `count` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=206 DEFAULT CHARSET=utf8;