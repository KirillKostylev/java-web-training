package by.epam.training.validator;

import java.util.*;

public class ValidatorResult {
    private Map<ErrorCode, List<String>> errors;

    public ValidatorResult() {
        this.errors = new HashMap<>();
    }

    public void addError(ErrorCode code, String str) {
        if (errors.containsKey(code)) {
            errors.get(code).add(str);
        } else {
            List<String> errorMessages = new ArrayList<>();
            errorMessages.add(str);
            errors.put(code, errorMessages);
        }
    }

    public boolean isValid() {
        return errors.isEmpty();
    }

    @Override
    public String toString() {
        return "ValidatorResult{" +
                "errors=" + errors +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidatorResult that = (ValidatorResult) o;
        return Objects.equals(errors, that.errors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(errors);
    }
}
