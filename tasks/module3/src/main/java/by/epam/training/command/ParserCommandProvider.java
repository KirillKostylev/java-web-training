package by.epam.training.command;

import by.epam.training.entity.PostCard;

import java.util.HashMap;
import java.util.Map;

public class ParserCommandProvider implements CommandProvider<PostCard> {
    private Map<CommandType, Command<PostCard>> parsers;

    public ParserCommandProvider() {
        this.parsers = new HashMap<>();
    }

    @Override
    public void addCommand(CommandType type, Command<PostCard> command) {
        parsers.put(type, command);
    }

    @Override
    public Command<PostCard> getCommand(CommandType type) {
        return parsers.get(type);
    }
}
