package by.epam.training.validator;

public enum  ErrorCode {
    VALIDATION_ERROR, FILE_NOT_FOUND, EMPTY_FILE
}
