package by.epam.training.entity;

import java.util.List;
import java.util.Objects;

public class AdvertisingCard extends PostCard {
    private String companyName;


    public AdvertisingCard(long id, String theme, boolean isPosted, String country, int year, List<String> authors,
                           Valuable valuable, String companyName) {
        super(id, theme, isPosted, country, year, authors, valuable);
        this.companyName = companyName;
    }

    public AdvertisingCard() {
    }


    public AdvertisingCard(long id, String theme, boolean isPosted, String country, int year, List<String> authors,
                           String companyName) {
        this(id, theme, isPosted, country, year, authors, Valuable.DEFAULT_VALUE, companyName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AdvertisingCard that = (AdvertisingCard) o;
        return Objects.equals(companyName, that.companyName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), companyName);
    }

    @Override
    public String toString() {
        return "AdvertisingCard{" +
                "companyName='" + companyName + '\'' + super.toString() +
                "}";
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
