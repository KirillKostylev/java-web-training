package by.epam.training.entity;

import java.util.List;
import java.util.Objects;

public class PostCard {
    private long id;
    private String theme;
    private boolean isPosted;
    private String country;
    private int year;
    private List<String> authors;
    private Valuable valuable = Valuable.THEMATIC;

    public PostCard(long id, String theme, boolean isPosted, String country, int year, List<String> authors,
                    Valuable valuable) {
        this.id = id;
        this.theme = theme;
        this.isPosted = isPosted;
        this.country = country;
        this.year = year;
        this.authors = authors;
        this.valuable = valuable;
    }

    public PostCard() {

    }


    public PostCard(long id, String theme, boolean isPosted, String country, int year, List<String> authors) {
        this(id, theme, isPosted, country, year, authors, Valuable.DEFAULT_VALUE);
    }

    @Override
    public String toString() {
        return "PostCard{" +
                "id=" + id +
                ", theme='" + theme + '\'' +
                ", isPosted=" + isPosted +
                ", country='" + country + '\'' +
                ", year=" + year +
                ", authors=" + authors +
                ", valuable=" + valuable +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostCard postCard = (PostCard) o;
        return id == postCard.id &&
                isPosted == postCard.isPosted &&
                year == postCard.year &&
                Objects.equals(theme, postCard.theme) &&
                Objects.equals(country, postCard.country) &&
                Objects.equals(authors, postCard.authors) &&
                valuable == postCard.valuable;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, theme, isPosted, country, year, authors, valuable);
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setPosted(boolean posted) {
        isPosted = posted;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public void setValuable(Valuable valuable) {
        this.valuable = valuable;
    }
}
