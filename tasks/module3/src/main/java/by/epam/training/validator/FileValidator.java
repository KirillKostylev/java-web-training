package by.epam.training.validator;

import org.apache.log4j.Logger;

import java.io.File;
import java.text.MessageFormat;

public class FileValidator {
    private static final Logger LOGGER = Logger.getLogger(FileValidator.class);

    public ValidatorResult validate(String path) {
        LOGGER.info("Starting the file validation: " + path);
        ValidatorResult validatorResult = new ValidatorResult();
        File file = new File(path);
        if (!file.exists()) {
            validatorResult.addError(ErrorCode.FILE_NOT_FOUND, "File: " + path + " doesn't exist.");
            return validatorResult;
        }
        if (file.length() == 0) {
            validatorResult.addError(ErrorCode.EMPTY_FILE, "File: " + path + " is empty.");
            return validatorResult;
        }
        LOGGER.info(MessageFormat.format("File validation: {0} was finished successful", path));
        return validatorResult;
    }
}
