package by.epam.training.repository;

import by.epam.training.entity.PostCard;

import java.util.ArrayList;
import java.util.List;

public class PostCardRepository implements Repository<PostCard> {
    private List<PostCard> cards;

    public PostCardRepository() {
        this.cards = new ArrayList<>();
    }

    @Override
    public void add(PostCard postCard) {
        cards.add(postCard);
    }

    @Override
    public List<PostCard> getAll() {
        return cards;
    }


    public void addAll(List<PostCard> postCardList) {
        cards.addAll(postCardList);
    }
}
