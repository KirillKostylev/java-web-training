package by.epam.training.repository;

import java.util.List;

public interface Repository<T> {
    void add(T postCard);
    List<T> getAll();
}
