package by.epam.training.service;

import java.util.List;

public interface Service<T> {
    void save(T t);
    List<T> getAll();
}
