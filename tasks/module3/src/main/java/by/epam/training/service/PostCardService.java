package by.epam.training.service;

import by.epam.training.entity.PostCard;
import by.epam.training.repository.Repository;

import java.util.List;

public class PostCardService implements Service<PostCard> {
    private Repository<PostCard> postCardRepository;

    public PostCardService(Repository<PostCard> postCardRepository) {
        this.postCardRepository = postCardRepository;
    }

    @Override
    public void save(PostCard postCard) {
        postCardRepository.add(postCard);
    }

    @Override
    public List<PostCard> getAll() {
        return postCardRepository.getAll();
    }
}

