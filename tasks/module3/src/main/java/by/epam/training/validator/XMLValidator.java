package by.epam.training.validator;

import org.apache.log4j.Logger;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.text.MessageFormat;
import java.util.Objects;

public class XMLValidator {
    private static final Logger LOGGER = Logger.getLogger(XMLValidator.class);
    private String XSDPath;

    public XMLValidator(String XSDPath) {
        this.XSDPath = XSDPath;
    }

    public ValidatorResult validateByXSD(String XMLPath) {
        LOGGER.info(MessageFormat.format("Starting xml file :{0} validation by xsd :{1}", XMLPath, XSDPath));
        ValidatorResult validatorResult = new ValidatorResult();
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory factory = SchemaFactory.newInstance(language);
        File schemaLocation = new File(XSDPath);
        try {
            Schema schema = factory.newSchema(new StreamSource(XSDPath));
            Validator validator = schema.newValidator();
            Source source = new StreamSource(XMLPath);
            validator.validate(source);
        } catch (Exception e) {
            String msg = MessageFormat.format("Validation {0} is not valid because {1}",
                    XMLPath, e.getMessage());
            validatorResult.addError(ErrorCode.VALIDATION_ERROR, msg);
        }
        return validatorResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XMLValidator that = (XMLValidator) o;
        return Objects.equals(XSDPath, that.XSDPath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(XSDPath);
    }
}
