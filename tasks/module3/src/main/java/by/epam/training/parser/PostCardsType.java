package by.epam.training.parser;

public enum PostCardsType {
    POST_CARD("postCard"),
    ADVERTISING_CARD("advertisingCard"),
    CONGRATULATION_CARD("congratulationCard");
    private String value;

    PostCardsType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
