package by.epam.training.parser;

public class ParserException extends Exception {
    public ParserException(String msg) {
        super(msg);
    }

}
