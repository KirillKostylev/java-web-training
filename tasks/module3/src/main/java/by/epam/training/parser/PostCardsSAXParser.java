package by.epam.training.parser;

import by.epam.training.entity.PostCard;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.List;

public class PostCardsSAXParser implements Parser<PostCard> {
    private static final Logger LOGGER = Logger.getLogger(PostCardsSAXParser.class);
    private PostCardsSAXHandler handler;
    private XMLReader reader;

    public PostCardsSAXParser(){
        handler = new PostCardsSAXHandler();
        try {
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(handler);
        } catch (SAXException e) {
            String msg = "SAX parser error: " + e.getMessage();
            LOGGER.error(msg);
        }

    }

    @Override
    public List<PostCard> parse(String path) throws SAXParserException {
        try {
            reader.parse(path);
        } catch (IOException | SAXException e) {
            String msg = "SAX parser error: " + e.getMessage();
            LOGGER.error(msg);
            throw new SAXParserException(msg);
        }
        return handler.getPostCards();
    }
}