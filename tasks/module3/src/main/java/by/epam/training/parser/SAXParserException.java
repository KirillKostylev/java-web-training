package by.epam.training.parser;

public class SAXParserException extends ParserException {
    public SAXParserException(String msg) {
        super(msg);
    }
}
