package by.epam.training.controller;

import by.epam.training.command.*;
import by.epam.training.entity.PostCard;
import by.epam.training.service.PostCardService;
import by.epam.training.validator.FileValidator;
import by.epam.training.validator.ValidatorResult;
import by.epam.training.validator.XMLValidator;
import org.apache.log4j.Logger;

import java.util.List;

public class PostCardsController {
    private static final Logger LOGGER = Logger.getLogger(PostCardsController.class);
    private FileValidator fileValidator;
    private XMLValidator xmlValidator;
    private ParserCommandProvider parserCommandProvider;
    private PostCardService postCardService;

    public PostCardsController(FileValidator fileValidator, XMLValidator xmlValidator,
                               ParserCommandProvider parserCommandProvider, PostCardService postCardService) {
        this.fileValidator = fileValidator;
        this.xmlValidator = xmlValidator;
        this.parserCommandProvider = parserCommandProvider;
        this.postCardService = postCardService;
    }

    public void run(String xmlFileName, String xsdFileName, CommandType type) {
        ValidatorResult xmlValidatorResult = fileValidator.validate(xmlFileName);
        if (!xmlValidatorResult.isValid()) {
            LOGGER.error("XML " + xmlValidatorResult);
            return;
        }

        ValidatorResult xsdValidatorResult = fileValidator.validate(xsdFileName);
        if (!xsdValidatorResult.isValid()) {
            LOGGER.error("XSD " + xsdValidatorResult);
            return;
        }

        xmlValidatorResult = xmlValidator.validateByXSD(xmlFileName);
        if (!xmlValidatorResult.isValid()) {
            LOGGER.error(xmlValidatorResult);
            return;
        }

        DOMCommand domCommand = new DOMCommand();
        SAXCommand saxCommand = new SAXCommand();
        parserCommandProvider.addCommand(CommandType.DOM, domCommand);
        parserCommandProvider.addCommand(CommandType.SAX, saxCommand);
        Command<PostCard> command = parserCommandProvider.getCommand(type);
        try {
            List<PostCard> postCards = command.executeParse(xmlFileName);
            for (PostCard card: postCards) {
                postCardService.save(card);
            }
        } catch (CommandException e) {
            LOGGER.error(e.getMessage());
        }
    }
}

