package by.epam.training.entity;

public enum Valuable {

    HISTORICAL("historical"),
    COLLECTIBLE("collectible"),
    THEMATIC("thematic");

    public static Valuable DEFAULT_VALUE = Valuable.THEMATIC;
    private String value;

    Valuable(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;

    }
}
