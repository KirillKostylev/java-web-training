package by.epam.training.parser;

import by.epam.training.entity.AdvertisingCard;
import by.epam.training.entity.CongratulationCard;
import by.epam.training.entity.PostCard;
import by.epam.training.entity.Valuable;
import org.apache.log4j.Logger;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PostCardsDOMParser implements Parser<PostCard> {
    private static final Logger LOGGER = Logger.getLogger(PostCardsDOMParser.class);
    private List<PostCard> postCards;
    private DocumentBuilder docBuilder;

    public PostCardsDOMParser() {
        this.postCards = new ArrayList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOGGER.error(e.getStackTrace());
        }
    }

    public List<PostCard> parse(String fileName) throws DOMParserException {
        Document doc;
        try {
            LOGGER.info("Starting the DOM parser.");
            doc = docBuilder.parse(fileName);
            collectPostCards(PostCardsType.POST_CARD, doc);
            collectPostCards(PostCardsType.ADVERTISING_CARD, doc);
            collectPostCards(PostCardsType.CONGRATULATION_CARD, doc);
            LOGGER.info("The DOM parser was finished.");
        } catch (SAXException e) {
            String msg = "Parsing failure: " + e.getMessage();
            LOGGER.error(msg);
            throw new DOMParserException(msg);
        } catch (IOException e) {
            String msg = "IO error: " + e.getMessage();
            LOGGER.error(msg);
            throw new DOMParserException(msg);
        }
        return postCards;
    }

    private void collectPostCards(PostCardsType type, Document doc) throws DOMParserException {
        NodeList postCardsList = doc.getElementsByTagName(type.getValue());
        for (int i = 0; i < postCardsList.getLength(); i++) {
            Element currentElement = (Element) postCardsList.item(i);
            NamedNodeMap currentAttributes = currentElement.getAttributes();
            //Fields of PostCard
            long id = Long.parseLong(currentAttributes.getNamedItem("id").getNodeValue());

            boolean isPosted = Boolean.parseBoolean(currentAttributes.getNamedItem("isPosted").getNodeValue());

            Valuable valuable;
            if (currentElement.hasAttribute("valuable")) {
                valuable =
                        Valuable.valueOf(currentAttributes.getNamedItem("valuable").getNodeValue().toUpperCase());
            } else {
                valuable = Valuable.DEFAULT_VALUE;
            }

            String theme = getElementTextContent(currentElement, "theme");

            String country = getElementTextContent(currentElement, "country");

            int year = Integer.parseInt(getElementTextContent(currentElement, "year"));

            List<String> authors = getElementListTextContent(currentElement, "author");

            switch (type) {
                case POST_CARD:
                    PostCard postCard = new PostCard(id, theme, isPosted, country, year, authors, valuable);
                    postCards.add(postCard);
                    LOGGER.info(MessageFormat.format("Object: {0} was added.", postCard));
                    break;
                case ADVERTISING_CARD:
                    String companyName = currentAttributes.getNamedItem("companyName").getNodeValue().trim();
                    AdvertisingCard advertisingCard =
                            new AdvertisingCard(id, theme, isPosted, country, year, authors, valuable, companyName);
                    postCards.add(advertisingCard);
                    LOGGER.info(MessageFormat.format("Object: {0} was added.", advertisingCard));
                    break;
                case CONGRATULATION_CARD:
                    String holidayName = currentAttributes.getNamedItem("holidayName").getNodeValue().trim();
                    CongratulationCard congratulationCard =
                            new CongratulationCard(id, theme, isPosted, country, year, authors, valuable, holidayName);
                    postCards.add(congratulationCard);
                    LOGGER.info(MessageFormat.format("Object: {0} was added.", congratulationCard));
                    break;
                default:
                    String msg = "Unknown type: " + type;
                    LOGGER.error(msg);
                    throw new DOMParserException(msg);
            }
        }
    }


    private static String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        return node.getTextContent().trim();
    }

    private static List<String> getElementListTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        List<String> listContents = new ArrayList<>();
        for (int i = 0; i < nList.getLength(); i++) {
            if (!nList.item(i).hasChildNodes()) {
                return listContents;
            }
            String temp = nList.item(i).getTextContent().trim();
            listContents.add(temp);
        }
        return listContents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostCardsDOMParser that = (PostCardsDOMParser) o;
        return Objects.equals(postCards, that.postCards) &&
                Objects.equals(docBuilder, that.docBuilder);
    }

    @Override
    public int hashCode() {
        return Objects.hash(postCards, docBuilder);
    }

    @Override
    public String toString() {
        return "PostCardsDOMParser{" +
                "postCards=" + postCards +
                '}';
    }
}
