package by.epam.training.parser;

public class DOMParserException extends ParserException {
    public DOMParserException(String msg) {
        super(msg);
    }
}
