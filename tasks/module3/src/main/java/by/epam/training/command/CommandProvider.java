package by.epam.training.command;

public interface CommandProvider<T> {
    void addCommand(CommandType type, Command<T> command);

    Command<T> getCommand(CommandType type);
}
