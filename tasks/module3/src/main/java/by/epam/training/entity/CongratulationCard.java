package by.epam.training.entity;

import java.util.List;
import java.util.Objects;

public class CongratulationCard extends PostCard {
    private String holidayName;

    public CongratulationCard(long id, String theme, boolean isPosted, String country, int year, List<String> authors,
                              Valuable valuable, String holidayName) {
        super(id, theme, isPosted, country, year, authors, valuable);
        this.holidayName = holidayName;
    }


    public CongratulationCard() {
    }

    public CongratulationCard(long id, String theme, boolean isPosted, String country, int year, List<String> authors,
                              String holidayName) {
        this(id, theme, isPosted, country, year, authors, Valuable.DEFAULT_VALUE, holidayName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CongratulationCard that = (CongratulationCard) o;
        return Objects.equals(holidayName, that.holidayName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), holidayName);
    }

    @Override
    public String toString() {
        return "CongratulationCard{" +
                "holidayName='" + holidayName + '\'' + super.toString() +
                "}";
    }

    public void setHolidayName(String holidayName) {
        this.holidayName = holidayName;
    }
}
