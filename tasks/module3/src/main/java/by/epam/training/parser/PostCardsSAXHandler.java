package by.epam.training.parser;

import by.epam.training.entity.AdvertisingCard;
import by.epam.training.entity.CongratulationCard;
import by.epam.training.entity.PostCard;
import by.epam.training.entity.Valuable;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class PostCardsSAXHandler extends DefaultHandler {
    private static final String PATH_TO_POST_CARD = "by.epam.training.entity.";
    private static final Logger LOGGER = Logger.getLogger(PostCardsSAXHandler.class);

    private List<PostCard> postCards;
    private String currentString;
    private PostCard current;
    private List<String> currentAuthors;


    public PostCardsSAXHandler() {
        this.postCards = new ArrayList<>();

    }

    @Override
    public void startDocument() throws SAXException {
        LOGGER.info("Starting the SAX parser.");
    }

    @Override
    public void endDocument() throws SAXException {
        LOGGER.info("The SAX parsing was finished");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        currentString = qName;

        if (currentString == null) {
            return;
        }

        switch (qName) {
            case "postCard":
                current = new PostCard();
                parseBaseAttributePostCard(current, qName, attributes);
                break;
            case "advertisingCard":
                current = new AdvertisingCard();
                parseBaseAttributePostCard(current, qName, attributes);

                String companyName = attributes.getValue("companyName");
                ((AdvertisingCard) current).setCompanyName(companyName);
                break;
            case "congratulationCard":
                current = new CongratulationCard();
                parseBaseAttributePostCard(current, qName, attributes);

                String holidayName = attributes.getValue("holidayName");
                ((CongratulationCard) current).setHolidayName(holidayName);
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String s = new String(ch, start, length).trim();
        if (currentString != null && !s.equals("")) {
            switch (currentString) {
                case "theme":
                    current.setTheme(s);
                    break;
                case "country":
                    current.setCountry(s);
                    break;
                case "year":
                    int year = Integer.parseInt(s);
                    current.setYear(year);
                    break;
                case "author":
                    currentAuthors.add(s);
                    break;
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (qName.equals(PostCardsType.POST_CARD.getValue()) ||
                qName.equals(PostCardsType.CONGRATULATION_CARD.getValue()) ||
                qName.equals(PostCardsType.ADVERTISING_CARD.getValue())) {
            postCards.add(current);
            LOGGER.info(MessageFormat.format("Object: {0} was added.", current));
        }
        if (qName.equals("authors")) {
            current.setAuthors(currentAuthors);
        }
    }


    private void parseBaseAttributePostCard(PostCard pCard, String qName, Attributes attributes) {
        currentAuthors = new ArrayList<>();
        String stringId = attributes.getValue("id");
        long longId = Long.parseLong(stringId);
        pCard.setId(longId);

        String stringIsValid = attributes.getValue("isPosted");
        boolean isValid = Boolean.parseBoolean(stringIsValid);
        pCard.setPosted(isValid);

        if (attributes.getIndex("valuable") != -1) {
            String stringValuable = attributes.getValue("valuable").trim().toUpperCase();
            Valuable valuable = Valuable.valueOf(stringValuable);
            pCard.setValuable(valuable);
        }

    }

    public List<PostCard> getPostCards() {
        return postCards;
    }
}
