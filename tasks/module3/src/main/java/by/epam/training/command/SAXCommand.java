package by.epam.training.command;

import by.epam.training.entity.PostCard;
import by.epam.training.parser.Parser;
import by.epam.training.parser.ParserException;
import by.epam.training.parser.PostCardsSAXParser;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Objects;

public class SAXCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(SAXCommand.class);
    private Parser<PostCard> parser;

   public SAXCommand() {
        parser = new PostCardsSAXParser();
    }

    @Override
    public List<PostCard> executeParse(String fileName) throws CommandException {
        try {
            return parser.parse(fileName);
        } catch (ParserException e) {
            LOGGER.error(e.getStackTrace());
            throw new CommandException("Parser failed because: " + e.getMessage());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SAXCommand that = (SAXCommand) o;
        return Objects.equals(parser, that.parser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parser);
    }

    public Parser<PostCard> getParser() {
        return parser;
    }
}
