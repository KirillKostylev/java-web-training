package by.epam.training.validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

@RunWith(JUnit4.class)
public class XMLValidatorTest {
    private XMLValidator xmlValidator;

    @Before
    public void init() {
        ClassLoader classLoader = getClass().getClassLoader();
        File testXSDFile = new File(classLoader.getResource("postCards.xsd").getFile());
        String XSDPath = testXSDFile.getAbsolutePath();
        xmlValidator = new XMLValidator(XSDPath);
    }

    @Test
    public void validateByXSD() {
        //Actual
        ClassLoader classLoader = getClass().getClassLoader();
        File testXMLFile = new File(classLoader.getResource("parseTest.xml").getFile());
        String xmlPath = testXMLFile.getAbsolutePath();

        ValidatorResult actualVR = xmlValidator.validateByXSD(xmlPath);

        //Assert
        Assert.assertTrue(actualVR.isValid());
    }

    @Test
    public void ErrorValidateByXSD() {
        //Actual
        ClassLoader classLoader = getClass().getClassLoader();
        File testXMLFile = new File(classLoader.getResource("wrongPostCards.xml").getFile());
        String xmlPath = testXMLFile.getAbsolutePath();

        ValidatorResult actualVR = xmlValidator.validateByXSD(xmlPath);

        //Assert
        Assert.assertFalse(actualVR.isValid());
    }
}