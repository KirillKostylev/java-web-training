package by.epam.training.parser;

import by.epam.training.entity.AdvertisingCard;
import by.epam.training.entity.CongratulationCard;
import by.epam.training.entity.PostCard;
import by.epam.training.entity.Valuable;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class PostCardsSAXParserTest {

    @Test
    public void parse() throws SAXParserException {
        //prepare
        ClassLoader classLoader = getClass().getClassLoader();
        File testXMLFile = new File(classLoader.getResource("parseTest.xml").getFile());

        String xmlPath = testXMLFile.getAbsolutePath();

        PostCardsSAXParser postCardsSAXParser = new PostCardsSAXParser();

        //expected

        List<PostCard> expectedPostCards = Arrays.asList(
                new PostCard(1, "Urban landscape", true, "Belarus", 2001,
                        Arrays.asList("Alexey Nekrasov", "Pavel Krasnov"), Valuable.HISTORICAL),
                new AdvertisingCard(2, "Sport", false, "Russia", 2019,
                        Arrays.asList("Alexandr Sergeev"), "Nike"),
                new CongratulationCard(3, "People", true, "USA", 2017,
                        Arrays.asList(), Valuable.COLLECTIBLE, "Birthday")

        );
        //actual
        List<PostCard> actualPostCards = postCardsSAXParser.parse(xmlPath);


        //assert
        Assert.assertEquals(expectedPostCards, actualPostCards);
    }
}
