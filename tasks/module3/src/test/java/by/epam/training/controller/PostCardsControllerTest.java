package by.epam.training.controller;

import by.epam.training.command.CommandType;
import by.epam.training.command.ParserCommandProvider;
import by.epam.training.entity.AdvertisingCard;
import by.epam.training.entity.CongratulationCard;
import by.epam.training.entity.PostCard;
import by.epam.training.entity.Valuable;
import by.epam.training.repository.PostCardRepository;
import by.epam.training.service.PostCardService;
import by.epam.training.validator.FileValidator;
import by.epam.training.validator.XMLValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class PostCardsControllerTest {
    private String xsdPath;
    private String xmlPath;
    private List<PostCard> expectedPostCardList;

    @Before
    public void init() {
        ClassLoader classLoader = getClass().getClassLoader();

        File testXSDFile = new File(classLoader.getResource("postCards.xsd").getFile());
        xsdPath = testXSDFile.getAbsolutePath();

        ClassLoader classLoader2 = getClass().getClassLoader();
        File testXMLFile = new File(classLoader2.getResource("parseTest.xml").getFile());
        xmlPath = testXMLFile.getAbsolutePath();

        expectedPostCardList = Arrays.asList(
                new PostCard(1, "Urban landscape", true, "Belarus", 2001,
                        Arrays.asList("Alexey Nekrasov", "Pavel Krasnov"), Valuable.HISTORICAL),
                new AdvertisingCard(2, "Sport", false, "Russia", 2019,
                        Arrays.asList("Alexandr Sergeev"), "Nike"),
                new CongratulationCard(3, "People", true, "USA", 2017,
                        Arrays.asList(), Valuable.COLLECTIBLE, "Birthday"));
    }

    @Test
    public void runWithDOM() {
        //Actual
        XMLValidator xmlValidator = new XMLValidator(xsdPath);

        PostCardService service = new PostCardService(new PostCardRepository());

        PostCardsController controller = new PostCardsController(new FileValidator(), xmlValidator,
                new ParserCommandProvider(), service);
        controller.run(xmlPath, xsdPath, CommandType.DOM);

        //Assert
        Assert.assertEquals(expectedPostCardList, service.getAll());
    }

    @Test
    public void runWithSAX() {
        //Actual
        XMLValidator xmlValidator = new XMLValidator(xsdPath);

        PostCardService service = new PostCardService(new PostCardRepository());

        PostCardsController controller = new PostCardsController(new FileValidator(), xmlValidator,
                new ParserCommandProvider(), service);
        controller.run(xmlPath, xsdPath, CommandType.SAX);

        //Assert
        Assert.assertEquals(expectedPostCardList, service.getAll());
    }

}