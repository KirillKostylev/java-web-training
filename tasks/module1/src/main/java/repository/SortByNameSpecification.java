package repository;

import entity.Worker;
import org.apache.log4j.Logger;

import java.util.Comparator;

public class SortByNameSpecification implements SortSpecification {
    private final static Logger LOGGER = Logger.getLogger(SortByNameSpecification.class);
    @Override
    public Comparator<Worker> getSuitableComparator() {
        LOGGER.info("Comparator was created");
        return Comparator.comparing(Worker::getFullName);
    }
}
