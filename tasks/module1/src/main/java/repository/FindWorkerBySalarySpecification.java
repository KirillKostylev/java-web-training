package repository;

import entity.Worker;
import repository.FindSpecification;

public class FindWorkerBySalarySpecification implements FindSpecification {

    private int minSalary;
    private int maxSalary;

    public FindWorkerBySalarySpecification(int minSalary, int maxSalary) {
        this.minSalary = minSalary;
        this.maxSalary = maxSalary;
    }

    @Override
    public boolean find(Worker worker) {
        boolean answer = false;
        if (worker != null) {
            answer = worker.getSalary() >= minSalary && worker.getSalary() <= maxSalary;
        }
        return answer;
    }
}
