package repository;

import entity.Worker;
import org.apache.log4j.Logger;

import java.util.Comparator;

public class SortByNameAndBySalarySpecification implements SortSpecification {
    private final static Logger LOGGER = Logger.getLogger(SortByNameAndBySalarySpecification.class);
    @Override
    public Comparator<Worker> getSuitableComparator() {
        LOGGER.info("Comparator was created");
        return Comparator.comparing(Worker::getFullName).thenComparing(Worker::getSalary);
    }
}
