package repository;

import entity.Worker;
import org.apache.log4j.Logger;
import service.WorkerNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class WorkerRepository implements Repository {
    private final static Logger LOGGER = Logger.getLogger(WorkerRepository.class);
    private List<Worker> workers;

    public WorkerRepository() {
        this.workers = new ArrayList<>();
    }

    public WorkerRepository(List<Worker> workers) {
        if (workers != null) {
            this.workers = workers;
        }
    }

    @Override
    public void add(Worker... listWorkers) {
        if (listWorkers != null) {
            for (Worker worker : listWorkers) {
                if (worker != null) {
                    workers.add(worker);
                }
            }
        }
    }


    @Override
    public void remove(Worker worker) throws WorkerNotFoundException {
        if (worker != null) {
            if (workers.contains(worker)) {
                workers.remove(worker);
                LOGGER.info("Worker was removed");
            } else throw new WorkerNotFoundException("The worker is not on the WorkerRepository");
        }

    }

    @Override
    public List<Worker> query(Specification spec) throws SpecificationException {
        List<Worker> result = new ArrayList<>();
        if (spec != null) {
            if (spec instanceof FindSpecification) {
                for (Worker worker : workers) {
                    if (((FindSpecification) spec).find(worker)) {
                        result.add(worker);
                        LOGGER.info("Suitable worker was found");
                    }
                }
            } else if (spec instanceof SortSpecification) {
                result.addAll(workers);
                result.sort(((SortSpecification) spec).getSuitableComparator());
                LOGGER.info("Sort finished successfully");
            } else throw new SpecificationException("Wrong specification");
        }
        return result;
    }

    @Override

    public List<Worker> getAll() {
        return workers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorkerRepository that = (WorkerRepository) o;
        return Objects.equals(workers, that.workers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(workers);
    }
}
