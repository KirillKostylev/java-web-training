package repository;

import entity.Worker;
import service.WorkerNotFoundException;

import java.util.List;

public interface Repository {
    void add(Worker ...listWorker);
    void remove(Worker worker) throws WorkerNotFoundException;
    List<Worker> query(Specification spec) throws SpecificationException;
    List<Worker> getAll();

}
