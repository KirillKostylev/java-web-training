package repository;

import entity.Worker;

import java.util.Comparator;

public interface SortSpecification extends Specification {
    Comparator<Worker> getSuitableComparator();
}
