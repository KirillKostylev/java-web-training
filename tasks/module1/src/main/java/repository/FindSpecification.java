package repository;

import entity.Worker;

public interface FindSpecification<T> extends Specification {
    boolean find(Worker worker);
}
