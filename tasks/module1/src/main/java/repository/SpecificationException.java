package repository;

public class SpecificationException extends Exception {
    public SpecificationException(String msg) {
        super(msg);
    }
}
