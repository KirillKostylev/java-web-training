package service;

public class WorkerNotFoundException extends Exception {
    public WorkerNotFoundException(String msg) {
        super(msg);
    }
}
