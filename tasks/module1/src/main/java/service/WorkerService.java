package service;

import entity.Worker;
import org.apache.log4j.Logger;
import repository.WorkerRepository;

public class WorkerService {
    private final static Logger LOGGER = Logger.getLogger(WorkerService.class);
    private WorkerRepository workerRepository;

    public WorkerService(WorkerRepository workerRepository) {
        this.workerRepository = workerRepository;
    }

    public double calculateCostTeam() {
        double result = 0;
        for (Worker worker : workerRepository.getAll()) {
            result += worker.getCostInHours();
        }
        return result;
    }

    public WorkerRepository getWorkerRepository() {
        return workerRepository;
    }
}
