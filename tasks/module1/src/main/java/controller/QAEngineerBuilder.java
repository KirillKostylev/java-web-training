package controller;

import entity.QAEngineer;
import entity.Worker;

import java.util.Map;

public class QAEngineerBuilder extends AbstractWorkerBuilder {

    private boolean testSkills;

    QAEngineerBuilder(Map<String, String> infoAboutWorker){
        super(infoAboutWorker);
        testSkills = Boolean.parseBoolean(infoAboutWorker.get("testSkills"));

    }

    @Override
    Worker build() {
        return new QAEngineer(fullName, costInHours, level, salary, testSkills);
    }
}
