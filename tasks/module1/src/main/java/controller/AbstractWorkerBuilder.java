package controller;

import entity.Level;
import entity.Position;
import entity.Worker;
import validators.util.TypeChecker;

import java.util.Map;

public abstract class AbstractWorkerBuilder {
    protected String fullName;
    protected Position position;
    protected int costInHours;
    protected Level level;
    protected double salary;

    public AbstractWorkerBuilder(Map<String, String> infoAboutWorker){
        fullName = infoAboutWorker.get("fullName");
        position = Position.valueOf(infoAboutWorker.get("position"));
        costInHours = Integer.parseInt(infoAboutWorker.get("costInHours"));
        level = Level.valueOf(infoAboutWorker.get("level"));
        salary = Integer.parseInt(infoAboutWorker.get("salary"));
    }
    abstract Worker build();
}
