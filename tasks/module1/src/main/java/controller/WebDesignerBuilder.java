package controller;

import entity.WebDesigner;
import entity.Worker;

import java.util.Map;

public class WebDesignerBuilder extends AbstractWorkerBuilder {

    private boolean drawSkills;

    WebDesignerBuilder(Map<String, String> infoAboutWorker){
        super(infoAboutWorker);
        drawSkills = Boolean.parseBoolean(infoAboutWorker.get("drawSkills"));
    }

    @Override
    Worker build() {
        return new WebDesigner(fullName, costInHours, level, salary, drawSkills);
    }
}
