package controller;

import org.apache.log4j.Logger;
import parsers.WorkerLineParser;
import repository.WorkerRepository;
import service.WorkerService;
import validators.*;

import java.util.List;
import java.util.Map;

public class WorkerController {

    private final static Logger LOGGER = Logger.getLogger(WorkerController.class);

    private FactoryValidator factoryValidator;
    private FileValidator fileValidator;
    private DataReader fileReader;
    private WorkerLineParser workerLineParser;
    private WorkerService workerService;
    private BuilderFactory builderFactory;


    public WorkerController(FactoryValidator factoryValidator, FileValidator fileValidator,
                            DataReader fileReader, WorkerLineParser workerLineParser,WorkerService workerService,
                            BuilderFactory builderFactory) {
        this.factoryValidator = factoryValidator;
        this.fileValidator = fileValidator;
        this.fileReader = fileReader;
        this.workerLineParser = workerLineParser;
        this.workerService = workerService;
        this.builderFactory = builderFactory;
    }

    public WorkerRepository run(String path) {

        ValidatorResult fileValidatorResult = fileValidator.validate(path);
        if (!fileValidatorResult.isValid()) {
            throw new IllegalArgumentException(path + "is wrong");
        }

        List<String> lines = fileReader.dataRead(path);
        ValidatorResult validatorResult = new ValidatorResult();
        for (String line : lines) {
            Map<String, String> typeAndValueAboutWorker = workerLineParser.parse(line);
            if (!typeAndValueAboutWorker.containsKey("position")) {
                validatorResult.addError(ErrorCode.DATA_KEY, "Illegal key");
                break;
            }
            AbstractValidator validator =
                    factoryValidator.getValidatorByTypeOfValidator(typeAndValueAboutWorker.get("position"));
            if(!validator.validate(typeAndValueAboutWorker).isValid()){
                break;
            }
            AbstractWorkerBuilder builder = builderFactory.getWorker(typeAndValueAboutWorker);
            workerService.getWorkerRepository().add(builder.build());
        }
//        workerService.calculateCostTeam();
        return workerService.getWorkerRepository();
    }
}

