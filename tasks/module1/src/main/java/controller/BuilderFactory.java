package controller;

import entity.Developer;
import entity.Position;
import entity.Worker;
import javafx.util.Builder;

import java.util.Map;

public class BuilderFactory {
    public AbstractWorkerBuilder getWorker(Map<String, String> infoAboutWorker) {
        Position position = Position.valueOf(infoAboutWorker.get("position"));
        AbstractWorkerBuilder builder;
        switch (position) {
            case DEVELOPER:
                return new DeveloperBuilder(infoAboutWorker);
            case QA_ENGINEER:
                return new QAEngineerBuilder(infoAboutWorker);
            case WEB_DESIGNER:
                return new WebDesignerBuilder(infoAboutWorker);
            default:
                throw new IllegalArgumentException("Wrong param");
        }
    }
}