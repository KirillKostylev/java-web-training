package controller;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class DataReader {
    private final static Logger LOGGER = Logger.getLogger(DataReader.class);

    public List<String> dataRead(String path) {
        List<String> lines = new ArrayList<>();

        if (path == null) {
            throw new IllegalArgumentException("Path can't be null");
        }

        try {
            lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
            LOGGER.info("File was read");
        } catch (IOException e) {
            LOGGER.error(e.getStackTrace());
        }
        return lines;
    }
}
