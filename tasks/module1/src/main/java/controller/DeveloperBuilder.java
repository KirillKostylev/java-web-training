package controller;

import entity.Developer;
import entity.Worker;

import java.util.Map;

public class DeveloperBuilder extends AbstractWorkerBuilder {

    private boolean developSkills;

    public DeveloperBuilder(Map<String, String> infoAboutWorker) {
        super(infoAboutWorker);
        developSkills = Boolean.parseBoolean(infoAboutWorker.get("developSkills"));
    }

    @Override
    Worker build() {
        return new Developer(fullName, costInHours, level, salary, developSkills);
    }
}
