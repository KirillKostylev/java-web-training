package controller;

public class IllegalLineException extends Exception{
    public IllegalLineException(String msg) {
        super(msg);
    }
}
