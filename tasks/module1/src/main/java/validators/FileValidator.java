package validators;

import org.apache.log4j.Logger;

import java.io.File;

public class FileValidator extends AbstractValidator<String> {
    private final static Logger LOGGER = Logger.getLogger(FileValidator.class);

    @Override
    public ValidatorResult validate(String path) {

        if (path == null) {
            LOGGER.warn("Path can't be null");
            validatorResult.addError(ErrorCode.PATH, "Path can't be null");
            return validatorResult;
        }

        File file = new File(path);

        if (!file.exists()) {
            LOGGER.warn(path + "not found");
            validatorResult.addError(ErrorCode.FILE_NOT_FOUND, path + " not found");
        }

        if (file.length() == 0) {
            LOGGER.warn(path + " can't be empty");
            validatorResult.addError(ErrorCode.EMPTY_FILE, path + " can't be empty");
        }

        return validatorResult;
    }

}


