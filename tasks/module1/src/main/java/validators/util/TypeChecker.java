package validators.util;

import entity.Level;
import entity.Position;

public class TypeChecker {

    public static boolean isNumber(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isLevel(String str) {
        try {
            Level.valueOf(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isPos(String str) {
        try {
            Position.valueOf(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isDevelopSkills(String str) {
        return Boolean.parseBoolean(str);

    }

    public static boolean isTestSkills(String str) {
        return Boolean.parseBoolean(str);
    }


    public static boolean isDrawSkills(String str) {
        return Boolean.parseBoolean(str);
    }

    public static boolean isFullName(String str) {
        return str.contains(" ");
    }
}