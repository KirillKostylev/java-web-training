package validators;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ValidatorResult {
    private static Logger LOGGER = Logger.getLogger(ValidatorResult.class);

    private Map<ErrorCode, String> validationResult;

    public ValidatorResult() {
        this.validationResult = new HashMap<>();
        LOGGER.info("ValidationResult was created");
    }

    public boolean isValid() {
        return validationResult.isEmpty();
    }

    public void addError(ErrorCode key, String errorMessage) {
        if (errorMessage != null) {
            validationResult.put(key, errorMessage);
            LOGGER.info("Error was added");
        } else throw new IllegalArgumentException("String cannot be null");
    }

    public Map<ErrorCode, String> getValidationResult() {
        return validationResult;
    }

    public void setValidationResult(Map<ErrorCode, String> validationResult) {
        if (validationResult != null) {
            this.validationResult = validationResult;
        }
    }

    @Override
    public String toString() {
        return "ValidatorResult{" +
                "validationResultMap=" + validationResult +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidatorResult that = (ValidatorResult) o;
        return Objects.equals(validationResult, that.validationResult);
    }

    @Override
    public int hashCode() {
        return Objects.hash(validationResult);
    }
}

