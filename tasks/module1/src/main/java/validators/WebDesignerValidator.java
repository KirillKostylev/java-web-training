package validators;

import org.apache.log4j.Logger;
import validators.util.TypeChecker;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class WebDesignerValidator extends AbstractValidator<Map<String, String>> {

    private final static Logger LOGGER = Logger.getLogger(WebDesignerValidator.class);
    private final static List<String> webDesignerFields = Arrays.asList("drawskills");

    @Override
    public ValidatorResult validate(Map<String, String> data) {

        int counter = 0;
        for (Map.Entry<String, String> pair : data.entrySet()) {

            if(!checkPair(pair.getKey(),pair.getValue(),webDesignerFields).isValid()){
                return validatorResult;
            }
            if (webDesignerFields.contains(pair.getKey().toLowerCase()) && !checkWebDesignerDate(pair.getKey(), pair.getValue())) {
                validatorResult.addError(ErrorCode.DATA_VALUE, "Illegal data");
                return validatorResult;
            }
        }
        return validatorResult;
    }


    private boolean checkWebDesignerDate(String key, String value) {
        switch (key.toLowerCase()) {
            case "drawskills":
                return isDrawSkills(value);
        }
        return false;
    }

    private boolean isDrawSkills(String str) {
        return TypeChecker.isDrawSkills(str);
    }
}
