package validators;

public enum ErrorCode {
    FILE_NOT_FOUND,
    EMPTY_FILE,
    PATH,
    DATA_VALUE,
    DATA_KEY;
}
