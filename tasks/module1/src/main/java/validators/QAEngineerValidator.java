package validators;

import org.apache.log4j.Logger;
import validators.util.TypeChecker;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class QAEngineerValidator extends AbstractValidator<Map<String, String>> {
    private final static Logger LOGGER = Logger.getLogger(QAEngineerValidator.class);
    private final static List<String> qAEngineerFields = Arrays.asList("testskills");

    @Override
    public ValidatorResult validate(Map<String, String> data) {

        for (Map.Entry<String, String> pair : data.entrySet()) {

            if(!checkPair(pair.getKey(),pair.getValue(),qAEngineerFields).isValid()){
                return validatorResult;
            }
            if (qAEngineerFields.contains(pair.getKey().toLowerCase())
                    && !checkQAEngineerDate(pair.getKey(), pair.getValue())) {
                validatorResult.addError(ErrorCode.DATA_VALUE, "Illegal data");
                return validatorResult;
            }
        }
        return validatorResult;
    }

    private boolean checkQAEngineerDate(String key, String value) {
        switch (key.toLowerCase()) {
            case "testskills":
                return isTestSkills(value);
        }
        return false;
    }

    private boolean isTestSkills(String str) {
        return TypeChecker.isTestSkills(str);
    }
}
