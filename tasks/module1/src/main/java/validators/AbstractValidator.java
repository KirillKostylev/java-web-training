package validators;

import org.apache.log4j.Logger;
import validators.util.TypeChecker;

import java.util.*;

import static validators.util.TypeChecker.isPos;

public abstract class AbstractValidator<T> {
    private final static Logger LOGGER = Logger.getLogger(AbstractValidator.class);

    protected static final List<String> workerFields = new ArrayList<>();
    ;
    protected ValidatorResult validatorResult;

    public AbstractValidator() {
        workerFields.add("position");
        workerFields.add("fullname");
        workerFields.add("costinhours");
        workerFields.add("salary");
        workerFields.add("level");
        this.validatorResult = new ValidatorResult();
    }

    abstract public ValidatorResult validate(T data);

    boolean checkWorkerData(String key, String value) {
        switch (key.toLowerCase()) {
            case "position":
                return isPosition(value);
            case "costinhours":
                return isCostInHours(value);
            case "level":
                return isLevel(value);
            case "salary":
                return isSalary(value);
            case "fullname":
                return isFullName(value);
        }

        return false;
    }

    protected ValidatorResult checkPair(String key, String value, List<String> childField) {

        if (!workerFields.contains(key.toLowerCase())
                && !childField.contains(key.toLowerCase())) {
            LOGGER.info(key + " wasn't found such field");
            validatorResult.addError(ErrorCode.DATA_KEY,
                    key + " wasn't found such field");
        }
        if (workerFields.contains(key.toLowerCase())
                && !checkWorkerData(key, value)) {

            validatorResult.addError(ErrorCode.DATA_VALUE, "Illegal data");
            return validatorResult;
        }
        return validatorResult;
    }

    private boolean isFullName(String str) {
        return str != null && TypeChecker.isFullName(str);
    }

    private boolean isCostInHours(String str) {
        return str != null && TypeChecker.isNumber(str) && Integer.parseInt(str) > 0 && Integer.parseInt(str) < 24;
    }

    private boolean isSalary(String str) {
        return str != null && TypeChecker.isNumber(str) && Integer.parseInt(str) > 0;
    }

    private boolean isLevel(String str) {
        return str != null && TypeChecker.isLevel(str);
    }

    private boolean isPosition(String str) {
        return isPos(str);
    }

    public ValidatorResult getValidatorResult() {
        return validatorResult;
    }

}
