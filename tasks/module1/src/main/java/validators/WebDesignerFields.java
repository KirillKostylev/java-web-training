package validators;

public enum WebDesignerFields {
    DRAW_SKILLS("drawSkills");

    private final String type;

    WebDesignerFields(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
