package validators;

public enum TypeOfValidators {
    FILE_VALIDATOR,
    DEVELOPER_VALIDATOR,
    QA_ENGINEER_VALIDATOR,
    WEB_DESIGNER_VALIDATOR;
}
