package validators;

import entity.Position;

public class FactoryValidator {
    public AbstractValidator getValidatorByTypeOfValidator(String position) {
        Position pos = Position.valueOf(position);
        switch (pos) {
            case DEVELOPER:
                return new DeveloperValidator();
            case QA_ENGINEER:
                return new QAEngineerValidator();
            case WEB_DESIGNER:
                return new WebDesignerValidator();
            default:
                throw new IllegalArgumentException(position + " should be in Enum Position");
        }
    }
}
