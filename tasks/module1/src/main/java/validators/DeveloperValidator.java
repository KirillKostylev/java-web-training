package validators;

import org.apache.log4j.Logger;
import validators.util.TypeChecker;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class DeveloperValidator extends AbstractValidator<Map<String, String>> {
    private final static Logger LOGGER = Logger.getLogger(DeveloperValidator.class);
    private final static List<String> developerFields = Arrays.asList("developskills");

    @Override
    public ValidatorResult validate(Map<String, String> data) {

        for (Map.Entry<String, String> pair : data.entrySet()) {

            if(!checkPair(pair.getKey(),pair.getValue(),developerFields).isValid()){
                return validatorResult;
            }

            if (developerFields.contains(pair.getKey().toLowerCase())
                    && !checkDeveloperDate(pair.getKey(), pair.getValue())) {
                validatorResult.addError(ErrorCode.DATA_VALUE, "Illegal data");
                return validatorResult;
            }
        }
        return validatorResult;
    }

    private boolean checkDeveloperDate(String key, String value) {
        switch (key.toLowerCase()) {
            case "developskills":
                return isDevelopSkills(value);
        }
        return false;
    }

    private boolean isDevelopSkills(String str) {
        return TypeChecker.isDevelopSkills(str);
    }
}
