package entity;

import org.apache.log4j.Logger;

import java.util.Objects;

public class Developer extends Worker {
    private final static Logger LOGGER = Logger.getLogger(Developer.class);
    private static Boolean DEFAULT_VALUE_DEVELOP_SKILLS = false;

    private boolean developSkills;

    public Developer(String fullName, int costInHours, Level level, double salary,
                     boolean developSkills) {
        super(fullName, Position.DEVELOPER, costInHours, level, salary);
        if (!developSkills) {
            this.setLevel(Level.JUNIOR);
        }
        this.developSkills = developSkills;
        LOGGER.info("Developer was created");
    }

//    public Developer(String fullName, int costInHours, Level level, double salary,
//                     boolean developSkills) {
//        this(fullName, costInHours, level, salary, DEFAULT_VALUE_DEVELOP_SKILLS);
//    }

    public boolean isDevelopSkills() {
        return developSkills;
    }

    public void setDevelopSkills(boolean developSkills) {
        this.developSkills = developSkills;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Developer developer = (Developer) o;
        return developSkills == developer.developSkills;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), developSkills);
    }
}
