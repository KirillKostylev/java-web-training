package entity;

import org.apache.log4j.Logger;

public class WebDesigner extends Worker {
    private final static Logger LOGGER = Logger.getLogger(WebDesigner.class);
    private static boolean DEFAULT_VALUE_DRAW_SKILLS = false;
    private boolean drawSkills;

    public WebDesigner(String fullName, int costInHours, Level level, double salary, boolean drawSkills) {
        super(fullName, Position.WEB_DESIGNER,costInHours, level, salary);
        if (!drawSkills) {
            this.setLevel(Level.JUNIOR);
        }
        this.drawSkills = drawSkills;
        LOGGER.info("WebDesigner was created");
    }

    public WebDesigner(String fullName, int costInHours, Level level, double salary) {
        this(fullName, costInHours, level, salary, DEFAULT_VALUE_DRAW_SKILLS);
    }

    public boolean isDrawSkills() {
        return drawSkills;
    }

    public void setDrawSkills(boolean drawSkills) {
        this.drawSkills = drawSkills;
    }
}
