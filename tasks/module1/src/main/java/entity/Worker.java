package entity;

import java.util.Objects;

public abstract class Worker {
    private String fullName;
    private Position position;
    private int costInHours;
    private Level level;
    private double salary;

    public Worker(String fullName, Position position, int costInHours, Level level, double salary) {
        if (fullName != null && position != null && level != null) {
            this.fullName = fullName;
            this.position = position;
            this.costInHours = costInHours;
            this.level = level;
            this.salary = salary;
        }
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }


    public double getCostInHours() {
        return costInHours;
    }

    public void setCostInHours(int costInHours) {
        this.costInHours = costInHours;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        if (level != null) {
            this.level = level;
        }
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Worker worker = (Worker) o;
        return costInHours == worker.costInHours &&
                Double.compare(worker.salary, salary) == 0 &&
                Objects.equals(fullName, worker.fullName) &&
                position == worker.position &&
                level == worker.level;
    }

    @Override
    public int hashCode() {
        return Objects.hash(fullName, position, costInHours, level, salary);
    }

    @Override
    public String toString() {
        return "Worker{" +
                "fullName='" + fullName + '\'' +
                ", costInHours=" + costInHours +
                ", level=" + level +
                ", salary=" + salary +
                '}';
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        if (position != null) {
            this.position = position;
        }
    }
}
