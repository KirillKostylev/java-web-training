package entity;

import org.apache.log4j.Logger;

import java.util.Objects;

public class QAEngineer extends Worker {

    private final static Logger LOGGER = Logger.getLogger(QAEngineer.class);
    private static boolean DEFAULT_VALUE_TEST_SKILLS = false;
    private boolean testSkills;

    public QAEngineer(String fullName, int costInHours, Level level, double salary, boolean testSkills) {
        super(fullName, Position.QA_ENGINEER, costInHours, level, salary);
        if (!testSkills) {
            this.setLevel(Level.JUNIOR);
        }
        this.testSkills = testSkills;
        LOGGER.info("QAEngineer was created");
    }

    public QAEngineer(String fullName, int costInHours, Level level, double salary) {
        this(fullName, costInHours, level, salary, DEFAULT_VALUE_TEST_SKILLS);
    }

    public boolean isTestSkills() {
        return testSkills;
    }

    public void setTestSkills(boolean testSkills) {
        this.testSkills = testSkills;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        QAEngineer that = (QAEngineer) o;
        return testSkills == that.testSkills;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), testSkills);
    }
}
