package parsers;

import org.apache.log4j.Logger;

import javax.print.DocFlavor;
import java.lang.reflect.Array;
import java.util.*;

public class WorkerLineParser {
    private final static Logger LOGGER = Logger.getLogger(WorkerLineParser.class);
    private static final String CHARACTER_SPLIT_BETWEEN_TYPES = ", ";
    private static final String CHARACTER_SPLIT_BETWEEN_KEY_AND_VALUE = ":";

    public Map<String, String> parse(String line) {
        Map<String, String> objectTypeWithValue = new LinkedHashMap<>();

        for (String str : line.split(CHARACTER_SPLIT_BETWEEN_TYPES)) {
            String[] tempKeyAndValue = str.split(CHARACTER_SPLIT_BETWEEN_KEY_AND_VALUE);
            objectTypeWithValue.put(tempKeyAndValue[0].trim(), tempKeyAndValue[1].trim());
        }

        LOGGER.info("parsing was successful");
        return objectTypeWithValue;
    }
}
