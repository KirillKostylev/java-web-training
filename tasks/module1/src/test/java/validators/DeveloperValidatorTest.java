package validators;

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

public class DeveloperValidatorTest {

    @Test
    public void validateWithoutProblem() {
        ValidatorResult expectedValidatorResult = new ValidatorResult();

        Map<String, String> map = new LinkedHashMap<>();
        map.put("fullName", "Sidorov Ivan");
        map.put("position", "DEVELOPER");
        map.put("costInHours", "8");
        map.put("level", "MIDDLE");
        map.put("salary", "1000");
        map.put("developSkills", "true");

        DeveloperValidator developerValidator = new DeveloperValidator();
        ValidatorResult actualValidationResult = developerValidator.validate(map);

        Assert.assertEquals(expectedValidatorResult, actualValidationResult);
    }


    @Test
    public void validateWithWrongType() {
        ValidatorResult expectedValidatorResult = new ValidatorResult();
        expectedValidatorResult.addError(ErrorCode.DATA_KEY, "fullNam wasn't found such field");

        Map<String, String> map = new LinkedHashMap<>();
        map.put("fullNam", "Sidorov Ivan");
        map.put("position", "DEVELOPER");
        map.put("costInHours", "8");
        map.put("level", "MIDDLE");
        map.put("salary", "1000");
        map.put("developSkills", "true");

        DeveloperValidator developerValidator = new DeveloperValidator();
        ValidatorResult actualValidationResult = developerValidator.validate(map);

        Assert.assertEquals(expectedValidatorResult, actualValidationResult);
    }

    @Test
    public void validateWithWrongValue() {
        ValidatorResult expectedValidatorResult = new ValidatorResult();
        expectedValidatorResult.addError(ErrorCode.DATA_VALUE, "Illegal data");

        Map<String, String> map = new LinkedHashMap<>();
        map.put("fullName", "Sidorov Ivan");
        map.put("position", "DEVELOPER");
        map.put("costInHours", "qwecas");
        map.put("level", "MIDDLE");
        map.put("salary", "1000");
        map.put("developSkills", "true");

        DeveloperValidator developerValidator = new DeveloperValidator();
        ValidatorResult actualValidationResult = developerValidator.validate(map);

        Assert.assertEquals(expectedValidatorResult, actualValidationResult);
    }
}