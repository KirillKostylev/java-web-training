package validators;

import entity.QAEngineer;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class QAEngineerValidatorTest {
    @Test
    public void validateWithoutProblem() {
        ValidatorResult expectedValidatorResult = new ValidatorResult();

        Map<String, String> map = new LinkedHashMap<>();
        map.put("fullName", "Sidorov Ivan");
        map.put("position", "QA_ENGINEER");
        map.put("costInHours", "8");
        map.put("level", "MIDDLE");
        map.put("salary", "1000");
        map.put("testSkills", "true");

        QAEngineerValidator qaEngineerValidator = new QAEngineerValidator();
        ValidatorResult actualValidationResult = qaEngineerValidator.validate(map);

        Assert.assertEquals(expectedValidatorResult, actualValidationResult);
    }


    @Test
    public void validateWithWrongType() {
        ValidatorResult expectedValidatorResult = new ValidatorResult();
        expectedValidatorResult.addError(ErrorCode.DATA_KEY, "oiasfa8 wasn't found such field");

        Map<String, String> map = new LinkedHashMap<>();
        map.put("fullName", "Sidorov Ivan");
        map.put("position", "QA_ENGINEER");
        map.put("costInHours", "8");
        map.put("oiasfa8", "MIDDLE");
        map.put("salary", "1000");
        map.put("testSkills", "true");

        QAEngineerValidator qaEngineerValidator = new QAEngineerValidator();
        ValidatorResult actualValidationResult = qaEngineerValidator.validate(map);

        Assert.assertEquals(expectedValidatorResult, actualValidationResult);
    }

    @Test
    public void validateWithWrongValue() {
        ValidatorResult expectedValidatorResult = new ValidatorResult();
        expectedValidatorResult.addError(ErrorCode.DATA_VALUE, "Illegal data");

        Map<String, String> map = new LinkedHashMap<>();
        map.put("fullName", "Sidorov Ivan");
        map.put("position", "QA_ENGINEER");
        map.put("costInHours", "8");
        map.put("level", " ");
        map.put("salary", "1000");
        map.put("testSkills", "true");

        QAEngineerValidator qaEngineerValidator = new QAEngineerValidator();
        ValidatorResult actualValidationResult = qaEngineerValidator.validate(map);

        Assert.assertEquals(expectedValidatorResult, actualValidationResult);
    }
}