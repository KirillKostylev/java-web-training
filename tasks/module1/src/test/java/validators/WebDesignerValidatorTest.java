package validators;

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class WebDesignerValidatorTest {
    @Test
    public void validateWithoutProblem() {
        ValidatorResult expectedValidatorResult = new ValidatorResult();

        Map<String, String> map = new LinkedHashMap<>();
        map.put("fullName", "Sidorov Ivan");
        map.put("position", "WEB_DESIGNER");
        map.put("costInHours", "8");
        map.put("level", "MIDDLE");
        map.put("salary", "1000");
        map.put("drawSkills", "true");

        WebDesignerValidator webDesignerValidator = new WebDesignerValidator();
        ValidatorResult actualValidationResult = webDesignerValidator.validate(map);

        Assert.assertEquals(expectedValidatorResult, actualValidationResult);
    }


    @Test
    public void validateWithWrongType() {
        ValidatorResult expectedValidatorResult = new ValidatorResult();
        expectedValidatorResult.addError(ErrorCode.DATA_KEY, ",sdf wasn't found such field");

        Map<String, String> map = new LinkedHashMap<>();
        map.put("fullName", "Sidorov Ivan");
        map.put("position", "WEB_DESIGNER");
        map.put("costInHours", "8");
        map.put("Level", "MIDDLE");
        map.put("salary", "1000");
        map.put(",sdf", "true");
        WebDesignerValidator webDesignerValidator = new WebDesignerValidator();
        ValidatorResult actualValidationResult = webDesignerValidator.validate(map);

        Assert.assertEquals(expectedValidatorResult, actualValidationResult);
    }

    @Test
    public void validateWithWrongValue() {
        ValidatorResult expectedValidatorResult = new ValidatorResult();
        expectedValidatorResult.addError(ErrorCode.DATA_VALUE, "Illegal data");

        Map<String, String> map = new LinkedHashMap<>();
        map.put("fullName", "Sidorov Ivan");
        map.put("position", "WEB_DESIGNER");
        map.put("costInHours", "8");
        map.put("level", "MIDDLE");
        map.put("salary", "1000");
        map.put("drawSkills", "tre");
        WebDesignerValidator webDesignerValidator = new WebDesignerValidator();
         ValidatorResult actualValidationResult = webDesignerValidator.validate(map);

        Assert.assertEquals(expectedValidatorResult, actualValidationResult);
    }
}