package controller;

import entity.Developer;
import entity.Level;
import entity.QAEngineer;
import entity.WebDesigner;
import org.junit.Assert;
import org.junit.Test;
import parsers.WorkerLineParser;
import repository.WorkerRepository;
import service.WorkerService;
import validators.FactoryValidator;
import validators.FileValidator;

import java.io.File;

public class WorkerControllerTest {

    @Test
    public void runTest1() {
        //expected
        WorkerRepository expectedWorkerRepository = new WorkerRepository();

        expectedWorkerRepository.add(
                new Developer("Sidorov Ivan", 8, Level.MIDDLE, 1000, true),
                new QAEngineer("Nekrasov Vasiliy", 6, Level.JUNIOR, 200),
                new QAEngineer("Petrov Michail", 8, Level.MIDDLE, 800, true),
                new Developer("Petrov Michail", 8, Level.JUNIOR, 400, true),
                new WebDesigner("Krasnov Roman", 8, Level.SENIOR, 2000, true));


        // prepare
        FactoryValidator factoryValidator = new FactoryValidator();
        FileValidator fileValidator = new FileValidator();
        DataReader dataReader = new DataReader();
        WorkerLineParser workerLineParser = new WorkerLineParser();
        WorkerService workerService = new WorkerService(new WorkerRepository());
        BuilderFactory builderFactory = new BuilderFactory();
        WorkerController workerController = new WorkerController(factoryValidator, fileValidator, dataReader, workerLineParser,
                workerService, builderFactory);


        File file = new File(getClass().getClassLoader().getResource("test.txt").getFile());

//        workerController.run(file.getAbsolutePath());

        WorkerRepository actualWorkerRepository = workerController.run(file.getAbsolutePath());

        //Assert
        Assert.assertEquals(expectedWorkerRepository, actualWorkerRepository);
    }
}