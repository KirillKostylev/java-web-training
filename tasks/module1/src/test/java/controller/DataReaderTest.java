package controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
@RunWith(JUnit4.class)
public class DataReaderTest {

    @Test
    public void checkDataRead() {
        List<String> expectedResultAfterReadingFile = Arrays.asList(
                "fullName:Sidorov Ivan, position:DEVELOPER, costInHours:8, level:MIDDLE, salary:1000," +
                        " developSkills:true",
                "fullName:Nekrasov Vasiliy, position:QA_ENGINEER, costInHours:6, level:JUNIOR, salary:200",
                "fullName:Petrov Michail, position:QA_ENGINEER, costInHours:8, level:MIDDLE, salary:800," +
                        " testSkills:true",
                "fullName:Petrov Michail, position:DEVELOPER, costInHours:8, level:JUNIOR, salary:400, " +
                        "developSkills:true",
                "fullName:Krasnov Roman, position:WEB_DESIGNER, costInHours:8, level:SENIOR, salary:2000," +
                        " drawSkills:true");


        File file = new File(getClass().getClassLoader().getResource("test.txt").getFile());
        DataReader dataReader = new DataReader();
        List<String> actualResultAfterReadingFile = dataReader.dataRead(file.getAbsolutePath());

        Assert.assertEquals(expectedResultAfterReadingFile, actualResultAfterReadingFile);
    }
}