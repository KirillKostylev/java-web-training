package parser;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import parsers.WorkerLineParser;

import java.util.LinkedHashMap;
import java.util.Map;
@RunWith(JUnit4.class)
public class ParserTest {

    @Test
    public void parser() {
        WorkerLineParser workerLineParser = new WorkerLineParser();
        Map<String,String> realResult = workerLineParser.parse("fullName:Sidorov Ivan, position:DEVELOPER, " +
                "costInHours:8, level:MIDDLE, salary:1000, developSkills:true");
        Map<String,String> expectedResult = new LinkedHashMap<>();
        expectedResult.put("fullName","Sidorov Ivan");
        expectedResult.put("position","DEVELOPER");
        expectedResult.put("costInHours","8");
        expectedResult.put("level","MIDDLE");
        expectedResult.put("salary","1000");
        expectedResult.put("developSkills","true");

        Assert.assertEquals(expectedResult, realResult);
    }
}