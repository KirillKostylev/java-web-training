package service;

import entity.Developer;
import entity.Level;
import entity.QAEngineer;
import entity.WebDesigner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import repository.WorkerRepository;
@RunWith(JUnit4.class)
public class WorkerServiceTest {

    @Test
    public void calculateCostTeam() {
        WorkerRepository workerRepository = new WorkerRepository();
        workerRepository.add(
                new Developer("Sidorov Ivan", 8, Level.MIDDLE, 1000, true),
                new QAEngineer("Nekrasov Vasiliy", 6, Level.JUNIOR, 200),
                new QAEngineer("Petrov Michail", 8, Level.MIDDLE, 800, true),
                new Developer("Petrov Michail", 8, Level.JUNIOR, 400, true),
                new WebDesigner("Krasnov Roman", 6, Level.SENIOR, 2000, true));

        double shouldBeResultInHours = 36;

        Assert.assertEquals(shouldBeResultInHours, new WorkerService(workerRepository).calculateCostTeam(), 0);
    }
}