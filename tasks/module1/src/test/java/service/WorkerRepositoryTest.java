package service;

import entity.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import repository.*;

import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class WorkerRepositoryTest {
    private WorkerRepository workerRepository;

    @Before
    public void init() {
        workerRepository = new WorkerRepository();
        workerRepository.add(new Developer("Sidorov Ivan", 8, Level.MIDDLE, 1000, true),
                new QAEngineer("Nekrasov Vasiliy", 6, Level.JUNIOR, 200),
                new QAEngineer("Petrov Michail", 8, Level.MIDDLE, 800, true),
                new Developer("Petrov Michail", 8, Level.JUNIOR, 400, true),
                new WebDesigner("Krasnov Roman", 8, Level.SENIOR, 2000, true));
    }

    @Test
    public void find() throws SpecificationException {
        List<Worker> shouldBeAfterFindBySalary = Arrays.asList(
                new Developer("Sidorov Ivan", 8, Level.MIDDLE, 1000, true),
                new QAEngineer("Petrov Michail", 8, Level.MIDDLE, 800, true));
        Assert.assertEquals(shouldBeAfterFindBySalary, workerRepository.query(
                new FindWorkerBySalarySpecification(500, 1000)));
    }

    @Test
    public void sort() throws SpecificationException {
        List<Worker> shouldBeAfterSortByName = Arrays.asList(
                new WebDesigner("Krasnov Roman", 8, Level.SENIOR, 2000, true),
                new QAEngineer("Nekrasov Vasiliy", 6, Level.JUNIOR, 200),
                new QAEngineer("Petrov Michail", 8, Level.MIDDLE, 800, true),
                new Developer("Petrov Michail", 8, Level.JUNIOR, 400, true),
                new Developer("Sidorov Ivan", 8, Level.MIDDLE, 1000, true));
        Assert.assertEquals(shouldBeAfterSortByName, workerRepository.query(new SortByNameSpecification()));
    }

    @Test
    public void sortWithSecondField() throws SpecificationException {
        List<Worker> shouldBeAfterSortByName = Arrays.asList(
                new WebDesigner("Krasnov Roman", 8, Level.SENIOR, 2000, true),
                new QAEngineer("Nekrasov Vasiliy", 6, Level.JUNIOR, 200),
                new Developer("Petrov Michail", 8, Level.JUNIOR, 400, true),
                new QAEngineer("Petrov Michail", 8, Level.MIDDLE, 800, true),
                new Developer("Sidorov Ivan", 8, Level.MIDDLE, 1000, true));
        Assert.assertEquals(shouldBeAfterSortByName, workerRepository.query(new SortByNameAndBySalarySpecification()));
    }


}