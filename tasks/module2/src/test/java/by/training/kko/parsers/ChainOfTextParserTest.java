package by.training.kko.parsers;

import by.training.kko.repository.ParagraphRepository;
import by.training.kko.repository.SentenceRepository;
import by.training.kko.repository.TextRepository;
import by.training.kko.repository.WordRepository;
import by.training.kko.services.ParagraphService;
import by.training.kko.services.SentenceService;
import by.training.kko.services.TextService;
import by.training.kko.services.WordService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ChainOfTextParserTest {

    @Test
    public void parse() {
        //prepare
        ParagraphRepository.getNextId().set(0);
        WordRepository wordRepository = new WordRepository();
        WordService wordService = new WordService(wordRepository);

        SentenceRepository sentenceRepository = new SentenceRepository();
        SentenceService sentenceService = new SentenceService(sentenceRepository);

        ParagraphRepository paragraphRepository = new ParagraphRepository();
        ParagraphService paragraphService = new ParagraphService(paragraphRepository);

        TextRepository textRepository = new TextRepository();
        TextService textService = new TextService(textRepository);

        //expected
        String expectedString = "\tIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n" +
                "\tIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.\n" +
                "\tIt is a established fact that a reader will be of a page when looking at its layout.\n" +
                "\tBye.\n";

        ChainOfTextParser chainOfTextParser = new ChainOfTextParser(textService,paragraphService,sentenceService,wordService);

        //actual
        String actual = chainOfTextParser.parse(expectedString).getText();

        //assert
        Assert.assertEquals(expectedString, actual);


    }
}