package by.training.kko.validator;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class FileValidatorTest {

    @Test
    public void validate() {
        // Expected
        ValidatorResult expectedValidatorResult = new ValidatorResult();

        //Actual
        FileValidator fileValidator = new FileValidator();
        File file = new File(getClass().getClassLoader().getResource("test.txt").getFile());
        ValidatorResult actualResult = fileValidator.validate(file.getAbsolutePath());

        //Assert
        Assert.assertEquals(expectedValidatorResult, actualResult);
    }
}