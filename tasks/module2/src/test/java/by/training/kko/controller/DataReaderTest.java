package by.training.kko.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

@RunWith(JUnit4.class)
public class DataReaderTest {

    @Test
    public void read() {
        // expected
        String expectedString = "\tIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n" +
                "\tIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.\n" +
                "\tIt is a established fact that a reader will be of a page when looking at its layout.\n" +
                "\tBye.\n";

        File file = new File(getClass().getClassLoader().getResource("test.txt").getFile());

        DataReader dataReader = new DataReader();
        //actual
        String actualString = dataReader.read(file.getAbsolutePath());

        //assert
        Assert.assertEquals(expectedString, actualString);
    }
}