package by.training.kko.repository;

import by.training.kko.model.Paragraph;
import by.training.kko.model.TextComposite;
import by.training.kko.model.TextLeaf;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicLong;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ParagraphRepositoryTest {

    @Test
    public void findByID() {
        //Prepare
        ParagraphRepository.getNextId().set(0);
        ParagraphRepository paragraphRepository = new ParagraphRepository();
        paragraphRepository.add(new Paragraph());
        paragraphRepository.add(new Paragraph());
        Paragraph expectedParagraph = paragraphRepository.getAll().get(1);


        //Actual
        Paragraph actualParagraph = paragraphRepository.findByID(1).get();

        //Assert
        Assert.assertEquals(actualParagraph, expectedParagraph);
    }

    @Test
    public void WrongFindByID() {
        //Prepare
        ParagraphRepository.getNextId().set(0);
        ParagraphRepository paragraphRepository = new ParagraphRepository();
        paragraphRepository.add(new Paragraph());
        paragraphRepository.add(new Paragraph());
        Paragraph expectedParagraph = paragraphRepository.getAll().get(1);


        //Actual
        Paragraph actualParagraph = paragraphRepository.findByID(0).get();

        //Assert
        Assert.assertNotEquals(actualParagraph, expectedParagraph);
    }




    @Test
    public void remove() {
        //Actual
        ParagraphRepository.getNextId().set(0);
        ParagraphRepository actualParagraphRepository = new ParagraphRepository();
        actualParagraphRepository.add(new Paragraph());
        actualParagraphRepository.add(new Paragraph());
        actualParagraphRepository.remove(1);
        //Expected
        ParagraphRepository expectedParagraphRepository = new ParagraphRepository();
        expectedParagraphRepository.add(new Paragraph());

        //Assert
        assertEquals(expectedParagraphRepository.getAll().size(), actualParagraphRepository.getAll().size());
    }
}