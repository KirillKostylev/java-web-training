package by.training.kko.services;

import by.training.kko.model.Paragraph;
import by.training.kko.model.Sentence;
import by.training.kko.model.WordLeaf;
import org.junit.Assert;
import org.junit.Test;

public class SortSentencesByWordNumberTest {

    @Test
    public void sort() {
        //Prepare
        Sentence bigSentence = new Sentence();
        bigSentence.addText(new WordLeaf("", "Hello", ","));
        bigSentence.addText(new WordLeaf("", "beautiful", ""));
        bigSentence.addText(new WordLeaf("", "world", "."));

        Sentence averageSentence = new Sentence();
        averageSentence.addText(new WordLeaf("", "It's", ""));
        averageSentence.addText(new WordLeaf("", "raining", "."));

        Sentence smallSentence = new Sentence();
        smallSentence.addText(new WordLeaf("", "Bye", "."));

        //Expected
        Paragraph expectedParagraph = new Paragraph();
        expectedParagraph.addText(smallSentence);
        expectedParagraph.addText(averageSentence);
        expectedParagraph.addText(bigSentence);

        //Actual
        Paragraph actualParagraph = new Paragraph();
        actualParagraph.addText(averageSentence);
        actualParagraph.addText(smallSentence);
        actualParagraph.addText(bigSentence);
        SortSentencesByWordNumber.sort(actualParagraph);

        //Assert
        Assert.assertEquals(expectedParagraph, actualParagraph);
    }

    @Test
    public void reverse() {
        //Prepare
        Sentence bigSentence = new Sentence();
        bigSentence.addText(new WordLeaf("", "Hello", ","));
        bigSentence.addText(new WordLeaf("", "beautiful", ""));
        bigSentence.addText(new WordLeaf("", "world", "."));

        Sentence averageSentence = new Sentence();
        averageSentence.addText(new WordLeaf("", "It's", ""));
        averageSentence.addText(new WordLeaf("", "raining", "."));

        Sentence smallSentence = new Sentence();
        smallSentence.addText(new WordLeaf("", "Bye", "."));

        //Expected
        Paragraph expectedParagraph = new Paragraph();
        expectedParagraph.addText(bigSentence);
        expectedParagraph.addText(averageSentence);
        expectedParagraph.addText(smallSentence);

        //Actual
        Paragraph actualParagraph = new Paragraph();
        actualParagraph.addText(averageSentence);
        actualParagraph.addText(smallSentence);
        actualParagraph.addText(bigSentence);
        SortSentencesByWordNumber.reverse(actualParagraph);

        //Assert
        Assert.assertEquals(expectedParagraph, actualParagraph);
    }
}