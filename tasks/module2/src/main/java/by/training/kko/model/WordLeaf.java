package by.training.kko.model;

import org.apache.log4j.Logger;

import java.util.Objects;

public class WordLeaf implements TextLeaf {
    private final static Logger LOGGER = Logger.getLogger(WordLeaf.class);

    private long wordID;
    private String word;
    private String starting;
    private String ending;

    public WordLeaf(String starting, String word, String ending) {
        this.starting = starting;
        this.word = word;
        this.ending = ending;
        LOGGER.info("WordLeaf was created");
    }

    public String getText() {
        return starting + word + ending;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WordLeaf wordLeaf = (WordLeaf) o;
        return wordID == wordLeaf.wordID &&
                Objects.equals(word, wordLeaf.word) &&
                Objects.equals(starting, wordLeaf.starting) &&
                Objects.equals(ending, wordLeaf.ending);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wordID, word, starting, ending);
    }

    public long getWordID() {
        return wordID;
    }

    public void setWordID(long wordID) {
        this.wordID = wordID;
    }
}
