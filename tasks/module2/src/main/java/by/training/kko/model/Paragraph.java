package by.training.kko.model;

import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Paragraph implements TextComposite {
    private final static Logger LOGGER = Logger.getLogger(Paragraph.class);

    private long paragraphID;
    private List<TextLeaf> paragraph;

    public Paragraph() {
        this.paragraph = new LinkedList<>();
        LOGGER.info("Paragraph was created");
    }

    @Override
    public void addText(TextLeaf sentence) {
        paragraph.add(sentence);
        LOGGER.info("\"" + sentence.getText() + "\"" + " was added in paragraph");
    }


    @Override
    public String getText() {
        StringBuilder str = new StringBuilder();
//        str.append("\t");
        for (TextLeaf sentence : paragraph) {
            str.append(sentence.getText());
        }
        return str.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Paragraph paragraph1 = (Paragraph) o;
        return paragraphID == paragraph1.paragraphID &&
                Objects.equals(paragraph, paragraph1.paragraph);
    }

    @Override
    public int hashCode() {
        return Objects.hash(paragraphID, paragraph);
    }

    public long getParagraphID() {
        return paragraphID;
    }

    public void setParagraphID(long paragraphID) {
        this.paragraphID = paragraphID;
    }

    public List<TextLeaf> getParagraph() {
        return paragraph;
    }
}

