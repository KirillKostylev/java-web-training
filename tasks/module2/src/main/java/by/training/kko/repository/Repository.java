package by.training.kko.repository;

import java.util.List;
import java.util.Optional;

public interface Repository<T> {
    void add(T textLeaf);

    Optional<T> findByID(long id);

    List<T> getAll();

    boolean remove(long id);


}
