package by.training.kko.services;

import by.training.kko.model.TextLeaf;
import by.training.kko.model.WordLeaf;
import by.training.kko.repository.Repository;

public class WordService implements TextLeafService {
    private Repository<WordLeaf> repository;

    public WordService(Repository<WordLeaf> repository) {
        this.repository = repository;
    }

    @Override
    public void save(TextLeaf textLeaf) {
        repository.add((WordLeaf) textLeaf);
    }
}
