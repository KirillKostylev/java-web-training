package by.training.kko.services;

import by.training.kko.model.Text;
import by.training.kko.model.TextLeaf;
import by.training.kko.repository.Repository;

public class TextService implements TextLeafService {
    private Repository<Text> repository;

    public TextService(Repository<Text> repository) {
        this.repository = repository;
    }

    @Override
    public void save(TextLeaf textLeaf) {
        repository.add((Text) textLeaf);
    }
}
