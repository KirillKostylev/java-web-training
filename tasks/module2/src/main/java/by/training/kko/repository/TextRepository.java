package by.training.kko.repository;

import by.training.kko.model.Text;
import by.training.kko.model.TextLeaf;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

public class TextRepository implements Repository<Text> {
    private static final Logger LOGGER = Logger.getLogger(TextRepository.class);

    private static AtomicLong nextId = new AtomicLong();

    private List<Text> textRepository;

    public TextRepository() {
        this.textRepository = new LinkedList<>();
    }

    @Override
    public void add(Text textLeaf) {
        textRepository.add(textLeaf);
    }

    @Override
    public Optional<Text> findByID(long id) {
        return textRepository.stream().filter(text -> text.getTextID() == id).findFirst();
    }

    @Override
    public List<Text> getAll() {
        return textRepository;
    }

    @Override
    public boolean remove(long id) {
        Optional<TextLeaf> a = Optional.of(findByID(id).get());
        if (!textRepository.remove(a.get())) {
            return false;
        }
        LOGGER.info(a.get() + " was removed");
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TextRepository that = (TextRepository) o;
        return Objects.equals(textRepository, that.textRepository);
    }

    @Override
    public int hashCode() {
        return Objects.hash(textRepository);
    }

}
