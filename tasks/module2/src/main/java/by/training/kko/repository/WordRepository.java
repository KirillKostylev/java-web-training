package by.training.kko.repository;

import by.training.kko.model.TextLeaf;
import by.training.kko.model.WordLeaf;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

public class WordRepository implements Repository<WordLeaf> {
    private static final Logger LOGGER = Logger.getLogger(WordRepository.class);
    private static AtomicLong nextId = new AtomicLong();

    private List<WordLeaf> sentence;

    public WordRepository() {
        this.sentence = new LinkedList<>();
    }

    @Override
    public void add(WordLeaf word) {
        sentence.add(word);
    }

    @Override
    public Optional<WordLeaf> findByID(long id) {
        return sentence.stream().filter(wordLeaf -> wordLeaf.getWordID() == id).findFirst();
    }


    @Override
    public List<WordLeaf> getAll() {
        return sentence;
    }

    @Override
    public boolean remove(long id) {
        Optional<TextLeaf> a = Optional.of(findByID(id).get());
        if (!sentence.remove(a.get())) {
            return false;
        }
        LOGGER.info(a.get() + " was removed");
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WordRepository that = (WordRepository) o;
        return Objects.equals(sentence, that.sentence);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentence);
    }

    @Override
    public String toString() {
        return "WordRepository{" +
                "sentence=" + sentence +
                '}';
    }
}
