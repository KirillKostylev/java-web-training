package by.training.kko.validator;

public enum ErrorKey {
    EMPTY_FILE, FILE_NOT_FOUND
}
