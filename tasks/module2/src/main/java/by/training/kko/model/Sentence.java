package by.training.kko.model;

import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Sentence implements TextComposite {
    private final static Logger LOGGER = Logger.getLogger(Sentence.class);

    private long sentenceID;
    private List<TextLeaf> sentence;


    public Sentence() {
        this.sentence = new LinkedList<>();
        LOGGER.info("Sentence was created");
    }

    public String getText() {
        StringBuilder str = new StringBuilder();
        for (TextLeaf word : sentence) {
            str.append(word.getText()).append(" ");
        }
        return str.toString();
    }

    public void addText(TextLeaf word) {
        sentence.add(word);
        LOGGER.info("\"" + word.getText() + "\"" + " was added in sentences");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sentence sentence1 = (Sentence) o;
        return sentenceID == sentence1.sentenceID &&
                Objects.equals(sentence, sentence1.sentence);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentenceID, sentence);
    }

    public long getSentenceID() {
        return sentenceID;
    }

    public void setSentenceID(long sentenceID) {
        this.sentenceID = sentenceID;
    }

    public List<TextLeaf> getSentence() {
        return sentence;
    }
}
