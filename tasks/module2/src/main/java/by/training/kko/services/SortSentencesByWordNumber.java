package by.training.kko.services;

import by.training.kko.model.Paragraph;
import by.training.kko.model.Sentence;
import by.training.kko.model.TextLeaf;

import java.util.Comparator;

public class SortSentencesByWordNumber {

    static public Paragraph sort(Paragraph paragraph) {
        paragraph.getParagraph().sort(new Comparator<TextLeaf>() {
            @Override
            public int compare(TextLeaf o1, TextLeaf o2) {
                return Integer.compare(((Sentence) o1).getSentence().size(), ((Sentence) o2).getSentence().size());
            }
        });
        return paragraph;
    }

    static public Paragraph reverse(Paragraph paragraph) {
        paragraph.getParagraph().sort(new Comparator<TextLeaf>() {
            @Override
            public int compare(TextLeaf o2, TextLeaf o1) {
                return Integer.compare(((Sentence) o1).getSentence().size(), ((Sentence) o2).getSentence().size());
            }
        });
        return paragraph;
    }
}
