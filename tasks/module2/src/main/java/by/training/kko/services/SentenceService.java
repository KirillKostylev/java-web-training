package by.training.kko.services;

import by.training.kko.model.Sentence;
import by.training.kko.model.TextLeaf;
import by.training.kko.repository.Repository;

public class SentenceService implements TextLeafService {
    private Repository<Sentence> repository;

    public SentenceService(Repository<Sentence> repository) {
        this.repository = repository;
    }

    @Override
    public void save(TextLeaf textLeaf) {
        repository.add((Sentence) textLeaf);
    }
}
