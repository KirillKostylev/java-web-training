package by.training.kko.validator;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ValidatorResult {
    private Map<ErrorKey, String> result;

    public ValidatorResult() {
        this.result = new HashMap<>();
    }

    public boolean isValid() {
        return result.isEmpty();
    }

    public void addError(ErrorKey key, String msg) {
        result.put(key, msg);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidatorResult that = (ValidatorResult) o;
        return Objects.equals(result, that.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(result);
    }

    @Override
    public String toString() {
        return "ValidatorResult{" +
                "result=" + result +
                '}';
    }
}
