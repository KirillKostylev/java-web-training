package by.training.kko.controller;

public class ControllerPathException extends IllegalArgumentException {
    ControllerPathException(String msg) {
        super(msg);
    }
}
