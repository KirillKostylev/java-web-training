package by.training.kko.validator;

import org.apache.log4j.Logger;

import java.io.File;

public class FileValidator {
    private static final Logger LOGGER = Logger.getLogger(FileValidator.class);

    public ValidatorResult validate(String path) {
        ValidatorResult validatorResult = new ValidatorResult();
        File file = new File(path);

        if (!file.exists()) {
            LOGGER.error(path + "not found");
            validatorResult.addError(ErrorKey.FILE_NOT_FOUND, path + " not found");
        }

        if (file.length() == 0) {
            LOGGER.error(path + " can't be empty");
            validatorResult.addError(ErrorKey.EMPTY_FILE, path + " can't be empty");
        }

        return validatorResult;
    }
}
