package by.training.kko.model;

public interface TextComposite extends TextLeaf{
    void addText(TextLeaf text);
}
