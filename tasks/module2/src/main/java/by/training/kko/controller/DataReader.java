package by.training.kko.controller;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataReader {
    private final static Logger LOGGER = Logger.getLogger(DataReader.class);

    public String read(String path) {
        StringBuilder str = new StringBuilder();
        if (path == null) {
            LOGGER.error(path + " cannot be null");
            throw new IllegalArgumentException(path + " cannot be null");
        }

        try {
            for (String line : Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8)) {

                line += "\n";
                str.append(line);
            }
            LOGGER.info("The text was read");
        } catch (IOException e) {
            LOGGER.error(e.getStackTrace());
        }
        return str.toString();
    }
}
