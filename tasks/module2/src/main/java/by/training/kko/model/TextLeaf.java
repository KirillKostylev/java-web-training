package by.training.kko.model;

import java.util.List;

public interface TextLeaf {
    String getText();

}
