package by.training.kko.model;

import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Text implements TextComposite {
    private final static Logger LOGGER = Logger.getLogger(Text.class);

    private long textID;
    private List<TextLeaf> paragraphsList;

    public Text() {
        this.paragraphsList = new LinkedList<>();
        LOGGER.info("Text was created");
    }

    @Override
    public void addText(TextLeaf paragraph) {
        paragraphsList.add(paragraph);
        LOGGER.info("\"" + paragraph.getText() + "\" was added in text");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Text text = (Text) o;
        return textID == text.textID &&
                Objects.equals(paragraphsList, text.paragraphsList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(textID, paragraphsList);
    }

    @Override
    public String getText() {
        StringBuilder str = new StringBuilder();
        for (TextLeaf paragraph : paragraphsList) {
            str.append("\t");
            str.append(paragraph.getText().trim());
            str.append("\n");
        }
        return str.toString();
    }

    @Override
    public String toString() {
        return "Text{" +
                "textID=" + textID +
                ", paragraphsList=" + getText() +
                '}';
    }

    public long getTextID() {
        return textID;
    }

    public void setTextID(long textID) {
        this.textID = textID;
    }
}
