package by.training.kko.parsers;

import by.training.kko.model.Sentence;
import by.training.kko.model.WordLeaf;
import by.training.kko.services.WordService;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceParser implements Parser {
    private static final Logger LOGGER = Logger.getLogger(SentenceParser.class);
    private static final String SENTENCE_REGEX =
            "(['(]+)?(\\w+-\\w+-\\w+|\\w+-\\w+|\\w+'?\\w+|—|\\w+)(\\.\\.\\.+|[.,'):]+|\\?+|!+)?";

    private WordService wordService;

    public SentenceParser(WordService wordService) {
        this.wordService = wordService;
    }

    @Override
    public Sentence parse(String text) {
        Pattern pattern = Pattern.compile(SENTENCE_REGEX);
        Matcher matcher = pattern.matcher(text);

        Sentence sentence = new Sentence();

        while (matcher.find()) {
            String strStarting = matcher.group(1);
            String str = matcher.group(2);
            String strEnding = matcher.group(3);
            // group(3) is starting, group(2) is word, group(3) is ending
            if (strStarting == null) {
                strStarting = "";
            }
            if (str == null) {
                str = "";
            }
            if (strEnding == null) {
                strEnding = "";
            }
            WordLeaf word = new WordLeaf(strStarting, str, strEnding);
            wordService.save(word);
            sentence.addText(word);
        }
        LOGGER.info("Sentence was parsed");
        return sentence;
    }
}
