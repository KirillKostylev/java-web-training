package by.training.kko.repository;

import by.training.kko.model.Sentence;
import by.training.kko.model.TextLeaf;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

public class SentenceRepository implements Repository<Sentence> {
    private static final Logger LOGGER = Logger.getLogger(SentenceRepository.class);
    private static AtomicLong nextId = new AtomicLong();

    private List<Sentence> paragraph;

    public SentenceRepository() {
        this.paragraph = new LinkedList<>();
    }

    @Override
    public void add(Sentence sentence) {
        sentence.setSentenceID(nextId.getAndIncrement());
        paragraph.add(sentence);
    }

    @Override
    public Optional<Sentence> findByID(long id) {
        return paragraph.stream().filter(sentence -> sentence.getSentenceID() == id).findFirst();
    }

    @Override
    public List<Sentence> getAll() {
        return paragraph;
    }

    @Override
    public boolean remove(long id) {
        Optional<TextLeaf> a = Optional.of(findByID(id).get());
        if (!paragraph.remove(a.get())) {
            return false;
        }
        LOGGER.info(a.get() + " was removed");
        return true;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SentenceRepository that = (SentenceRepository) o;
        return Objects.equals(paragraph, that.paragraph);
    }

    @Override
    public int hashCode() {
        return Objects.hash(paragraph);
    }

}
