package by.training.kko.parsers;

import by.training.kko.model.TextLeaf;

public interface Parser {
    TextLeaf parse(String text);
}
