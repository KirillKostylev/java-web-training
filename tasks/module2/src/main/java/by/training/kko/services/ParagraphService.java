package by.training.kko.services;

import by.training.kko.model.Paragraph;
import by.training.kko.model.TextLeaf;
import by.training.kko.repository.Repository;

public class ParagraphService implements TextLeafService {
    private Repository<Paragraph> repository;

    public ParagraphService(Repository<Paragraph> repository) {
        this.repository = repository;
    }


    @Override
    public void save(TextLeaf textLeaf) {
        repository.add((Paragraph) textLeaf);
    }

}
