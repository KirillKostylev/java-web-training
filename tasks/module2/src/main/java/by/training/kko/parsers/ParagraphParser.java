package by.training.kko.parsers;

import by.training.kko.model.Paragraph;
import by.training.kko.model.Sentence;
import by.training.kko.services.SentenceService;
import by.training.kko.services.WordService;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParagraphParser implements Parser {
    private static final Logger LOGGER = Logger.getLogger(ParagraphParser.class);
    private static final String SENTENCE_REGEX = "([A-Z])(.)+?([!?]|\\.{3}|\\.)";

    private SentenceService sentenceService;
    private WordService wordService;

    public ParagraphParser(SentenceService sentenceService, WordService wordService) {
        this.sentenceService = sentenceService;
        this.wordService = wordService;
    }

    @Override
    public Paragraph parse(String text) {

        Pattern pattern = Pattern.compile(SENTENCE_REGEX);
        Matcher matcher = pattern.matcher(text);

        Paragraph paragraph = new Paragraph();

        SentenceParser sentenceParse = new SentenceParser(wordService);
        while (matcher.find()) {
            Sentence str = sentenceParse.parse(matcher.group(0));
            sentenceService.save(str);
            paragraph.addText(str);
        }
        LOGGER.info("Paragraph was parsed");
        return paragraph;
    }
}
