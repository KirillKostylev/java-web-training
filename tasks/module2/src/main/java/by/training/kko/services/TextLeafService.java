package by.training.kko.services;

import by.training.kko.model.TextLeaf;

public interface TextLeafService {
    void save(TextLeaf textLeaf);
}
