package by.training.kko.parsers;

import by.training.kko.model.Paragraph;
import by.training.kko.model.Text;
import by.training.kko.services.ParagraphService;
import by.training.kko.services.SentenceService;
import by.training.kko.services.TextService;
import by.training.kko.services.WordService;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChainOfTextParser implements Parser {
    private static final Logger LOGGER = Logger.getLogger(ChainOfTextParser.class);
    private static final String PARAGRAPH_REGEX = "(\\t)(.+)(\\n+)?";

    private TextService textService;
    private ParagraphService paragraphService;
    private SentenceService sentenceService;
    private WordService wordService;

    public ChainOfTextParser(TextService textService, ParagraphService paragraphService, SentenceService sentenceService,
                             WordService wordService) {
        this.textService = textService;
        this.paragraphService = paragraphService;
        this.sentenceService = sentenceService;
        this.wordService = wordService;
    }

    @Override
    public Text parse(String str) {
        Pattern pattern = Pattern.compile(PARAGRAPH_REGEX);
        Matcher matcher = pattern.matcher(str);

        Text text = new Text();

        ParagraphParser paragraphParse = new ParagraphParser(sentenceService, wordService);
        while (matcher.find()) {
            Paragraph paragraph = paragraphParse.parse(matcher.group(2));
            paragraphService.save(paragraph);
            text.addText(paragraph);
        }
        LOGGER.info("Text was parsed");
        textService.save(text);
        return text;
    }
}
