package by.training.kko.controller;

import by.training.kko.model.Text;
import by.training.kko.parsers.ChainOfTextParser;
import by.training.kko.services.ParagraphService;
import by.training.kko.services.SentenceService;
import by.training.kko.services.TextService;
import by.training.kko.services.WordService;
import by.training.kko.validator.FileValidator;
import by.training.kko.validator.ValidatorResult;
import org.apache.log4j.Logger;

public class TextController {
    private WordService wordService;
    private SentenceService sentenceService;
    private ParagraphService paragraphService;
    private TextService textService;
    private DataReader dataReader;

    public TextController(WordService wordService, SentenceService sentenceService, ParagraphService paragraphService,
                          TextService textService, DataReader dataReader) {
        this.wordService = wordService;
        this.sentenceService = sentenceService;
        this.paragraphService = paragraphService;
        this.textService = textService;
        this.dataReader = dataReader;
    }

    private static final Logger LOGGER = Logger.getLogger(TextController.class);

    public void collectText(String path) {
        FileValidator fileValidator = new FileValidator();
        ValidatorResult validatorResult = fileValidator.validate(path);
        if (!validatorResult.isValid()) {
            LOGGER.error(validatorResult.toString());
            throw new ControllerPathException("Trouble with path = " + path);
        }
        String str = dataReader.read(path);
        ChainOfTextParser chainOfTextParser = new ChainOfTextParser(textService, paragraphService, sentenceService, wordService);
        Text text = chainOfTextParser.parse(str);
        LOGGER.info("The result of the program: " + text.getText());
    }
}
