package by.training.kko.repository;

import by.training.kko.model.Paragraph;
import by.training.kko.model.TextLeaf;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

public class ParagraphRepository implements Repository<Paragraph> {
    private static final Logger LOGGER = Logger.getLogger(ParagraphRepository.class);
    private static AtomicLong nextId = new AtomicLong();

    private List<Paragraph> text;

    public ParagraphRepository() {
        this.text = new LinkedList<>();
    }

    @Override
    public void add(Paragraph paragraph) {
        paragraph.setParagraphID(nextId.getAndIncrement());
        text.add(paragraph);
    }

    @Override
    public Optional<Paragraph> findByID(long id) {
        return text.stream().filter(paragraph -> paragraph.getParagraphID() == id).findFirst();
    }

    @Override
    public List<Paragraph> getAll() {
        return text;
    }

    @Override
    public boolean remove(long id) {
        Optional<TextLeaf> a = Optional.of(findByID(id).get());
        if (!text.remove(a.get())) {
            return false;
        }
        LOGGER.info(a.get() + " was removed");
        return true;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParagraphRepository that = (ParagraphRepository) o;
        return Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(text);
    }

    @Override
    public String toString() {
        return "ParagraphRepository{" +
                "text=" + text +
                '}';
    }

    public static AtomicLong getNextId() {
        return nextId;
    }
}
